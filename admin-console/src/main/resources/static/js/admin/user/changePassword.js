layui.define([ 'form', 'table','userApi'], function(exports) {
	var form = layui.form;
	var userApi = layui.userApi;
	var index = layui.index;

	form.verify({
		username: function(value, item){ //value：表单的值、item：表单的DOM对象
			if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
				return '用户名不能有特殊字符';
			}
			if(/(^\_)|(\__)|(\_+$)/.test(value)){
				return '用户名首尾不能出现下划线\'_\'';
			}
			if(/^\d+\d+\d$/.test(value)){
				return '用户名不能全为数字';
			}
		}

		//我们既支持上述函数式的方式，也支持下述数组的形式
		//数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
		,pass: [
			/^[\S]{6,20}$/
			,'密码必须6到20位，且不能出现空格'
		],
		confirmpwd:function (value, item) {
			if($("#changePasswordInput1").val() !=$("#changePasswordInput2").val() ){
				return '两次输入的密码不一致';
			}
		}
	});

	var view = {
			init:function(){
				Lib.initGenrealForm($("#changePasswordForm"),form);
				this.initSubmit();
			},
			initSubmit:function(){
				$("#savePassword").click(function(){

					form.on('submit(form)', function(){
						userApi.changePassword($('#changePasswordForm'),function(){
							Common.info("密码更改成功");
							Lib.closeFrame();
						});
					});

					
				});
				
				$("#savePassword-cancel").click(function(){
					Lib.closeFrame();
				});
			}
				
	}
	
	
	
	 exports('changePassword',view);
	
});