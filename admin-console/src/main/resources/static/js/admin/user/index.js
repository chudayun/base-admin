layui.define([ 'form', 'laydate', 'table','userApi' ,'upload','element'], function(exports) {
	var form = layui.form;
	var laydate = layui.laydate;
	var table = layui.table;
	var userApi=layui.userApi;
	var userTable = null;
	var upload = layui.upload;
	var element = layui.element
	
	var view ={
		
		init:function(){
			this.initTable();
			this.initSearchForm();
			this.initToolBar();
			this.initupload();
			window.dataReload = function(){
				Lib.doSearchForm($("#searchForm"),userTable)
			}
			element.render('progress');
			
		},
		initTable:function(){
			userTable = table.render({
				elem : '#userTable',
				height : Lib.getTableHeight(2),
				method : 'post',
				url : Common.ctxPath + '/admin/user/list.json' //数据接口
				,page : Lib.tablePage //开启分页
				,limit : 10,
				cols : [ [ //表头
				{
					type : 'checkbox',
					fixed:'left',
				}, {
					field : 'id',
					title : 'id',
					width : 80,
					fixed:'left',
					sort : true,
					hide:true
				}, {
					field : 'code',
					title : '用户名',
					width : 150
				}, {
					field : 'name',
					title : '姓名',
					width : 120,
					sort : true
				}, {
					field : 'orgName',
					title : '机构',
					width : 200,
					sort : true
				}, {
					field : 'stateText',
					title : '状态',
					width : 120,
					sort : true
				},
				 {
					field : 'jobType0Text',
					title : '职位',
					sort : true
				},
				 {
					field : 'jobType1Text',
					title : '职位明细',
					sort : true
				},
				{
					field : 'createTime',
					title : '创建时间',
					templet:function(d){
						return Common.getDate(d.createTime);
					},
					sort : true
				}
				] ]

			});
		},
		
		initSearchForm:function(){
			Lib.initSearchForm( $("#searchForm"),userTable,form);
		},
		initupload:function(){
			uploadListIns = upload.render({
				elem: '#exportImgs',
				url: "/admin/user/uploadAttachment.json",
				accept: 'file',
				exts: 'zip', //只允许上传压缩文件|rar|7z
				multiple: true,
				auto: true,
				data: {
					"batchFileUUID":'',
					"bizId":"",
					"bizType":"User"
				},
				before: function(obj){

				},
				done: function(res, index, upload) {
					if (res.code != 0) {
						this.error(index, upload);
						return
					} //上传成功
					element.progress('component-progress', '0%');
					return layer.msg('上传成功');
				},
				error: function(index, upload) {
					return layer.msg('上传失败');
				},
				progress: function(n, elem){
					var percent = n + '%' //获取进度百分比
					element.progress('component-progress', percent); //可配合 layui 进度条元素使用

					//以下系 layui 2.5.6 新增
					console.log(elem); //得到当前触发的元素 DOM 对象。可通过该元素定义的属性值匹配到对应的进度条。
				}
			});
		},
		initToolBar:function(){
			toolbar = {
					add : function() { //获取选中数据
						var url = "/admin/user/add.do";
						Common.openPage(url,"用户管理>新增",['70%','90%']);
					},
					edit : function() { //获取选中数目
						var data = Common.getOneFromTable(table,"userTable");
						if(data==null){
							return ;
						}
						var url = "/admin/user/edit.do?id="+data.id;
						Common.openPage(url,"用户管理>编辑",['70%','90%']);
						
					},
					del : function() { 
						layui.use(['del'], function(){
							  var delView = layui.del
							  delView.delBatch();
						});
					},
					userRole : function() { //获取选中数目
						var data = Common.getOneFromTable(table,"userTable");
						if(data==null){
							return ;
						}
						var url = "/admin/user/role/list.do?id="+data.id;
						Common.openDlg(url,"用户管理>"+data.name+">角色管理");
						
					},
					changePassword:function(){
						var data = Common.getOneFromTable(table,"userTable");
						if(data==null){
							return ;
						}
						var url = "/admin/user/changePassword.do?id="+data.id;
						Common.openPage(url,"用户管理>更改密码",['70%','50%']);

					},
					exportUsers:function(){
					    Common.openConfirm("确认要导出这些用户?",function(){
					        userApi.exportUsers($("#searchForm"),function(fileId){
					           Lib.download(fileId);
					        })
		                })

                    }
				};
			$('.ext-toolbar').on('click', function() {
				var type = $(this).data('type');
				toolbar[type] ? toolbar[type].call(this) : '';
			});
		}
	}

	 exports('index',view);
	
});