var userTable;

layui.use(['layer', 'jquery', 'contextMenu', 'admin', 'form', 'table'], function(){

    var $ = layui.jquery;
    var layer = layui.layer;
    // var zTree = layui.zTree;
    var zTree = $.fn.zTree;
    var admin = layui.admin;
    var form = layui.form;
    var contextMenu = layui.contextMenu;
    var table = layui.table;

    /**
     * refushNode：暂存需要刷新的节点
     */
    var refushNode;
    var zTreeObj;

    var rightMenu = [{
        icon: 'layui-icon layui-icon-addition',
        name: '增加子机构',
        click: function (e, node) {

            refushNode = node;

            if(node.id==0) {

                showEditModel(null, null);
            } else {

                showEditModel(null, node);
            }
        }
    }, {
        icon: 'layui-icon layui-icon-edit',
        name: '编辑机构',
        click: function (e, node) {

            refushNode = node.getParentNode();
            showEditModel(node, refushNode.id==0? null: refushNode);
        }
    }, {
        icon: 'layui-icon layui-icon-delete',
        name: '删除机构',
        click: function (e, node) {

            layer.confirm('确定要删除此机构吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (i) {

                layer.close(i);
                refushNode = node.getParentNode();
                var loadIndex = layer.load(2);

                $.ajax({

                    url: Common.ctxPath + '/admin/org/delete.json?ids=' + node.id,
                    type: 'post',
                    dataType: 'json'
                }).success(function(res) {
                    debugger;
                    layer.close(loadIndex);
                    if (0 === res.code) {
                        layer.msg(res.msg, {icon: 1});
                        zTreeObj.reAsyncChildNodes(refushNode, 'refresh', true);
                    } else {
                        layer.msg(res.msg, {icon: 2});
                    }
                }).error(function(res, textStatus, errorThrown){

                    layer.close(loadIndex);
                    layer.msg(res.responseJSON.msg, {icon: 2});
                });
            });
        }
    }, {
        icon: 'layui-icon layui-icon-refresh',
        name: '刷新机构',
        click: function (e, node) {
            zTreeObj.reAsyncChildNodes(node, 'refresh', true);
        }
    }];

    var setting = {
        view: {
            dblClickExpand: false
        },
        data: {
            simpleData: {
                enable: true,
                idKey: "id",
                pIdKey: "parentOrgId"
            }
        },
        async: {

            enable: true,
            autoParam: ["id"],
            url: Common.ctxPath + '/admin/org/listChild.json',
            dataFilter: function (treeId, parentNode, responseData) {

                for(var i=0; i<responseData.data.length; i++) {

                    responseData.data[i].isParent = true
                }

                return responseData.data;
            }
        },
        callback: {

            onClick: function(event, treeId, treeNode) {

                $("#userSearchForm-orgId").val(treeNode.id==0?null: treeNode.id);
                $("#userSearchForm-orgName").val(treeNode.id==0?null: treeNode.name);
                $("#userSearchForm-orgSeq").val(treeNode.seq||'');
                dataReload();
            },
            onRightClick: function (event, treeId, treeNode) {

                if (treeNode) {

                    if(treeNode.id == 0) {

                        contextMenu.show([rightMenu[3]], event.clientX, event.clientY, treeNode);
                    } else {

                        contextMenu.show(rightMenu, event.clientX, event.clientY, treeNode);
                    }
                }
            }
        }
    };

    var nodes = [
        {id: 0, parentOrgId: null, name: '组织机构', isParent: true}
    ];

    zTreeObj = zTree.init($("#orgTree"), setting, nodes);

    var node = zTreeObj.getNodeByTId("orgTree_1");
    zTreeObj.expandNode(node,true );

    userTable = table.render({
        elem : '#userTable',
        height : Lib.getTableHeight(2),
        method : 'post',
        url : Common.ctxPath + '/admin/user/list.json' //数据接口
        ,page : Lib.tablePage //开启分页
        ,limit : 10,
        cols : [ [ //表头
            {
                type : 'checkbox',
                fixed:'left',
            }, {
                field : 'id',
                title : 'id',
                width : 80,
                sort : true,
                hide:true
            }, {
                field : 'code',
                title : '用户名',
                width : 120,
                fixed:'left'
            }, {
                field : 'name',
                title : '姓名',
                width : 120,
                sort : true,
                fixed:'left'
            }, {
                field : 'orgName',
                title : '机构',
                width : 200,
                sort : true
            }, {
                field : 'stateText',
                title : '状态',
                width : 100,
                sort : true
            },
            {
                field : 'jobType0Text',
                title : '职位',
                width : 150,
                sort : true
            },
            {
                field : 'jobType1Text',
                title : '职位明细',
                sort : true
            },
            {
                field : 'createTime',
                title : '创建时间',
                templet:function(d){
                    return Common.getDate(d.createTime);
                },
                sort : true
            }
        ] ]
    });

    $(".ext-toolbar").click(function () {

        var type = $(this).data('type');
        userOp[type] ? userOp[type].call(this) : '';
    });

    var userOp = {

        addUser: function () {

            var selectOrgNodes = zTreeObj.getSelectedNodes();
            var orgId="";
            if(null!=selectOrgNodes && selectOrgNodes.length>0){
                orgId = (selectOrgNodes[0].id==0?'': selectOrgNodes[0].id);
            }
            var url = "/admin/user/add.do?orgId=" + orgId;
            Common.openPage(url,"用户管理>新增",['70%','80%']);
        },
        editUser: function () {

            var data = Common.getOneFromTable(table,"userTable");
            if(data==null){
                return ;
            }
            var url = "/admin/user/edit.do?id="+data.id;
            Common.openPage(url,"用户管理>编辑",['70%','80%']);
        },
        delUser: function () {

            var data = Common.getMoreDataFromTable(table,"userTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些用户?",function(){

                var ids =Common.concatBatchId(data);
                Common.post("/admin/user/delete.json", {
                    "ids" : ids
                }, function() {
                    Common.info("删除成功");
                    dataReload();
                });
            })
        }
    };

    function dataReload() {
        Lib.doSearchForm($("#userSearchForm"),userTable);
    }

    $("#userSearchBt").click(function () {

        Lib.doSearchForm($("#userSearchForm"),userTable);
    });

    function showEditModel(org, parent) {

        admin.open({
            type: 1,
            area: '600px',
            title: (org ? '修改' : '添加') + '机构',
            content: $('#organizationEditDialog').html(),
            success: function (layero, dIndex) {
                // 回显表单数据
                form.val('organizationEditForm', $.extend({}, org, {
                    parentOrgName: parent==null? null: parent.name,
                    parentOrgId: parent==null? null: parent.id
                }));
                // 表单提交事件
                form.on('submit(organizationEditSubmit)', function (data) {

                    var loadIndex = layer.load(2);

                    var saveUrl = Common.ctxPath + '/admin/org/save.json';

                    if(data.field.id && data.field.id.length>0) {

                        saveUrl = Common.ctxPath + '/admin/org/update.json';
                    }

                    $.post(saveUrl, data.field, function (res) {
                        layer.close(loadIndex);
                        if (0 === res.code) {
                            layer.close(dIndex);
                            layer.msg(res.msg, {icon: 1});
                            zTreeObj.reAsyncChildNodes(refushNode, 'refresh', true);
                        } else {
                            layer.msg(res.msg, {icon: 2});
                        }
                    }, 'json');
                    return false;
                });

                // 禁止弹窗出现滚动条
                $(layero).children('.layui-layer-content').css('overflow', 'visible');
            }
        });
    }
});

function dataReload() {
    Lib.doSearchForm($("#userSearchForm"),userTable);
}