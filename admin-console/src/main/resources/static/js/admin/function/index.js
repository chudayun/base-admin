layui.define([ 'form', 'laydate', 'table' ], function(exports) {
	var form = layui.form;
	var laydate = layui.laydate;
	var table = layui.table;

	var admin = layui.admin;
	var treeTable = layui.treeTable;
	var util = layui.util;
	var tbDataList = [];


	var functionTable = null;
	var view ={
		
		init:function(){
			this.initTable();
			this.initSearchForm();
			this.initToolBar();
			window.dataReload = function(){
				Lib.doSearchForm($("#functionSearchForm"),functionTable)
			}
			
			
		},
		initTable:function(){
			functionTable = treeTable.render({
				elem : '#functionTable',
				height : Lib.getTableHeight(2),
				method : 'post'
				,page : {"layout":['count','prev', 'page', 'next']} //开启分页
				,limit : 10
				,tree: {
					iconIndex: 2,
					isPidData: true,
					idName: 'id',
					pidName: 'parentId'
				},
				reqData: function (data, callback) {
					//console.log("1=",data);
					data = $("#functionSearchForm").serializeJson();
					console.log("2=",data);
					$.post(Common.ctxPath + '/admin/function/list.json',data, function (res) {
						//console.log("res=",res);
						res.data = res.data ? res.data : [];
						for (var i = 0; i < res.data.length; i++) {
							//res.data[i]["open"] = true;
						}
						callback(res.data);
					});
				},
				cols :  [ //表头
				{
					type : 'numbers',
					fixed:'left',
				}, 
				// {
				// 	field : 'id',
				// 	title : 'id',
				// 	width : 80,
				// 	fixed:'left',
				// 	sort : true
				// },
					{
					field : 'code',
					title : '功能代码',
					width : 150
				}, {
					field : 'name',
					title : '功能名称',
					width : 250,
					sort : true
				}, {
					field : 'accessUrl',
					title : '访问地址',
					minWidth : 300,
					sort : true
				}, {
					field : 'parentFunctionText',
					title : '上一级功能',
					width : 120,
					sort : true
				},{
					field : 'typeText',
					title : '功能类型',
					width : 120,
					sort : true
				},
				{
					field : 'createTime',
					title : '创建时间',
					width : 120,
					templet:function(d){
						return Common.getDate(d.createTime);
					},
					sort : true
				}, {title: '操作', toolbar: '#functionTableOp', align: 'center', width: 200, minWidth: 200}

				]

			});

			/* 表格工具条点击事件 */
			treeTable.on('tool(functionTable)', function (obj) {
				var data = obj.data; // 获得当前行数据
				if (obj.event === 'edit') { // 修改
					if(data==null){
						return ;
					}

					var url = "/admin/function/edit.do?id="+data.id;
					Common.openDlg(url,"功能点管理>编辑");
				} else if (obj.event === 'del') { // 删除
					layui.use(['del'], function(){
						var delView = layui.del
						delView.delFunc(data);
					});
				} else if (obj.event === 'addChildren') { // 修改
					if(data==null){
						return ;
					}

					var url = "/admin/function/add.do";
					Common.openDlg(url,"功能点管理>新增",function(layero, index){
						var body = layer.getChildFrame('body', index);
						var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();

						body.find('input[name=parentId]').val(data.id);
						body.find('input[name=parentIdText]').val(data.name);
						// body.find('input[name=pName]').attr("disabled","disabled");
					});
				}
			});
		},
		
		initSearchForm:function(){
			Lib.initSearchForm( $("#functionSearchForm"),functionTable,form);
		},
		initToolBar:function(){
			toolbar = {
					add : function() { //获取选中数据
						var url = "/admin/function/add.do";
						Common.openDlg(url,"功能点管理>新增");
					},
					edit : function() { //获取选中数目
						var data = Common.getOneFromTable(table,"functionTable");
						if(data==null){
							return ;
						}
						var url = "/admin/function/edit.do?id="+data.id;
						Common.openDlg(url,"功能点管理>编辑");
						
					},
					del : function() { 
						layui.use(['del'], function(){
							  var delView = layui.del
							  delView.delBatch();
						});
					}
					
				};
			$('.ext-toolbar').on('click', function() {
				var type = $(this).data('type');
				toolbar[type] ? toolbar[type].call(this) : '';
			});
		}
	}

	 exports('index',view);
	
});