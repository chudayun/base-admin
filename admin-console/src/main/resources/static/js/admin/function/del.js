layui.define(['table', 'functionApi'], function(exports) {
	var functionApi = layui.functionApi;
	var table=layui.table;
	var view = {
			init:function(){
				
			},
			delBatch:function(){
				var data = Common.getMoreDataFromTable(table,"functionTable");
				if(data==null){
					return ;
				}
				Common.openConfirm("确认要删除这些Function?",function(){
					var ids =Common.concatBatchId(data);
					functionApi.del(ids,function(){
						Common.info("删除成功");
						dataReload();
					})
				})
				
			},
			delFunc:function(data){

				if(data==null){
					return ;
				}
				Common.openConfirm("确认要删除此功能吗?",function(){
					var ids =data.id
					functionApi.del(ids,function(){
						Common.info("删除成功");
						dataReload();
					})
				})

			}
				
	}
	 exports('del',view);
	
});