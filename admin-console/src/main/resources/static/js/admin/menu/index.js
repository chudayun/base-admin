layui.define([ 'form', 'laydate', 'table' ], function(exports) {
	var form = layui.form;
	var laydate = layui.laydate;
	var table = layui.table;

	var admin = layui.admin;
	var treeTable = layui.treeTable;
	var util = layui.util;
	var tbDataList = [];

	var menuTable = null;
	var view ={
		
		init:function(){
			this.initTable();
			this.initSearchForm();
			this.initToolBar();
			window.dataReload = function(){
				Lib.doSearchForm($("#menuSearchForm"),menuTable)
			}
			
			
		},
		initTable:function(){
			menuTable = treeTable.render({
				elem : '#menuTable',
				height : Lib.getTableHeight(2),
				method : 'post'
				,tree: {
					iconIndex: 2,
					isPidData: true,
					idName: 'id',
					pidName: 'parentMenuId'
				},
				reqData: function (data, callback) {
					//console.log("1=",data);
					data = $("#menuSearchForm").serializeJson();
					console.log("2=",data);
					$.post(Common.ctxPath + '/admin/menu/list.json',data, function (res) {
						//console.log("res=",res);
						res.data = res.data ? res.data : [];
						for (var i = 0; i < res.data.length; i++) {
							//res.data[i]["open"] = true;
						}
						callback(res.data);
					});
				},
				cols : [ //表头
				{
					type : 'numbers',
					fixed:'left',
				},
				// {
				// 	field : 'id',
				// 	title : 'id',
				// 	width : 80,
				// 	fixed:'left',
				// 	sort : true
				// },
					{
					field : 'code',
					title : '菜单代码',
					width:120
				}, {
					field : 'name',
					title : '菜单名称',
						minWidth : 250
				}, {
					field : 'accessUrl',
					title : '菜单入口地址',
						minWidth : 100
				} , {
					field : 'icon',
					title : '图标',
						width:60,templet: '<p><i class="layui-icon {{d.icon}}"></i></p>', align: 'center'

				},{
					field : 'seq',
					title : '排序',
						width:60
				},
					{
						field : 'parentMenuName',
						title : '上一级菜单',
						width:120
					},
					{
					field : 'typeText',
					title : '菜单类型',
						width:80
				},
				{
					field : 'createTime',
					title : '创建时间',
					width:120,
					templet:function(d){
						return Common.getDate(d.createTime);
					}
				}, {title: '操作', toolbar: '#menuTableOp', align: 'center', width: 200, minWidth: 200}

				] ,
				done: function (data) {
					tbDataList = data;
				}
			});

			/* 表格工具条点击事件 */
			treeTable.on('tool(menuTable)', function (obj) {
				var data = obj.data; // 获得当前行数据
				if (obj.event === 'edit') { // 修改
					if(data==null){
						return ;
					}
					var url =  "/admin/menu/edit.do?id="+data.id;
					Common.openDlg(url,"菜单管理>编辑");
				} else if (obj.event === 'del') { // 删除
					layui.use(['del'], function(){
						var delView = layui.del
						delView.delMenu(data);
					});
				} else if (obj.event === 'addChildren') { // 修改
					if(data==null){
						return ;
					}

					var url = "/admin/menu/add.do";
					Common.openDlg(url,"菜单管理>新增",function(layero, index){
						var body = layer.getChildFrame('body', index);
						var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();

						body.find('input[name=parentMenuId]').val(data.id);
						body.find('input[name=parentMenuIdText]').val(data.name);
						// body.find('input[name=pName]').attr("disabled","disabled");
					});
				}
			});
		},
		
		initSearchForm:function(){
			Lib.initSearchForm( $("#menuSearchForm"),menuTable,form);
		},
		initToolBar:function(){
			toolbar = {
					add : function() { //获取选中数据
						var url = "/admin/menu/add.do";
						Common.openDlg(url,"菜单管理>新增");
					},
					edit : function() { //获取选中数目
						var data = Common.getOneFromTable(table,"menuTable");
						if(data==null){
							return ;
						}
						var url = "/admin/menu/edit.do?id="+data.id;
						Common.openDlg(url,"菜单管理>编辑");
						
					},
					del : function() { 
						layui.use(['del'], function(){
							  var delView = layui.del
							  delView.delBatch();
						});
					}
					
				};
			$('.ext-toolbar').on('click', function() {
				var type = $(this).data('type');
				toolbar[type] ? toolbar[type].call(this) : '';
			});
		}
	}

	 exports('index',view);
	
});