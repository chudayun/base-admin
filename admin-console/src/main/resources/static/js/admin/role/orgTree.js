var userTable = null;
layui.define(['layer', 'jquery','table'], function(exports){

    var $ = layui.jquery;
    var zTree = $.fn.zTree;//layui.zTree;
    var table = layui.table;

    var view ={
        init:function(){
            this.initTable();
            this.initZTree();

            this.initButton();
            window.dataReload = function(){
                Lib.doSearchForm($("#userSearchForm"),userTable)
            }
        },
        initZTree:function(){

            var setting = {
                view: {
                    dblClickExpand: false
                },
                data: {
                    simpleData: {
                        enable: true,
                        idKey: "id",
                        pIdKey: "parentOrgId"
                    }
                },
                async: {

                    enable: true,
                    autoParam: ["id"],
                    url: Common.ctxPath + '/admin/org/listChild.json',
                    dataFilter: function (treeId, parentNode, responseData) {

                        for(var i=0; i<responseData.data.length; i++) {

                            responseData.data[i].isParent = true
                        }

                        return responseData.data;
                    }
                },
                callback: {

                    onClick: function(event, treeId, treeNode) {

                        $("#userSearchForm-orgId").val(treeNode.id==0?null: treeNode.id);
                        $("#userSearchForm-orgName").val(treeNode.name);

                        dataReload();
                    }

                }
            };

            var nodes = [
                {id: 0, parentOrgId: null, name: '机构管理树', isParent: true}
            ];

            zTree.init($("#orgTree"), setting, nodes);
        },
        initTable:function(){
            var data = $("#userSearchForm").serializeJson();
            userTable = table.render({
                elem : '#userTable',
                height : Lib.getTableHeight(),
                method : 'post',
                where: data ,
                url : Common.ctxPath + '/admin/user/role/getUnUserList.json' //数据接口
                ,page : Lib.tablePage //开启分页
                ,limit : 10,
                cols : [ [ //表头
                    {
                        type : 'checkbox',
                        fixed:'left',
                    }, {
                        field : 'id',
                        title : 'id',
                        width : 80,
                        fixed:'left',
                        sort : true,
                        hide :true
                    }, {
                        field : 'code',
                        title : '用户名'
                    }, {
                        field : 'name',
                        title : '姓名',
                        sort : true
                    }, {
                        field : 'orgName',
                        title : '机构',
                        sort : true
                    }, {
                        field : 'stateText',
                        title : '状态',
                        sort : true
                    },
                    {
                        field : 'jobType0Text',
                        title : '职位',
                        sort : true
                    },
                    {
                        field : 'jobType1Text',
                        title : '职位明细',
                        sort : true
                    }
                ] ]
            });

        },
        dataReload:function(){
            Lib.doSearchForm($("#userSearchForm"),userTable);
        },
        initButton:function(){
            $("#userSearchBt").click(function () {

                Lib.doSearchForm($("#userSearchForm"),userTable);
            });
            $("#userSearchRest").click(function () {
                $("#userSearchForm-orgId").val(null);
                $("#userSearchForm-orgName").val(null);
                $("#userSearchForm-roleId").val(null);
                Lib.doSearchForm($("#userSearchForm"),userTable);
            });

        }

    }
    exports('orgTree',view);

});

function dataReload(){
    Lib.doSearchForm($("#userSearchForm"),userTable);
}