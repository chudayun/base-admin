var userRoleTable = null;
layui.define([ 'form', 'laydate', 'table' ], function(exports) {
	var form = layui.form;
	var laydate = layui.laydate;
	var table = layui.table;

	var roleId = null;
	var roleName = null;
	var view ={
		
		init:function(id,name){
			roleId = id;
			roleName = name;
			this.initTable();
			this.initSearchForm();
			this.initToolBar();
			
			$("#close").click(function(){
				Lib.closeFrame();
			});
			window.dataReload = function(){
				Lib.doSearchForm($("#userRoleSearchForm"),userRoleTable)
			}
			
		},
		initTable:function(){
			userRoleTable = table.render({
				elem : '#userRoleTable',
				height : Lib.getTableHeight(),
				method : 'post',
				url : Common.ctxPath + '/admin/role/role/users.json?roleId='+roleId //数据接口
				,page : false
				,limit : 10000,
				cols : [ [ //表头
				{
					type : 'checkbox',
					fixed:'left',
				},{
					field : 'id',
					width : 50,
					hide :true
				},{
					field : 'usercode',
					title : '账号',
					sort : true
				},{
					field : 'username',
					title : '名称',
					sort : true
				},{
					field : 'orgname',
					title : '机构名称',

				}

				] ]

			});
		},
		
		initSearchForm:function(){
			Lib.initSearchForm( $("#userRoleSearchForm"),userRoleTable,form);
		},
		initToolBar:function(){
			toolbar = {
					add : function() { //获取选中数据
						var url = "/admin/role/addUser.do?id="+roleId;
						Common.openPage(url,"角色管理>"+roleName+">新增角色用户",['80%','100%']);
					},
					del : function() { 
						layui.use(['del'], function(){
							  var delView = layui.del;
							  delView.delUserBatch();
						});
					}
				};
			$('.ext-toolbar').on('click', function() {
				var type = $(this).data('type');
				toolbar[type] ? toolbar[type].call(this) : '';
			});
		}
	}

	 exports('userRole',view);
	
});

function dataReload(){
	Lib.doSearchForm($("#userRoleSearchForm"),userRoleTable);
}