layui.define([ 'form', 'laydate', 'table','roleApi'], function(exports) {
	var form = layui.form;
	var roleApi = layui.roleApi;
	var index = layui.index;
	var table = layui.table;
	var view = {
			init:function(){
				this.initSubmit();
			},
			initSubmit:function(){
				$("#userRoleAdd").click(function() {
					var data = Common.getMoreDataFromTable(table, "userTable");

					if (data == null) {
						return;
					}
					var roleId = $("#userSearchForm-roleId").val();
					var datas = [];
					$.each(data, function (i, v) {
						var obj = {};
						obj.orgId = v.orgId;
						obj.roleId = roleId;
						obj.userId = v.id;
						datas.push(obj);
					})
					roleApi.addUser2RoleBatch(datas, function () {
						parent.dataReload();
						Common.info("添加成功");
						Lib.closeFrame();

					})
				});




				$("#saveUserRole-cancel").click(function(){
					Lib.closeFrame();
				});
			}
				
	}
	 exports('userRoleAdd',view);
	
});