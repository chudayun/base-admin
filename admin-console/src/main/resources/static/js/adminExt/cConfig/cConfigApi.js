/*访问后台的代码*/
layui.define([], function(exports) {
    var api={
            updateCConfig:function(form,callback){
                Lib.submitForm("/adminExt/cConfig/edit.json",form,{},callback)
            },
            addCConfig:function(form,callback){
                Lib.submitForm("/adminExt/cConfig/add.json",form,{},callback)
            },
            del:function(ids,callback){
                Common.post("/adminExt/cConfig/delete.json",{"ids":ids},function(){
                    callback();
                })
            }
		
    };
    exports('cConfigApi',api);
});