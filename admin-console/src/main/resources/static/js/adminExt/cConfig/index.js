layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cConfigTable = null;
    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cConfigTable)
            }
        },
        initTable:function(){
            cConfigTable = table.render({
                elem : '#cConfigTable',
                height : Lib.getTableHeight(1),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cConfig/list.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头
                    {
                        type : 'checkbox',
                        fixed:'left',
                    },
                // {
                //
                //     field : 'id',
                //         title : 'id',
                //     fixed:'left',
                //         width : 60,
                // },
                {

                    field : 'typeText', //数据字典类型为 c_config_type 1 client 2 管理端
                    title : '类别',
                },
                {

                    field : 'catalogText', //数据字典类型为 c_config_catalog
                    title : '目录',
                },
                {

                    field : 'name', 
                        title : '名称',
                },
                {

                    field : 'code', 
                        title : '代码',
                },
                {

                    field : 'value', 
                        title : '值',
                },

                // {
                //
                //     field : 'createTime',
                //         title : 'createTime',
                // },
                // {
                //
                //     field : 'createId',
                //         title : 'createId',
                // },
                // {
                //
                //     field : 'updateId',
                //         title : 'updateId',
                // },
                // {
                //
                //     field : 'updateTime',
                //         title : 'updateTime',
                // },
                // {
                //
                //     field : 'tenantId',
                //         title : 'tenantId',
                // }

        ] ]

        });

            table.on('checkbox(cConfigTable)', function(obj){
                var cConfig = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })
        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cConfigTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cConfig/add.do";
                    Common.openDlg(url,"系统配置管理>新增");
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cConfigTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cConfig/edit.do?id="+data.id;
                    Common.openDlg(url,"系统配置管理>"+data.name+">编辑");
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
        };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('index',view);

});