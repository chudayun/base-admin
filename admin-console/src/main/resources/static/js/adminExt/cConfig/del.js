layui.define(['table', 'cConfigApi'], function(exports) {
    var cConfigApi = layui.cConfigApi;
    var table=layui.table;
    var view = {
        init:function(){
        },
        delBatch:function(){
            var data = Common.getMoreDataFromTable(table,"cConfigTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些系统配置?",function(){
            var ids =Common.concatBatchId(data,"id");
            cConfigApi.del(ids,function(){
                Common.info("删除成功");
                    dataReload();
                })
            })
        }
    }
    exports('del',view);
	
});