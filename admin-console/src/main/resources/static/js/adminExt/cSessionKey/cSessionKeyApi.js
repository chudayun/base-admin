/*访问后台的代码*/
layui.define([], function(exports) {
    var api={
            updateCSessionKey:function(form,callback){
                Lib.submitForm("/adminExt/cSessionKey/edit.json",form,{},callback)
            },
            addCSessionKey:function(form,callback){
                Lib.submitForm("/adminExt/cSessionKey/add.json",form,{},callback)
            },
            del:function(ids,callback){
                Common.post("/adminExt/cSessionKey/delete.json",{"ids":ids},function(){
                    callback();
                })
            }
		
    };
    exports('cSessionKeyApi',api);
});