layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cSessionKeyTable = null;
    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cSessionKeyTable)
            }
        },
        initTable:function(){
            cSessionKeyTable = table.render({
                elem : '#cSessionKeyTable',
                height : Lib.getTableHeight(1),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cSessionKey/list.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头
                    {
                        type : 'checkbox',
                        fixed:'left',
                    },
//                {
//
//                    field : 'id', 
//                        title : '主键',
//                    fixed:'left',
//                        width : 60,
//                },
                {

                    field : 'sessionkey', 
                        title : '会话key',
                },
                {

                    field : 'userId', 
                        title : '用户ID',
                },
                {

                    field : 'userCode', 
                        title : '用户编码',
                },
                {

                    field : 'userName', 
                        title : '用户姓名',
                },
                {

                    field : 'loginTime', 
                        title : '登录时间',
                },
                // {
                //
                //     field : 'statusText', //数据字典类型为 user_state
                //         title : '状态 ',
                // },
                {

                    field : 'clientType', 
                        title : '客户端类别',
                },
//                {
//
//                    field : 'clientName', 
//                        title : 'clientName',
//                },
//                {
//
//                    field : 'updateTime', 
//                        title : 'updateTime',
//                },
                {

                    field : 'tenantId', 
                        title : 'tenantId',
                }

        ] ]

        });

            table.on('checkbox(cSessionKeyTable)', function(obj){
                var cSessionKey = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })
        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cSessionKeyTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cSessionKey/add.do";
                    Common.openDlg(url,"会话信息管理>新增");
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cSessionKeyTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cSessionKey/edit.do?id="+data.id;
                    Common.openDlg(url,"会话信息管理>"+data.id+">编辑");
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
        };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('index',view);

});