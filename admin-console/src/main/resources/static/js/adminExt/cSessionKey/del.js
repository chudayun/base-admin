layui.define(['table', 'cSessionKeyApi'], function(exports) {
    var cSessionKeyApi = layui.cSessionKeyApi;
    var table=layui.table;
    var view = {
        init:function(){
        },
        delBatch:function(){
            var data = Common.getMoreDataFromTable(table,"cSessionKeyTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些会话信息?",function(){
            var ids =Common.concatBatchId(data,"id");
            cSessionKeyApi.del(ids,function(){
                Common.info("删除成功");
                    dataReload();
                })
            })
        }
    }
    exports('del',view);
	
});