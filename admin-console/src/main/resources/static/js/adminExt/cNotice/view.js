layui.define([ 'form'], function(exports) {
    var form = layui.form;
    var cNoticeApi = layui.cNoticeApi;
    var index = layui.index;
    
//    tinymce.init({
//        selector: '#eQuestionContentEdt',
//        height: 525,
//        branding: false,
//        language: 'zh_CN',
//        plugins: 'code print preview fullscreen paste searchreplace save autosave link autolink image imagetools media table codesample lists advlist hr charmap emoticons anchor directionality pagebreak quickbars nonbreaking visualblocks visualchars wordcount',
//        toolbar: 'fullscreen preview code | undo redo | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | formatselect fontselect fontsizeselect | link image media emoticons charmap anchor pagebreak codesample | ltr rtl',
//        toolbar_drawer: 'sliding',
//        images_upload_url: '../../../json/tinymce-upload-ok.json',
//        file_picker_types: 'media',
//        file_picker_callback: function (callback, value, meta) {
//            layer.msg('演示环境不允许上传', {anim: 6});
//        }
//    });
    var view = {
        init:function(){
	        Lib.initGenrealForm($("#updateForm"),form);
	        this.initSubmit();
        },
        initSubmit:function(){
            $("#updateButton-cancel").click(function(){
                Lib.closeFrame();
            });
        }
            
    }
    exports('view',view);
	
});