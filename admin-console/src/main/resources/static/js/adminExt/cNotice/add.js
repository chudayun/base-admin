layui.define([ 'form', 'laydate', 'table','cNoticeApi'], function(exports) {
    var form = layui.form;
    var cNoticeApi = layui.cNoticeApi;
    var index = layui.index;
    var layedit = layui.layedit;
    //var editIndex = layedit.build('eQuestionContentEdt',{uploadImage:{url: '"/adminExt/cNotice/addImg.json"', type: 'post'}}); // 建立编辑器

 // 渲染富文本编辑器
    tinymce.init({
        selector: '#eQuestionContentEdt',
        height: 525,
        branding: false,
        language: 'zh_CN',
        plugins: 'code print preview fullscreen paste searchreplace save autosave link autolink image imagetools media table codesample lists advlist hr charmap emoticons anchor directionality pagebreak quickbars nonbreaking visualblocks visualchars wordcount',
        toolbar: 'fullscreen preview code | undo redo | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | formatselect fontselect fontsizeselect | link image media emoticons charmap anchor pagebreak codesample | ltr rtl',
        toolbar_drawer: 'sliding',
        images_upload_url: '../../../json/tinymce-upload-ok.json',
        file_picker_types: 'media',
        file_picker_callback: function (callback, value, meta) {
            layer.msg('演示环境不允许上传', {anim: 6});
        }
    });

    var view = {
        init:function(){
            Lib.initGenrealForm($("#addForm"),form);
            this.initSubmit();
        },
        initSubmit:function(){
            $("#addButton").click(function(){
                 form.on('submit(form)', function(data){
//                	 console.log("111",data);
//                	 console.log("222",$('#addForm').serializeJson());
//                	 console.log(editIndex)
//                	 console.debug("333",layedit.getContent(editIndex));
                	 //layedit.sync(editIndex);
//                	 console.log("444",$('#addForm').serializeJson());
                	 
                	 var content = tinymce.get('eQuestionContentEdt').getContent();
                     //layer.alert(content, {skin: 'layui-layer-admin', shade: .1});
                	 $("#eQuestionContentEdt").val(content);
                     cNoticeApi.addCNotice($('#addForm'),function(){
                         parent.window.dataReload();
                         Common.info("添加成功");
                         Lib.closeFrame();
                     });
                	 return false;
                });
            });
        
            $("#addButton-cancel").click(function(){
                Lib.closeFrame();
            });
        }
    			
    }
    exports('add',view);
});