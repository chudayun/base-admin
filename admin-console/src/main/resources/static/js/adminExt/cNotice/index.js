layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cNoticeTable = null;
    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cNoticeTable)
            }
        },
        initTable:function(){
            cNoticeTable = table.render({
                elem : '#cNoticeTable',
                height : Lib.getTableHeight(1),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cNotice/list.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头
                    {
                        type : 'checkbox',
                        fixed:'left',
                    },
//                {
//
//                    field : 'id', 
//                        title : 'id',
//                    fixed:'left',
//                        width : 60,
//                },
                {

                    	title : '标题', 
                    field : 'title',
                },
//                {
//
//                	title : '内容', 
//                    field : 'content',
//                },
//                {
//
//                	title : '网址', 
//                    field : 'url',
//                },
                {

                    title : '发布者', 
                    field : 'author',
                },
                {

                	title : '创建人', 
                    field : 'createName',
                },
                
                {

                	title : '创建时间', 
                    field : 'createTime',
                },
                {title: '操作', toolbar: '#cNoticeTableBar', align: 'center', width: 100, minWidth: 100}
                
        ] ]

        });

            table.on('checkbox(cNoticeTable)', function(obj){
                var cNotice = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })
            /* 表格工具条点击事件 */
            table.on('tool(cNoticeTable)', function (obj) {
                var data = obj.data; // 获得当前行数据
                if (obj.event === 'view') { // 删除
                	if(data==null){
                        return ;
                    }
                    var url = Common.ctxPath + "/adminExt/cNotice/view.do?id="+data.id;
                    Common.openDlg(url,"查看");
                }
            });
        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cNoticeTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cNotice/add.do";
                    Common.openDlg(url,"系统通知管理>新增");
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cNoticeTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cNotice/edit.do?id="+data.id;
                    Common.openDlg(url,"系统通知管理>"+data.title+">编辑");
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
        };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('index',view);

});