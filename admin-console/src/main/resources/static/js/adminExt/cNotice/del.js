layui.define(['table', 'cNoticeApi'], function(exports) {
    var cNoticeApi = layui.cNoticeApi;
    var table=layui.table;
    var view = {
        init:function(){
        },
        delBatch:function(){
            var data = Common.getMoreDataFromTable(table,"cNoticeTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些系统通知?",function(){
            var ids =Common.concatBatchId(data,"id");
            cNoticeApi.del(ids,function(){
                Common.info("删除成功");
                    dataReload();
                })
            })
        }
    }
    exports('del',view);
	
});