/*访问后台的代码*/
layui.define([], function(exports) {
    var api={
            updateCNotice:function(form,callback){
                Lib.submitForm("/adminExt/cNotice/edit.json",form,{},callback)
            },
            addCNotice:function(form,callback){
                Lib.submitForm("/adminExt/cNotice/add.json",form,{},callback)
            },
            del:function(ids,callback){
                Common.post("/adminExt/cNotice/delete.json",{"ids":ids},function(){
                    callback();
                })
            }
		
    };
    exports('cNoticeApi',api);
});