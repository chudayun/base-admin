layui.define(['table', 'cOpenAppApi'], function(exports) {
    var cOpenAppApi = layui.cOpenAppApi;
    var table=layui.table;
    var view = {
        init:function(){
        },
        delBatch:function(){
            var data = Common.getMoreDataFromTable(table,"cOpenAppTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些应用?",function(){
            var ids =Common.concatBatchId(data,"id");
            cOpenAppApi.del(ids,function(){
                Common.info("删除成功");
                    dataReload();
                })
            })
        }
    }
    exports('del',view);
	
});