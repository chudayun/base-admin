layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cOpenAppTable = null;
    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cOpenAppTable)
            }
        },
        initTable:function(){
            cOpenAppTable = table.render({
                elem : '#cOpenAppTable',
                height : Lib.getTableHeight(1),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cOpenApp/list.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头
                    {
                        type : 'checkbox',
                        fixed:'left',
                    },
//                {
//
//                    field : 'id',
//                        title : 'id',
//                    fixed:'left',
//                        width : 60,
//                },
                    {

                        field : 'appId',
                        title : 'appId',
                    },
                    {

                        field : 'appName',
                        title : 'appName',
                    },
                    {

                        field : 'appSecret',
                        title : 'appSecret',
                    },
//                {
//
//                    field : 'publicKey',
//                        title : 'publicKey',
//                },
//                {
//
//                    field : 'privateKey',
//                        title : 'privateKey',
//                },

                    {

                        field : 'remark',
                        title : 'remark',
                    },
                    {

                        field : 'rulesText', //数据字典类型为 c_app_encryption_rules
                        title : '加密规则',
                    },
//                {
//
//                    field : 'createId',
//                        title : 'createId',
//                },
//                {
//
//                    field : 'createTime',
//                        title : 'createTime',
//                },

                ] ]

            });

            table.on('checkbox(cOpenAppTable)', function(obj){
                var cOpenApp = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })
        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cOpenAppTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cOpenApp/add.do";
                    Common.openDlg(url,"应用管理>新增");
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cOpenAppTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cOpenApp/edit.do?id="+data.id;
                    Common.openDlg(url,"应用管理>"+data.id+">编辑");
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
            };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('index',view);

});