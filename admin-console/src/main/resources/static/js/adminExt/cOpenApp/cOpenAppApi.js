/*访问后台的代码*/
layui.define([], function(exports) {
    var api={
            updateCOpenApp:function(form,callback){
                Lib.submitForm("/adminExt/cOpenApp/edit.json",form,{},callback)
            },
            addCOpenApp:function(form,callback){
                Lib.submitForm("/adminExt/cOpenApp/add.json",form,{},callback)
            },
            del:function(ids,callback){
                Common.post("/adminExt/cOpenApp/delete.json",{"ids":ids},function(){
                    callback();
                })
            }
		
    };
    exports('cOpenAppApi',api);
});