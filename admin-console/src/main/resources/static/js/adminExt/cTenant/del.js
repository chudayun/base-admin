layui.define(['table', 'cTenantApi'], function(exports) {
    var cTenantApi = layui.cTenantApi;
    var table=layui.table;
    var view = {
        init:function(){
        },
        delBatch:function(){
            var data = Common.getMoreDataFromTable(table,"cTenantTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些租户?",function(){
            var ids =Common.concatBatchId(data,"id");
            cTenantApi.del(ids,function(){
                Common.info("删除成功");
                    dataReload();
                })
            })
        }
    }
    exports('del',view);
	
});