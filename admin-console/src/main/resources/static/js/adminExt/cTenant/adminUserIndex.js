layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cTenantTable = null;
    var admin = layui.admin;

    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cTenantTable)
            }
        },
        initTable:function(){
            cTenantTable = table.render({
                elem : '#cTenantTable',
                height : Lib.getTableHeight(2),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cTenant/listAdminUser.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头

                {

                    field : 'id', 
                        title : 'id',
                    fixed:'left',
                        width : 60,
                    hide:true
                },
                    {

                        field : 'tenantName',
                        title : '租户名称',
                    },
                {

                    field : 'name', 
                        title : '用户名',
                },
                {

                    field : 'code',
                        title : '登录帐号',
                },
                {

                    field : 'createTime', //数据字典类型为 c_tenant_status
                        title : '创建时间',
                },
                    {title: '操作', toolbar: '#userTbBar', align: 'center', minWidth: 200}

        ] ]

        });

            table.on('checkbox(cTenantTable)', function(obj){
                var cTenant = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })

            /* 表格工具条点击事件 */
            table.on('tool(cTenantTable)', function (obj) {
                console.log("click:" ,obj);
                if (obj.event === 'edit') { // 修改

                } else if (obj.event === 'changeTenantRole') { // 删除
                    admin.open({
                        type: 1,
                        title: '修改角色',
                        content: $('#changeTenantRoleDialog').html(),
                        success: function (layero, dIndex) {
                            // 回显表单数据
                            obj.data["userId"] = obj.data["id"];
                            form.val('changeTenantRoleForm', obj.data);
                            // 表单提交事件
                            form.on('submit(changeTenantRoleFormSubmit)', function (data) {
                                // data.field.roleIds =
                                console.log(data);
                                var loadIndex = layer.load(2);
                                Common.post("/adminExt/cTenant/changeAdminRole.json",data.field,function(data){
                                    console.log("成功");
                                    layer.close(loadIndex);
                                    layer.close(dIndex);
                                    window.dataReload();
                                },function (res) {
                                    layer.close(loadIndex);
                                    Common.error(res.msg);
                                })

                                return false;
                            });

                            // 禁止弹窗出现滚动条
                            $(layero).children('.layui-layer-content').css('overflow', 'visible');
                        }
                    });

                } else if (obj.event === 'resetTenantAdminPwd') { // 重置密码

                    var url = "/admin/user/changePassword.do?id="+obj.data.id;
                    Common.openPage(url,"用户管理>更改密码",['70%','50%']);
                }
            });

        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cTenantTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cTenant/add.do";
                    Common.openPage(url,"租户管理>新增",["70%","85%"]);
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cTenantTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cTenant/edit.do?id="+data.id;
                    Common.openPage(url,"租户管理>"+data.name+">编辑",["70%","85%"]);
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
        };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('adminUserIndex',view);

});