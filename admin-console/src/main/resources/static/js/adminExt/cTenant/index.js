layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cTenantTable = null;
    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cTenantTable)
            }
        },
        initTable:function(){
            cTenantTable = table.render({
                elem : '#cTenantTable',
                height : Lib.getTableHeight(2),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cTenant/list.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头
                    {
                        type : 'checkbox',
                        fixed:'left'

                    },
                {

                    field : 'id', 
                        title : 'id',
                    fixed:'left',
                        width : 60,
                    hide:true
                },
                {

                    field : 'name', 
                        title : '租户名称',
                },
                {

                    field : 'subdomain', 
                        title : '二级域名',
                },
                {

                    field : 'domain', 
                        title : '自有域名',
                },
                {

                    field : 'statusText', //数据字典类型为 c_tenant_status
                        title : '租户状态',
                },
                {

                    field : 'creditCode', 
                        title : '统一社会信用代码',
                },
                {

                    field : 'createName', 
                        title : '租户创建人',
                }
        ] ]

        });

            table.on('checkbox(cTenantTable)', function(obj){
                var cTenant = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })
        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cTenantTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cTenant/add.do";
                    Common.openPage(url,"租户管理>新增",["70%","85%"]);
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cTenantTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cTenant/edit.do?id="+data.id;
                    Common.openPage(url,"租户管理>"+data.name+">编辑",["70%","85%"]);
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
        };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('index',view);

});