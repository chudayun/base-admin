/*访问后台的代码*/
layui.define([], function(exports) {
    var api={
            updateCTenant:function(form,callback){
                Lib.submitForm("/adminExt/cTenant/edit.json",form,{},callback)
            },
            addCTenant:function(form,callback){
                Lib.submitForm("/adminExt/cTenant/add.json",form,{},callback)
},
            del:function(ids,callback){
                Common.post("/adminExt/cTenant/delete.json",{"ids":ids},function(){
                    callback();
                })
            }
		
    };
    exports('cTenantApi',api);
});