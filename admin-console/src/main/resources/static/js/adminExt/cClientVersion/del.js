layui.define(['table', 'cClientVersionApi'], function(exports) {
    var cClientVersionApi = layui.cClientVersionApi;
    var table=layui.table;
    var view = {
        init:function(){
        },
        delBatch:function(){
            var data = Common.getMoreDataFromTable(table,"cClientVersionTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些客户端版本?",function(){
            var ids =Common.concatBatchId(data,"id");
            cClientVersionApi.del(ids,function(){
                Common.info("删除成功");
                    dataReload();
                })
            })
        }
    }
    exports('del',view);
	
});