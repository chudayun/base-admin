layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cClientVersionTable = null;
    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cClientVersionTable)
            }
        },
        initTable:function(){
            cClientVersionTable = table.render({
                elem : '#cClientVersionTable',
                height : Lib.getTableHeight(1),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cClientVersion/list.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头
                    {
                        type : 'checkbox',
                        fixed:'left',
                    },
                 {

                    field : 'appIdText',
                        title : 'APPID',
                    // fixed:'left',
                        width : 150,
                },
                 {

                    field : 'versionCode', 
                        title : '版本号',
                     width : 80,
                },
                 {

                    field : 'versionName', 
                        title : '版本名称',
                     width : 100,
                },
                 {

                    field : 'versionInfo', 
                        title : '版本更新说明',
                },
                 {

                    field : 'forceUpdateText', //数据字典类型为 BASE_IS_YES_OR_NO
                        title : '强制更新',
                     width : 100,

                },
                 {

                    field : 'downloadUrl', 
                        title : '下载地址',
                },
                 {

                    field : 'osTypeText', //数据字典类型为 c_client_ostype
                        title : '操作系统',
                     width : 100,
                },
                 {

                    field : 'packageTypeText', //数据字典类型为 c_client_package_type
                        title : '更新包类别',
                     width : 100,
                }

        ] ]

        });

            table.on('checkbox(cClientVersionTable)', function(obj){
                var cClientVersion = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })
        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cClientVersionTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cClientVersion/add.do";
                    Common.openDlg(url,"客户端版本管理>新增");
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cClientVersionTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cClientVersion/edit.do?id="+data.id;
                    Common.openDlg(url,"客户端版本管理>"+data.versionName+">编辑");
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
        };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('index',view);

});