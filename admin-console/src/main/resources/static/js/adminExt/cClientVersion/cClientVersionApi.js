/*访问后台的代码*/
layui.define([], function(exports) {
    var api={
            updateCClientVersion:function(form,callback){
                Lib.submitForm("/adminExt/cClientVersion/edit.json",form,{},callback)
            },
            addCClientVersion:function(form,callback){
                Lib.submitForm("/adminExt/cClientVersion/add.json",form,{},callback)
            },
            del:function(ids,callback){
                Common.post("/adminExt/cClientVersion/delete.json",{"ids":ids},function(){
                    callback();
                })
            }
		
    };
    exports('cClientVersionApi',api);
});