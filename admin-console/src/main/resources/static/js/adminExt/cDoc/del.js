layui.define(['table', 'cDocApi'], function(exports) {
    var cDocApi = layui.cDocApi;
    var table=layui.table;
    var view = {
        init:function(){
        },
        delBatch:function(){
            var data = Common.getMoreDataFromTable(table,"cDocTable");
            if(data==null){
                return ;
            }
            Common.openConfirm("确认要删除这些系统文档?",function(){
            var ids =Common.concatBatchId(data,"id");
            cDocApi.del(ids,function(){
                Common.info("删除成功");
                    dataReload();
                })
            })
        }
    }
    exports('del',view);
	
});