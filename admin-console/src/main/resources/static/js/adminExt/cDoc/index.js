layui.define([ 'form', 'laydate', 'table' ], function(exports) {
    var form = layui.form;
    var laydate = layui.laydate;
    var table = layui.table;
    var cDocTable = null;
    var view ={
        init:function(){
            this.initTable();
            this.initSearchForm();
            this.initToolBar();
            window.dataReload = function(){
                Lib.doSearchForm($("#searchForm"),cDocTable)
            }
        },
        initTable:function(){
            cDocTable = table.render({
                elem : '#cDocTable',
                height : Lib.getTableHeight(1),
                cellMinWidth: 100,
                method : 'post',
                url : Common.ctxPath + '/adminExt/cDoc/list.json' // 数据接口
                ,page : Lib.tablePage // 开启分页
                ,limit : 10,
                cols : [ [ // 表头
                    {
                        type : 'checkbox',
                        fixed:'left',
                    },
//                {
//
//                    field : 'id', 
//                        title : 'id',
//                    fixed:'left',
//                        width : 60,
//                },
                {

                    field : 'title', 
                        title : '标题',
                },
//                {
//
//                    field : 'thumbnail', 
//                        title : 'thumbnail',
//                },
//                {
//
//                    field : 'url', 
//                        title : 'url',
//                },
//                {
//
//                    field : 'content', 
//                        title : 'content',
//                },
                {

                    field : 'typeText', //数据字典类型为 c_doc_type
                        title : '文档类别',
                },
                {

                    field : 'remark', 
                        title : '描述',
                },
                {

                    field : 'catalog', 
                        title : '目录',
                },
                {

                    field : 'createId', 
                        title : '创建人',
                },
                {

                    field : 'createTime', 
                        title : '创建时间',
                },

        ] ]

        });

            table.on('checkbox(cDocTable)', function(obj){
                var cDoc = obj.data;
                if(obj.checked){
                    //按钮逻辑Lib.buttonEnable()
                }else{

                }
            })
        },

        initSearchForm:function(){
            Lib.initSearchForm( $("#searchForm"),cDocTable,form);
        },
        initToolBar:function(){
            toolbar = {
                add : function() { // 获取选中数据
                    var url = "/adminExt/cDoc/add.do";
                    Common.openDlg(url,"系统文档管理>新增");
                },
                edit : function() { // 获取选中数目
                    var data = Common.getOneFromTable(table,"cDocTable");
                    if(data==null){
                        return ;
                    }
                    var url = "/adminExt/cDoc/edit.do?id="+data.id;
                    Common.openDlg(url,"系统文档管理>"+data.title+">编辑");
                },
                del : function() {
                    layui.use(['del'], function(){
                        var delView = layui.del
                        delView.delBatch();
                    });
                }
        };
            $('.ext-toolbar').on('click', function() {
                var type = $(this).data('type');
                toolbar[type] ? toolbar[type].call(this) : '';
            });
        }
    }
    exports('index',view);

});