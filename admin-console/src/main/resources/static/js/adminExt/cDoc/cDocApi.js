/*访问后台的代码*/
layui.define([], function(exports) {
    var api={
            updateCDoc:function(form,callback){
                Lib.submitForm("/adminExt/cDoc/edit.json",form,{},callback)
            },
            addCDoc:function(form,callback){
                Lib.submitForm("/adminExt/cDoc/add.json",form,{},callback)
            },
            del:function(ids,callback){
                Common.post("/adminExt/cDoc/delete.json",{"ids":ids},function(){
                    callback();
                })
            }
		
    };
    exports('cDocApi',api);
});