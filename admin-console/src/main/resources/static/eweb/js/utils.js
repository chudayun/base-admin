var AppUtils = function () { }
AppUtils.dateFmt = function (input,fmt){ 
    var sdate = "1970.01.01 00:00:00";
    var time = new Date(sdate).getTime();
    var date = new Date();
    date.setTime(time + input * 1000);

    var o = {   
        "M+" : date.getMonth()+1,                 //月份   
        "d+" : date.getDate(),                    //日   
        "H+" : date.getHours(),                   //小时   
        "m+" : date.getMinutes(),                 //分   
        "s+" : date.getSeconds(),                 //秒   
        "q+" : Math.floor((date.getMonth()+3)/3), //季度   
        "S"  : date.getMilliseconds()             //毫秒   
    };   
    if(/(y+)/.test(fmt))   
        fmt=fmt.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length)); 

    if(/(h+)/.test(fmt)){
        var hour=date.getHours();
        if(hour>12){
            hour = hour-12;
        }else if(hour==0){
            hour=12; 	
    }
    fmt=fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (hour) : (("00"+ hour).substr((""+ hour).length)));  
    } 
        
    for(var k in o){
        if(new RegExp("("+ k +")").test(fmt))
        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length))); 
    }      
    return fmt; 
}

AppUtils.dateFmt2 = function (input,fmt){ 
	var timezone = 8; //目标时区时间，东八区
	var offset_GMT = new Date().getTimezoneOffset(); // 本地时间和格林威治的时间差，单位为分钟
	var nowDate = new Date().getTime(); // 本地时间距 1970 年 1 月 1 日午夜（GMT 时间）之间的毫秒数
	var targetDate = new Date(nowDate + offset_GMT * 60 * 1000 + timezone * 60 * 60 * 1000);
	console.log("东8区现在是：" + targetDate);
	//数据转化  
	function formatNumber(n) {  
	  n = n.toString()  
	  return n[1] ? n : '0' + n  
	}  
	/** 
	 * 时间戳转化为年 月 日 时 分 秒 
	 * number: 传入时间戳 
	 * format：返回格式，支持自定义，但参数必须与formateArr里保持一致 
	*/  
	  
	  var formateArr  = ['Y','M','D','h','m','s'];  
	  var returnArr   = [];  
	  
	  var date = new Date(input * 1000);  
	  returnArr.push(date.getFullYear());  
	  returnArr.push(formatNumber(date.getMonth() + 1));  
	  returnArr.push(formatNumber(date.getDate()));  
	  
	  returnArr.push(formatNumber(date.getHours()));  
	  returnArr.push(formatNumber(date.getMinutes()));  
	  returnArr.push(formatNumber(date.getSeconds()));  
	  
	  for (var i in returnArr)  
	  {  
	    format = format.replace(formateArr[i], returnArr[i]);  
	  }  
	  return format;  

}


AppUtils.changeSizeUnit = function (limit){
    var size = "";
    if(limit < 0.1 * 1024){                            //小于0.1KB，则转化成B
        size = limit.toFixed(2) + "B"
    }else if(limit < 1 * 1024 * 1024){            //小于0.1MB，则转化成KB
        size = (limit/1024).toFixed(2) + "KB"
    }else if(limit < 0.1 * 1024 * 1024 * 1024){        //小于0.1GB，则转化成MB
        size = (limit/(1024 * 1024)).toFixed(2) + "MB"
    }else{                                            //其他转化成GB
        size = (limit/(1024 * 1024 * 1024)).toFixed(2) + "GB"
    }

    var sizeStr = size + "";                        //转成字符串
    var index = sizeStr.indexOf(".");                    //获取小数点处的索引
    var dou = sizeStr.substr(index + 1 ,2)            //获取小数点后两位的值
    if(dou == "00"){                                //判断后两位是否为00，如果是则删除00                
        return sizeStr.substring(0, index) + sizeStr.substr(index + 3, 2)
    }
    return size;
}


AppUtils.compare =  function (propertyName) {
  return function (object1, object2) {
    var value1 = object1[propertyName];
    var value2 = object2[propertyName];
    if (value2 < value1) {
      return -1;
    } else if (value2 > value1) {
      return 1;
    } else {
      return 0;
    }
  }
}

AppUtils.getQueryString =  function  (name) {
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
  var r = window.location.search.substr(1).match(reg);
  if (r != null) return unescape(r[2]);
  return null;
}

AppUtils.getUrlParams =  function(url) {
  let params = {}
  let white = ['id'];
  if (!url) return params;
  if (url.indexOf("?") !== -1) {
    const index = url.indexOf("?");
    url = url.substr(index + 1);
    let list = url.split('&');
    list.forEach(ele => {
      let dic = ele.split('=');
      let label = dic[0];
      let value = dic[1];
      if (white.indexOf(label) === -1) {
        params[label] = value;
      }

    })
  }
  return params
}
AppUtils.loadJs =  function(url, callback) {
  var script = document.createElement('script');
  script.type = "text/javascript";
  if (typeof (callback) != "undefined") {
    if (script.readyState) {
      script.onreadystatechange = function () {
        if (script.readyState == "loaded" || script.readyState == "complete") {
          script.onreadystatechange = null;
          callback();
        }
      }
    } else {
      script.onload = function () {
        var params = getUrlParams(window.location.href);
        config.query = Object.assign((config.query || {}), params);
        callback();
      }
    }
  }
  script.src = url;
  document.body.appendChild(script);
}
AppUtils.initData =  function (callback) {
  var id = getQueryString('id');
  if (id) {
    loadJs('./data/data' + id + '.js', callback);
  }
  callback();
}

AppUtils.stringify =  function (json) {
  let count = 0;
  let list = [];
  let str = JSON.stringify(json, function (key, val) {
    if (typeof val === 'function') {
      list.push(val + '');
      const result = '$code{' + count + '}$code'
      count = count + 1;
      return result
    }
    return val;
  }, 2);
  let startCode = '$code{';
  let endCode = '}$code';
  list.forEach((ele, index) => {
    str = str.replace(startCode + index + endCode, startCode + ele + endCode)
  })
  for (let i = 0; i < count; i++) {
    str = str.replace('\"' + startCode, '').replace(endCode + '\"', '')
  }
  return str
}

AppUtils.parse =  function  (str) {
  return JSON.parse(str, function (k, v) {
    if (v.indexOf && v.indexOf('function') > -1) {
      return eval("(function(){return " + v + " })()")
    }
    return v;
  });
}
AppUtils.addUrlParam =  function (url, param, value) {
  if (!url) return
  function GetQueryString(name) {
    var num = url.indexOf("?")
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = url.substr(num + 1).match(reg);
    if (r != null) {
      return unescape(r[2]);
    }
    return null;
  };
  function replaceParamVal(paramName, value) {
    var re = eval('/(' + paramName + '=)([^&]*)/gi');
    return url.replace(re, paramName + '=' + value);
  }
  if (GetQueryString(param)) {
    return replaceParamVal(param, value)
  } else {
    if (url.includes('?')) {
      url = url + '&' + param + '=' + value
    } else {
      url = url + '?' + param + '=' + value
    }
    return url;
  }

}
