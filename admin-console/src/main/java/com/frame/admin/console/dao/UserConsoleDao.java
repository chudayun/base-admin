package com.frame.admin.console.dao;

import com.frame.admin.console.web.query.UserQuery;
import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.entity.CoreUserRole;
import com.frame.admin.core.util.enums.GeneralStateEnum;

import java.util.List;

@SqlResource("console.user")
public interface UserConsoleDao extends BaseMapper<CoreUser> {

    PageQuery<CoreUser> queryByCondtion(PageQuery<CoreUser> query);

    void batchDelUserByIds( List<Long> ids);

    void batchUpdateUserState(List<Long> ids, GeneralStateEnum state);
    
    List<CoreUserRole> queryUserRole( Long id,Long orgId,Long roleId);

    List<CoreUser> queryNuByCondtion(UserQuery query, Long roleId);

    @Sql("update core_user set attachment_id=? where id=?")
    public void updateAttachmentId(String AttachmentId,Long id);

}

