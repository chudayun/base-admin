package com.frame.admin.console.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;

/**
 *CmsBlog查询
 */
public class CmsBlogQuery extends PageParam {
    @Query(name = "id", display = true)
    private Integer id;
    public Integer getId(){
        return  id;
    }
    public void setId(Integer id ){
        this.id = id;
    }
 
}
