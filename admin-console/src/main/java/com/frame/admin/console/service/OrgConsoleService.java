package com.frame.admin.console.service;

import java.util.ArrayList;
import java.util.List;

import com.frame.admin.core.entity.BaseSessionResultQuery;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.query.LambdaQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.frame.admin.console.dao.OrgConsoleDao;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.rbac.tree.OrgItem;
import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.util.PlatformException;

/**
 * 
 * @author : xiandafu
 */
@Service
@Transactional
public class OrgConsoleService extends CoreBaseService<CoreOrg> {

    @Autowired
    private OrgConsoleDao orgDao;

    @Autowired
    private CorePlatformService platformService;

    /**
     * 根据条件查询
     * @param query
     */
    public void queryByCondtion(PageQuery<CoreOrg> query) {
        orgDao.queryByCondtion(query);
        List<CoreOrg> list = query.getList();
        queryListAfter(list);
        OrgItem root = platformService.buildOrg();
        //处理父机构名称显示，没有用sql查询是考虑到跨数据库
        for(CoreOrg org:list) {
        	Long parentId = org.getParentOrgId();
        	OrgItem item = root.findChild(parentId);
        	String name = item!=null?item.getName():"";
        	org.set("parentOrgText", name);
        }
    }
    
    public void queryUserByCondition(PageQuery<CoreUser> query) {
    	orgDao.queryUserByCondtion(query);
    	queryListAfter(query.getList());
    }


    /**
     * 获取机构下面的所以机构
     * @param orgId 机构id
     */
    public List<Long> getAllChildIdsByOrgId(Long orgId) {
        if (orgId == null)
            return null;

        OrgItem orgItem = platformService.buildOrg().findChild(orgId);
        if (orgItem == null) {
            return null;
        }
        List<Long> ids = orgItem.findAllChildrenId();
        if (ids == null) {
            ids = new ArrayList<>();
        }
        ids.add(orgId);

        return ids;
    }
    
    @Override
    public boolean deleteById(List<Long> ids) {
    	OrgItem root = platformService.buildOrg();
        //检查子节点
    	
        for (Long id : ids) {
        	OrgItem child = root.findChild(id);
        	if(child.getChildren().size()!=0){
        		throw new PlatformException("不能删除 "+child.getOrg().getName()+",还包含子机构");
        	}
        }
        return super.deleteById(ids);
    }

    /**
     * 根据父级ID获取下级机构
     * @param parentId
     * @return
     */
    public List<CoreOrg> listByParentId(Long parentId) {

        BaseSessionResultQuery q = platformService.getBaseSession();


        LambdaQuery lq = null;
        if(parentId==null || parentId==0) {
           lq = orgDao.createLambdaQuery()
                    .andIsNull(CoreOrg::getParentOrgId)
                    .andEq(CoreOrg::getDelFlag, 0)
                    .asc(CoreOrg::getId);
        } else {

            lq = orgDao.createLambdaQuery()
                    .andEq(CoreOrg::getParentOrgId, parentId)
                    .andEq(CoreOrg::getDelFlag, 0)
                    .asc(CoreOrg::getId);
        }

        return lq.andEq("tenant_id",q.getTenantId()).select();
//        if(q.getSupperAdmin() && q.getTenantId() .equals(platformService.TENANT_DEFAULT_ID) ){
//            return lq.select();
//        }else if(q.getSupperAdmin()){
//            return lq.andEq("tenant_id",q.getTenantId()).select();
//        }else{
//            return lq.andEq("tenant_id",q.getTenantId()).andLike("seq","%."+q.getOrgId()+".%").select();
//        }
    }


    public void saveOrg(CoreOrg org){
        BaseSessionResultQuery q = platformService.getBaseSession();

        org.setTenantId(q.getTenantId());
        super.save(org);
        if(org.getParentOrgId() != null){
            CoreOrg porg = super.queryById(org.getParentOrgId());
            org.setSeq(porg.getSeq()+org.getId()+".");
        }else{
            org.setSeq("."+org.getId()+".");
        }
        super.update(org);
    }

}
