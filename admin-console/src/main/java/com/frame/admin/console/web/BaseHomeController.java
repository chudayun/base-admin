package com.frame.admin.console.web;

import com.frame.admin.core.service.CorePlatformService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


/**
 *
 */
@Controller
public class BaseHomeController {

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/base/home";

    
    
    @Autowired
	CorePlatformService platformService;



    @GetMapping(MODEL + "/index.do")
    @ResponseBody
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/baseIndex.html") ;

        //判断租户有无绑定自定义域名，
        view.addObject("tenant", platformService.getCurrentTenant());
        return view;
    }






}
