package com.frame.admin.api;

import com.frame.admin.ext.service.CSessionKeyService;
import com.frame.admin.console.service.OrgConsoleService;
import com.frame.admin.core.annotation.SessionKeyCheck;
import com.frame.admin.core.conf.PasswordConfig;
import com.frame.admin.core.entity.CoreDict;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.rbac.UserLoginInfo;
import com.frame.admin.core.service.CoreDictService;
import com.frame.admin.core.service.CoreUserService;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 基础信息接口
 */
@Controller
public class CCommonController {

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/api/common";



    @Autowired
    private CoreDictService coreDictService;

    @Autowired
    private OrgConsoleService orgConsoleService;

    @Autowired
    FileService fileService;

    @Autowired
    CoreUserService userService;

    @Autowired
    private CSessionKeyService cSessionKeyService;

    @Autowired
    PasswordConfig.PasswordEncryptService passwordEncryptService;

    /* ajax json */

    @PostMapping(MODEL + "/dict.json")
    @ResponseBody
    public JsonResult<List<CoreDict>> dictList(String type)
    {
        return JsonResult.success(coreDictService.findAllByType(type));
    }



    /**
     * 根据组织机构ID获取子组织列表
     *
     * @param id 组织机构ID
     * @return
     */
    @PostMapping(MODEL + "/listChild.json")
    @SessionKeyCheck()
    @ResponseBody
    public JsonResult<List<CoreOrg>> list(Long id) {

        return JsonResult.success(orgConsoleService.listByParentId(id));
    }


    /**
     * 密码修改
     * @param request
     * @param code
     * @param opassword
     * @param npassword
     * @return
     */
    @RequestMapping(MODEL + "/resetPassword.json")
    @SessionKeyCheck()
    @ResponseBody
    public JsonResult resetPassword(HttpServletRequest request,String code, String opassword, String npassword) {

        String imei = request.getHeader("imei");
        try{
            UserLoginInfo info = userService.login(code, opassword);


            CoreUser user = info.getUser();

            user.setPassword(passwordEncryptService.password(npassword));
            userService.update(user);
        }catch (Exception e){
            return JsonResult.failMessage("原始密码错误，请确认后操作");
        }
        //更新同设备ID 的sessionkey 为无效状态

        cSessionKeyService.updateStatus(imei);
        return JsonResult.success();
    }


}
