package com.frame.admin.api;

import com.frame.admin.ext.entity.CConfig;
import com.frame.admin.ext.entity.CTenant;
import com.frame.admin.ext.service.CConfigService;
import com.frame.admin.ext.service.CTenantService;
import com.frame.admin.ext.web.query.CConfigQuery;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

/**
 * 租户 接口
 */
@Controller
public class CTenantApiController {

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/api/cTenant";

    @Autowired
    Environment env;

    @Autowired private CTenantService cTenantService;

    @Autowired private CConfigService cConfigService;
    
    @Autowired
    FileService fileService;

    @RequestMapping(MODEL + "/setting.json")
    @ResponseBody
    public JsonResult<CTenant>queryInfo(CTenant cTenant) {

        cTenant = cTenantService.queryById( cTenant.getId());


        if(null!=cTenant.getName()){
            CConfigQuery query = new CConfigQuery();
            query.setType(2);//APP端配置
            query.setTenantId(cTenant.getId());
            query.setDelFlag(0);
            List<CConfig> list = cConfigService.queryTenantCfgByCondition(query);
            HashMap<String,String> tenantCfg = new HashMap<String,String>();

            tenantCfg.put("websocketUrl",env.getProperty("tenant.websocketUrl"));

            for (int i = 0; i <list.size() ; i++) {
                CConfig cfg = list.get(i);
                tenantCfg.put(cfg.getCode(),cfg.getValue());
            }


            cTenant.set("tenantCfg",tenantCfg);
        }

        cTenant.setAdminCode(null);
        cTenant.setAdminName(null);
        return  JsonResult.success(cTenant);
    }
}
