package com.frame.admin.api;

import com.frame.admin.ext.entity.CClientVersion;
import com.frame.admin.ext.service.CClientVersionService;
import com.frame.admin.core.file.FileItem;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 租户 接口
 */
@Controller
public class CAppApiController {

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/api/app";


    @Autowired private CClientVersionService cClientVersionService;


    @Autowired
    FileService fileService;

    @Autowired
    Environment env;

    @RequestMapping(MODEL + "/update.json")
    @ResponseBody
    public JsonResult<CClientVersion> update(CClientVersion cClientVersion) {

        //appid, //appversion , //平台

        cClientVersion = cClientVersionService.getLatestVersion(cClientVersion);


        if(null!=cClientVersion && null!=cClientVersion.getVersionName()){
            List<FileItem> files = fileService.queryByBatchId(cClientVersion.getAttachmentId());//fileService.queryByBiz("CClientVersion",String.valueOf(cClientVersion.getId()));
            String downUrl = "";
            if(null!=files && files.size()>0){
                downUrl = files.get(0).getPath();
                //downUrl = downUrl.substring(0,downUrl.lastIndexOf("."));
            }

            //file.download.baseurl
            String baseurl = env.getProperty("file.download.baseurl");
            System.out.println("downUrl = ===========" + baseurl + downUrl);


            cClientVersion.setDownloadUrl(baseurl + downUrl);
        }

        return  JsonResult.success(cClientVersion);
    }

    @GetMapping(MODEL + "/latest.do")
    @ResponseBody
    public ModelAndView latest(CClientVersion cClientVersion) {
        ModelAndView view = new ModelAndView("/adminExt/cClientVersion/latest.html");

        //appid, //appversion , //平台
        cClientVersion = cClientVersionService.getLatestVersion(cClientVersion);

        if(null!=cClientVersion && null!=cClientVersion.getVersionName()){
            List<FileItem> files = fileService.queryByBatchId(cClientVersion.getAttachmentId());//fileService.queryByBiz("CClientVersion",String.valueOf(cClientVersion.getId()));
            String downUrl = "";
            if(null!=files && files.size()>0){
                downUrl = files.get(0).getPath();
            }
            //file.download.baseurl
            String baseurl = env.getProperty("file.download.baseurl");
            System.out.println("downUrl = ===========" + baseurl + downUrl);

            cClientVersion.setDownloadUrl(baseurl + downUrl);
        }
        view.addObject("cClientVersion", cClientVersion);
        return view;
    }
}
