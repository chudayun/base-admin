CREATE TABLE `c_client_version` (
  `id` bigint(20) NOT NULL,
  `version_code` int(11) NOT NULL,
  `app_id` varchar(200) DEFAULT NULL,
  `version_name` varchar(100) NOT NULL,
  `version_info` varchar(2000) DEFAULT NULL,
  `force_update` int(11) DEFAULT NULL COMMENT '1强制更新',
  `download_url` varchar(4000) DEFAULT NULL,
  `os_type` int(11) NOT NULL COMMENT '1安卓，2IOS',
  `package_type` varchar(100) DEFAULT NULL,
  `attachment_id` varchar(50) DEFAULT NULL,
  `del_flag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户端版本管理';

CREATE TABLE `c_config` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `code` varchar(100) NOT NULL,
  `value` varchar(1000) NOT NULL,
  `catalog` varchar(50) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '类别，0 不可变，1 client 2 管理端',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `create_id` bigint(20) DEFAULT NULL,
  `update_id` bigint(20) DEFAULT NULL,
  `update_time` timestamp NULL DEFAULT NULL,
  `del_flag` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `tenant_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统配置';

CREATE TABLE `c_doc` (
  `id` bigint(20) NOT NULL,
  `title` varchar(200) NOT NULL,
  `thumbnail` varchar(1000) DEFAULT NULL,
  `url` varchar(1000) DEFAULT NULL,
  `content` text,
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '0 文档，1 视频，2链接',
  `remark` varchar(5000) DEFAULT NULL,
  `catalog` bigint(20) NOT NULL DEFAULT '0',
  `create_id` bigint(20) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `del_flag` tinyint(4) DEFAULT '0',
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统文档';

CREATE TABLE `c_notice` (
  `id` bigint(20) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text,
  `url` varchar(1000) DEFAULT NULL,
  `create_id` bigint(20) NOT NULL,
  `create_name` varchar(100) NOT NULL,
  `author` varchar(100) NOT NULL,
  `create_time` datetime NOT NULL,
  `del_flag` tinyint(4) NOT NULL DEFAULT '0',
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统通知公告';

CREATE TABLE `c_open_app` (
  `id` bigint(20) NOT NULL,
  `app_id` varchar(32) NOT NULL,
  `app_secret` varchar(5000) DEFAULT NULL,
  `public_key` text,
  `private_key` text,
  `app_name` varchar(100) NOT NULL,
  `remark` int(11) DEFAULT NULL,
  `rules` int(11) NOT NULL DEFAULT '0' COMMENT '0 简单，1 复杂',
  `create_id` bigint(20) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `del_flag` tinyint(4) NOT NULL DEFAULT '0',
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_id` (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='开放应用管理';

CREATE TABLE `c_session_key` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sessionkey` varchar(100) NOT NULL DEFAULT '',
  `user_id` bigint(20) NOT NULL,
  `user_code` varchar(50) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `login_time` bigint(20) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '有效状态：0有效、1失效',
  `client_type` varchar(50) DEFAULT NULL,
  `client_name` varchar(100) DEFAULT NULL,
  `del_flag` int(11) DEFAULT '0',
  `update_time` bigint(20) DEFAULT NULL,
  `tenant_id` bigint(20) DEFAULT NULL,
  `imei` varchar(64) DEFAULT NULL COMMENT '手机设备号',
  `phone_model` varchar(32) DEFAULT NULL COMMENT '手机型号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1300265595039645697 DEFAULT CHARSET=utf8;

CREATE TABLE `c_tenant` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '租户名称',
  `subdomain` varchar(100) NOT NULL COMMENT '二级域名',
  `domain` varchar(500) DEFAULT NULL COMMENT '自有域名',
  `status` int(11) DEFAULT '0' COMMENT '状态，正常、欠费、注销、停用',
  `credit_code` text,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '租户创建时间',
  `create_name` varchar(100) NOT NULL COMMENT '租户创建人姓名',
  `create_id` bigint(20) DEFAULT NULL COMMENT '租户创建人id（内部时）',
  `admin_code` varchar(50) DEFAULT NULL COMMENT '管理员账号',
  `admin_name` varchar(50) DEFAULT NULL COMMENT '管理员名称',
  `del_flag` tinyint(4) DEFAULT '0' COMMENT '删除标识 1删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `subdomain` (`subdomain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='租户信息表';

CREATE TABLE `c_tenant_cfg` (
  `tenant_id` bigint(20) NOT NULL,
  `login_title` varchar(100) DEFAULT NULL COMMENT '登录标题',
  `login_bg_img` varchar(2000) DEFAULT NULL COMMENT '登录背景图片',
  `app_name` varchar(1000) DEFAULT NULL,
  `cp_right` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE `core_audit` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FUNCTION_CODE` varchar(45) DEFAULT NULL,
  `FUNCTION_NAME` varchar(45) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `USER_NAME` varchar(45) DEFAULT NULL,
  `IP` varchar(45) DEFAULT NULL,
  `CREATE_TIME` datetime(6) DEFAULT NULL,
  `SUCCESS` tinyint(4) DEFAULT NULL,
  `MESSAGE` varchar(250) DEFAULT NULL,
  `ORG_ID` varchar(45) DEFAULT NULL,
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1261 DEFAULT CHARSET=utf8;

CREATE TABLE `core_dict` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUE` varchar(16) NOT NULL,
  `NAME` varchar(128) NOT NULL COMMENT '名称',
  `TYPE` varchar(64) NOT NULL COMMENT '字典编码',
  `TYPE_NAME` varchar(64) NOT NULL COMMENT '类型描述',
  `SORT` int(6) DEFAULT NULL COMMENT '排序',
  `PARENT` int(64) DEFAULT NULL COMMENT '父id',
  `DEL_FLAG` int(6) DEFAULT NULL COMMENT '删除标记',
  `REMARK` varchar(255) DEFAULT NULL COMMENT '备注',
  `CREATE_TIME` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`),
  KEY `idx_code` (`TYPE`),
  KEY `idx_pid` (`PARENT`),
  KEY `idx_value` (`VALUE`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='字典表';

CREATE TABLE `core_file` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) DEFAULT NULL COMMENT '文件名称',
  `PATH` varchar(255) DEFAULT NULL COMMENT '路径',
  `BIZ_ID` varchar(128) DEFAULT NULL COMMENT '业务ID',
  `USER_ID` int(20) DEFAULT NULL COMMENT '上传人id',
  `CREATE_TIME` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `ORG_ID` int(20) DEFAULT NULL,
  `BIZ_TYPE` varchar(128) DEFAULT NULL,
  `FILE_BATCH_ID` varchar(128) DEFAULT NULL,
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8 COMMENT='文件表';

CREATE TABLE `core_file_tag` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `KEY` varchar(64) NOT NULL COMMENT 'key，关键字',
  `VALUE` varchar(255) NOT NULL COMMENT '关键字对应的值',
  `FILE_ID` int(20) NOT NULL COMMENT 'sys_file的id，文件id',
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文件标签';

CREATE TABLE `core_function` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(250) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `CREATE_TIME` datetime(6) DEFAULT NULL,
  `ACCESS_URL` varchar(250) DEFAULT NULL,
  `PARENT_ID` int(65) DEFAULT NULL,
  `TYPE` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=341 DEFAULT CHARSET=utf8;

CREATE TABLE `core_menu` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(100) DEFAULT NULL,
  `NAME` varchar(100) DEFAULT NULL,
  `CREATE_TIME` datetime(6) DEFAULT NULL,
  `FUNCTION_ID` int(6) DEFAULT NULL,
  `TYPE` varchar(6) DEFAULT NULL COMMENT '1,系统，2 导航 3 菜单项（对应某个功能点）',
  `PARENT_MENU_ID` int(65) DEFAULT NULL,
  `SEQ` int(65) DEFAULT NULL,
  `ICON` varchar(255) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

CREATE TABLE `core_org` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(16) NOT NULL,
  `NAME` varchar(16) NOT NULL,
  `CREATE_TIME` datetime(6) DEFAULT NULL,
  `PARENT_ORG_ID` int(20) DEFAULT NULL,
  `TYPE` varchar(6) NOT NULL COMMENT '1 公司，2 部门，3 小组',
  `DEL_FLAG` tinyint(6) DEFAULT NULL,
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  `SEQ` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

CREATE TABLE `core_role` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `NAME` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `CREATE_TIME` datetime(6) DEFAULT NULL COMMENT '创建时间',
  `TYPE` varchar(6) DEFAULT NULL COMMENT '1 可以配置 2 固定权限角色',
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`),
  KEY `code_idx` (`CODE`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8;

CREATE TABLE `core_role_function` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` int(65) DEFAULT NULL,
  `FUNCTION_ID` int(65) DEFAULT NULL,
  `DATA_ACCESS_TYPE` tinyint(65) DEFAULT NULL,
  `DATA_ACCESS_POLICY` varchar(128) DEFAULT NULL,
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=479 DEFAULT CHARSET=utf8;

CREATE TABLE `core_role_menu` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` int(65) DEFAULT NULL,
  `MENU_ID` int(65) DEFAULT NULL,
  `CREATE_TIME` datetime(6) DEFAULT NULL,
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=258 DEFAULT CHARSET=utf8;

CREATE TABLE `core_user` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(16) DEFAULT NULL,
  `NAME` varchar(16) DEFAULT NULL,
  `PASSWORD` varchar(64) DEFAULT NULL,
  `CREATE_TIME` datetime(6) DEFAULT NULL,
  `ORG_ID` int(65) DEFAULT NULL,
  `STATE` varchar(16) DEFAULT NULL COMMENT '用户状态 1:启用 0:停用',
  `JOB_TYPE1` varchar(16) DEFAULT NULL,
  `DEL_FLAG` tinyint(6) DEFAULT NULL COMMENT '用户删除标记 0:未删除 1:已删除',
  `update_Time` datetime DEFAULT NULL,
  `JOB_TYPE0` varchar(16) DEFAULT NULL,
  `attachment_id` varchar(128) DEFAULT NULL,
  `tenant_admin` int(11) DEFAULT NULL COMMENT '1表示该用户是租户超级管理员',
  `tenant_id` bigint(20) NOT NULL DEFAULT '9999',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=187 DEFAULT CHARSET=utf8;

CREATE TABLE `core_user_role` (
  `ID` int(20) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(20) DEFAULT NULL,
  `ROLE_ID` int(20) DEFAULT NULL,
  `ORG_ID` int(20) DEFAULT NULL,
  `CREATE_TIME` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8 COMMENT='用户角色关系表';

INSERT INTO `c_client_version` (`id`, `version_code`, `app_id`, `version_name`, `version_info`, `force_update`, `download_url`, `os_type`, `package_type`, `attachment_id`, `del_flag`) VALUES ('1296632111926607871','99','__UNI__2C7940C','0.9.9','1，增加了XXX功能2，修复了XXXbug3，修复CCCCC4，增加了XXXXX5，增加了XXXXXXXX','20','','1','apk','123456','0');
INSERT INTO `c_client_version` (`id`, `version_code`, `app_id`, `version_name`, `version_info`, `force_update`, `download_url`, `os_type`, `package_type`, `attachment_id`, `del_flag`) VALUES ('1296632111926607872','100','__UNI__2C7940C','1.0.0','1，增加了XXX功能2，修复了XXXbug3，修复CCCCC4，增加了XXXXX5，增加了XXXXXXXX','20','','1','apk','123456','0');

INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293851235937419263','登录页面背景图片','login_bg_img','/eweb/assets/images/bg-login1.jpg','login','1','2020-08-13 18:05:48.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293851235937419264','登录页面背景图片','login_bg_img','https://img.zcool.cn/community/01bee358aff3e0a801219c772edf0a.jpg@1280w_1l_2o_100sh.jpg','login','1','2020-08-13 18:05:48.0','179',null,null,'0','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293851606776807423','系统名称','system_name','易予雏鹰','comm','1','2020-08-13 18:07:16.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293851606776807424','系统名称','system_name','智能授权管理平台','comm','1','2020-08-13 18:07:16.0','179',null,null,'0','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293851760716152831','版权声明信息','copyright_info','易予雏鹰','login','2','2020-08-13 18:07:53.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293851760716152832','版权声明信息','copyright_info','-','login','2','2020-08-13 18:07:53.0','179',null,null,'0','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293852886479929343','登录头部图片','login_bg_img','https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=3138535757,68285057&fm=15&gp=0.jpg','login','2','2020-08-13 18:12:21.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293852886479929344','登录头部图片','login_bg_img','https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597914565813&di=8d6754a30fc0b61f89a4f53ad59aaeae&imgtype=0&src=http%3A%2F%2Fhbimg.b0.upaiyun.com%2F1836d6fe37349fda45b866361f39ee0d51acfd184fa8b-gqTuAv_fw658','login','2','2020-08-13 18:12:21.0','179',null,null,'1','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293854852807393280','登录页面子标题','login_sub_title','安全、可追溯','login','1','2020-08-13 18:20:10.0','179',null,null,'0','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293854852807393281','登录页面子标题','login_sub_title','多租户基础开发框架','login','1','2020-08-13 18:20:10.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293855048324874240','登录页面标题','login_title','智能授权管理平台','login','1','2020-08-13 18:20:57.0','179',null,null,'0','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293855048324874241','登录页面标题','login_title','易予雏鹰','login','1','2020-08-13 18:20:57.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293855269129814015','页面系统标题','page_title','易予雏鹰','comm','1','2020-08-13 18:21:49.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1293855269129814016','页面系统标题','page_title','智能授权管理平台','comm','1','2020-08-13 18:21:49.0','179',null,null,'0','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1295991937475543040','版权声明信息','copyright_info','Copyright © 2020 易予雏鹰','comm','1','2020-08-19 15:52:11.0','1',null,null,'0','9999');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1296333386683514880','登录页面底部背景图','login_footer_img','https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=3296732341,2867591336&fm=26&gp=0.jpg','login','2','2020-08-20 14:28:59.0','179',null,null,'1','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1296363568664412160','登录头部图片','login_bg_img','https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597922207994&di=1af20f8166448a17b7c2480659c724b2&imgtype=0&src=http%3A%2F%2Fimg1.gtimg.com%2Frushidao%2Fpics%2Fhv1%2F20%2F108%2F1744%2F113431160.jpg','login','2','2020-08-20 16:28:55.0','179',null,null,'1','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1299223079347552256','钥匙申请授权审核开关(开：yes，关：no)','is_audit','yes','comm','1','2020-08-28 13:51:35.0','179',null,null,'0','1280074884243783680');
INSERT INTO `c_config` (`id`, `name`, `code`, `value`, `catalog`, `type`, `create_time`, `create_id`, `update_id`, `update_time`, `del_flag`, `tenant_id`) VALUES ('1299282220028002304','授权审核间隔时间（分）','audit_time','120','comm','1','2020-08-28 17:46:35.0','179',null,null,'0','1280074884243783680');

INSERT INTO `c_doc` (`id`, `title`, `thumbnail`, `url`, `content`, `type`, `remark`, `catalog`, `create_id`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1254658224343220224','设置录音笔时间','http://www.hezyun.cn/doc/help_settime.mp4.png','http://www.hezyun.cn/doc/help_settime.mp4','','1','通过录音转储客户端，设置录音笔时间','1','1','2020-04-27 00:00:00.0','0','9999');
INSERT INTO `c_doc` (`id`, `title`, `thumbnail`, `url`, `content`, `type`, `remark`, `catalog`, `create_id`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1254658391381377024','操作手册-完整版-录音转储分析系统','http://www.hezyun.cn/doc/help_system_all.pdf.png','http://www.hezyun.cn/pdf/web/viewer.html?file=/doc/help_system_all.pdf','','2','','1','1','2020-04-27 00:00:00.0','0','9999');
INSERT INTO `c_doc` (`id`, `title`, `thumbnail`, `url`, `content`, `type`, `remark`, `catalog`, `create_id`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1260478220201885696','数码录音笔E3','http://www.hezyun.cn/doc/help_e3v3.0.pdf.png','http://www.hezyun.cn/pdf/web/viewer.html?file=/doc/help_e3v3.0.pdf','1','2','录音笔','1','1','2020-05-13 00:00:00.0','0','9999');
INSERT INTO `c_doc` (`id`, `title`, `thumbnail`, `url`, `content`, `type`, `remark`, `catalog`, `create_id`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1260479122522505216','数码录音笔E3','http://www.hezyun.cn/doc/help_e3v3.0.pdf.png','http://www.hezyun.cn/doc/help_e3v3.0.pdf.png','','2','通过录音转储客户端，设置录音笔时间','1','1','2020-05-13 00:00:00.0','1','9999');

INSERT INTO `c_notice` (`id`, `title`, `content`, `url`, `create_id`, `create_name`, `author`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1254327037556948992','录音转储分析系统和 安全新机制系统对接完成','<p>[增加] 步骤条steps模块进行全面重写 [增加] 系统管理增加机构管理、字典管理模板页面[增加] url方式弹窗支持直接使用模板引擎语法(tpl:true控制是否开启)[增加] 表格搜索栏折叠/展开增加事件监听[增加] admin模块增加对alert、confirm、prompt弹层的封装[增加] tooltips增加背景图片、padding、文字颜色、字体大小配置，并支持js形式调用[增加] 提供侧边栏展开/折叠事件监听，可用于实现折叠状态记忆(admin模块)[增加] putTempData和getTempData方法可通过参数控制是永久存储还是临时存储[优化] ew-event="closeDialog"可智能判断弹层类型，并保留closeIframeDialog和closePageDialog[优化] 退出登录ew-event="logout"支持ajax方式的配置[优化] theme、note、psw、logout等ew-event支持同open一样的参数配置[优化] tableX绑定表格行鼠标右键支持无限级[优化] formX.renderSelect方法支持设置initValue回显数据[优化] admin.req方法支持智能设置contentType为json类型，支持更丰富ajax参数设置[优化] 用户管理、角色管理、登录等页面布局美化[优化] admin.popupRight支持随屏幕高度改变自适应[优化] 单标签模式主页也能显示出标题栏[优化] 打开标签页过多侧边栏展开/折叠卡顿问题[优化] 锁屏页面密码验证使用MD5密文验证方式[优化] xmSelect下拉树单选提供更好的样式[修复] 锁屏后新标签页打开不能恢复锁屏状态的bug[修复] cascader模块清除不触发onChange的bug[修复] tableX模块exportDataX的url方式导出不是xlsx的bug[增加] 主题生成器增加侧边栏宽度、header高度、弹窗标题高度、input高度、input风格等配置[优化] 主题对全部layui组件及xmSelect的支持，主题生成器对老版本提供补丁功能，主题缩略图优化[修复] 主题对穿梭框、圆角按钮等一些组件的影响[其他] layer.tips样式进行了小幅的优化(减少阴影和圆角)，可以替换layer.js和form.js</p>','http://www.baidu.com','171','小钉','信息科','2020-04-26 16:30:45.0','0','9999');
INSERT INTO `c_notice` (`id`, `title`, `content`, `url`, `create_id`, `create_name`, `author`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1254339421302423552','AI录音分析，更快更准更安全','<p>ceshialert(1)hah alert(1)</p>','','171','小钉','信息科','2020-04-26 17:19:57.0','0','9999');
INSERT INTO `c_notice` (`id`, `title`, `content`, `url`, `create_id`, `create_name`, `author`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1255674617616400384','智能授权管理平台 正式上线的通知','<p style="padding-left: 40px;"><strong><em>layer</em>是一款近年来备受青睐的web弹层组件，她具备全方位的解决方案，致力于服务各水平段的开发人员，您的页面会轻松地拥有丰富友好的操作体验。</strong></p>
<p><span style="text-decoration: underline;">在与同类组件的比较中，<em>layer</em>总是能轻易获胜。她尽可能地在以更少的代码展现更强健的功能，且格外注重性能的提升、易用和实用性，正因如此，越来越多的开发者将媚眼投上了<em>layer</em>（已被<em id="sees">11022245</em>人次关注）。<em>layer</em>&nbsp;甚至兼容了包括 IE6 在内的所有主流浏览器。她数量可观的接口，使得您可以自定义太多您需要的风格，每一种弹层模式各具特色，广受欢迎。当然，这种&ldquo;王婆卖瓜&rdquo;的陈述听起来总是有点难受，因此你需要进一步了解她是否真的如你所愿。</span></p>
<p><span style="color: #e03e2d;"><strong><em>layer</em>&nbsp;采用 MIT 开源许可证</strong>，</span><em>将会永久性提供无偿服务</em>。因着数年的坚持维护，截至到2017年9月13日，已运用在超过&nbsp;<em>30万</em>&nbsp;家 Web 平台，其中不乏众多知名大型网站。目前 layer 已经成为国内乃至全世界最多人使用的 Web 弹层解决方案，并且她仍在与 Layui 一并高速发展。<a href="http://fly.layui.com/" target="_blank" rel="noopener"><em>Fly</em></a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>','','171','小钉','安全科','2020-04-30 09:45:33.0','0','9999');

INSERT INTO `c_open_app` (`id`, `app_id`, `app_secret`, `public_key`, `private_key`, `app_name`, `remark`, `rules`, `create_id`, `create_time`, `del_flag`, `tenant_id`) VALUES ('1293801124729454592','12345678','12345678',null,null,'Test1',null,'1','1','2020-08-13 14:46:39.0','0','9999');

INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1264459227087241216','8149f7f6c48744c6bcb62872e46a47e8','1','admin','超级管理员','1590305546','1','10000','admin','0',null,'0','1',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278232315067105280','91ac6d77a0c749059864b64d30249272','1','admin','超级管理员','1593589306','1','10000','admin','0',null,'0','111',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278232452053073920','0b7e506fca584ac394414bccd13d6ff5','1','admin','超级管理员','1593589339','1','10000','admin','0',null,'0','111',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278258037114535936','0a30e3de1c6d4da293768a90c2b3a96e','1','admin','超级管理员','1593595439','1','10000','admin','0',null,'0','111',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278261737501491200','9bea86786ab84434bdefa32e77d8ca6b','1','admin','超级管理员','1593596321','1','10000','admin','0',null,'0','111',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278261884218245120','69c7f261b87b4ae58e9ecfc645649507','1','admin','超级管理员','1593596356','0','10000','admin','0',null,'0','111',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278262639037775872','194351ef64ca4c839ec120825aea638e','1','admin','超级管理员','1593596536','1','10000','admin','0',null,'0','1',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278499153818681344','bfa23759bbd3434ca6df4f132cb017fc','1','admin','超级管理员','1593652926','1','10000','admin','0',null,'0','1',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278502831761915904','915e26931f86457eb5fd0cf58bcf1a3a','1','admin','超级管理员','1593653803','1','10000','admin','0',null,'0','1',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1278538418757828608','c5f681da1126454d9a61646e978e73d0','1','admin','超级管理员','1593662287','1','10000','admin','0',null,'9999','1',null);
INSERT INTO `c_session_key` (`id`, `sessionkey`, `user_id`, `user_code`, `user_name`, `login_time`, `status`, `client_type`, `client_name`, `del_flag`, `update_time`, `tenant_id`, `imei`, `phone_model`) VALUES ('1283592300521324544','bfd5c74a162a4e649eff08c9b77b7bd6','184','ddadmin','租户1管理员','1594867226','1','10000','ddadmin','0',null,'1283332638806900736','1',null);

INSERT INTO `c_tenant` (`id`, `name`, `subdomain`, `domain`, `status`, `credit_code`, `create_time`, `create_name`, `create_id`, `admin_code`, `admin_name`, `del_flag`) VALUES ('9999','默认租户-平台运营商','manager','smartlock.hezyun.cn','0','','2020-09-27 16:28:42.0','张三','0','zhangsan','张三','0');
INSERT INTO `c_tenant` (`id`, `name`, `subdomain`, `domain`, `status`, `credit_code`, `create_time`, `create_name`, `create_id`, `admin_code`, `admin_name`, `del_flag`) VALUES ('1280074884243783680','铁路测试','tlcs','tlcs.smart.hezyun.cn','0','','2020-09-27 16:28:42.0','超级管理员','1','sadmin','马新辉','0');
INSERT INTO `c_tenant` (`id`, `name`, `subdomain`, `domain`, `status`, `credit_code`, `create_time`, `create_name`, `create_id`, `admin_code`, `admin_name`, `del_flag`) VALUES ('1285455361041498112','南京分公司','nj','nj.smart.hezyun.cn','0','','2020-09-27 16:28:42.0','超级管理员','1','18621867509','贺卫龙','0');


INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1229','user','用户功能','180','测试人员1','192.168.2.108','2020-09-02 10:50:15.777','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1230','user.query','用户列表','180','测试人员1','192.168.2.108','2020-09-02 10:50:16.536','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1231','org.query','机构查询','180','测试人员1','192.168.2.108','2020-09-02 10:50:17.464','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1232','org.query','机构查询','180','测试人员1','192.168.2.108','2020-09-02 10:50:17.929','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1233','user.query','用户列表','180','测试人员1','192.168.2.108','2020-09-02 10:50:17.986','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1234','org.query','机构查询','180','测试人员1','192.168.2.108','2020-09-02 10:50:25.957','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1235','user.query','用户列表','180','测试人员1','192.168.2.108','2020-09-02 10:50:26.771','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1236','user.query','用户列表','180','测试人员1','192.168.2.108','2020-09-02 10:50:27.715','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1237','user.query','用户列表','180','测试人员1','192.168.2.108','2020-09-02 10:50:28.342','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1238','menu','菜单管理','1','超级管理员','192.168.2.108','2020-09-02 11:02:39.973','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1239','menu.query','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:02:40.907','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1240','menu','菜单管理','1','超级管理员','192.168.2.108','2020-09-02 11:14:36.367','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1241','menu.query','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:14:37.271','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1242','menu.delete','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:18:31.848','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1243','menu.query','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:18:31.901','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1244','menu.delete','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:18:37.058','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1245','menu.query','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:18:37.135','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1246','menu.edit','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:18:45.736','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1247','menu.update','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:19:05.636','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1248','menu.query','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:19:05.698','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1249','menu.delete','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:19:13.025','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1250','menu.query','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:19:13.082','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1251','menu.delete','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:19:20.147','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1252','menu.query','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:19:20.204','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1253','function','功能点管理','1','超级管理员','192.168.2.108','2020-09-02 11:21:52.911','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1254','function.query','功能查询','1','超级管理员','192.168.2.108','2020-09-02 11:21:53.596','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1255','function.delete','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:22:19.944','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1256','function.query','功能查询','1','超级管理员','192.168.2.108','2020-09-02 11:22:20.035','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1257','function.delete','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:22:33.211','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1258','function.query','功能查询','1','超级管理员','192.168.2.108','2020-09-02 11:22:33.3','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1259','function.delete','未定义','1','超级管理员','192.168.2.108','2020-09-02 11:22:40.494','1','',null,'9999');
INSERT INTO `core_audit` (`ID`, `FUNCTION_CODE`, `FUNCTION_NAME`, `USER_ID`, `USER_NAME`, `IP`, `CREATE_TIME`, `SUCCESS`, `MESSAGE`, `ORG_ID`, `tenant_id`) VALUES ('1260','function.query','功能查询','1','超级管理员','192.168.2.108','2020-09-02 11:22:40.563','1','',null,'9999');

INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('1','DA0','查看自己','data_access_type','数据权限','1',null,'0','11111111111111111123',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('2','DA1','查看本公司','data_access_type','数据权限','3',null,'0','hello,go',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('3','DA2','查看同机构','data_access_type','数据权限','3',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('4','DA3','查看本部门','data_access_type','数据权限','4',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('5','DA4','查看集团','data_access_type','数据权限','5',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('6','DA5','查看母公司','data_access_type','数据权限','6',null,'0',null,'2017-10-14 11:45:02.0','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('7','FN0','普通功能','function_type','功能点类型','2',null,'0',null,'2017-10-23 10:18:03.0','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('8','FN1','含数据权限','function_type','功能点类型','1',null,'0',null,'2017-10-23 10:20:05.0','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('9','JT_01','管理岗位','job_type','岗位类型','1',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('10','JT_02','技术岗位','job_type','岗位类型','2',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('11','JT_S_01','董事会','job_sub_managment_type','管理岗位子类型','1','9','0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('12','JT_S_02','秘书','job_sub_managment_type','管理岗位子类型','2','9','0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('13','JT_S_03','技术经理','job_dev_sub_type','技术岗位子类型','1','10','0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('14','JT_S_04','程序员','job_dev_sub_type','技术岗位子类型','2','10','0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('15','MENU_M','菜单','menu_type','菜单类型','3',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('16','MENU_N','导航','menu_type','菜单类型','2',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('17','MENU_S','系统','menu_type','菜单类型','1',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('18','ORGT0','平台','org_type','机构类型','1',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('19','ORGT1','租户','org_type','机构类型','2',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('20','ORGT2','部门','org_type','机构类型','3',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('21','ORGT3','小组','org_type','机构类型','4',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('22','R0','操作角色','role_type','数据权限','1',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('23','R1','工作流角色','role_type','用户角色','2',null,'1',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('24','S0','禁用','user_state','用户状态','2',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('25','S1','启用','user_state','用户状态','1',null,'0',null,null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('26','昂按','随碟附送分','kkkk','水电费水电费',null,null,'1','','2018-02-28 15:43:34.447','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('27','昂按','随碟附送分','kkkk','水电费水电费',null,null,'1','','2018-02-28 15:46:08.035','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('28','sdf','sdfsdf','sfsdf','sdfsdf','1',null,'1','','2018-02-28 15:47:56.384','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('29','asas','sdfsd','sd','sd',null,null,'1','','2018-02-28 15:50:32.214','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('30','asas','sdfsd','sd','sd',null,null,'1','','2018-02-28 15:50:50.241','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('31','1','男','gender','性别',null,null,'0','','2018-03-10 11:36:19.93','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('32','2','女','gender','性别',null,null,'0',null,'2018-03-10 11:36:20.001','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('33','10','设备条码','EQUIPMENT_CODE_TYPE','设备条码类型','1',null,'0','','2020-06-22 11:36:10.596','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('34','20','电子标签','EQUIPMENT_CODE_TYPE','设备条码类型','2',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('35','10','蓝牙','OPERNER_KEY_TYPE','钥匙类型','1',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('36','10','正常','OPENER_KEY_STATUS','钥匙状态','1',null,'0','','2020-06-24 09:23:20.431','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('37','20','失效','OPENER_KEY_STATUS','钥匙状态','2',null,'0','','2020-06-24 09:23:33.114','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('38','30','损坏','OPENER_KEY_STATUS','钥匙状态','3',null,'0','','2020-06-24 09:29:09.973','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('39','10','绑定个人','KEY_BINDING_TYPE','钥匙绑定类型','1',null,'0','','2020-06-24 13:59:18.96','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('40','20','绑定部门','KEY_BINDING_TYPE','钥匙绑定类型','2',null,'0','','2020-06-24 13:59:35.039','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('41','10','绑定','KEY_BINDING_STATUS','钥匙绑定状态','1',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('42','20','解绑','KEY_BINDING_STATUS','钥匙绑定状态','2',null,'0','','2020-06-24 15:24:55.804','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('43','10','绑定','LOCK_BINDING_STATUS','智能锁绑定状态','1',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('44','20','解绑','LOCK_BINDING_STATUS','智能锁绑定状态','2',null,'0','','2020-06-28 13:52:35.365','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('45','10','是','BASE_IS_YES_OR_NO','通用是否字典','1',null,'0','','2020-07-03 13:40:27.556','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('46','20','否','BASE_IS_YES_OR_NO','通用是否字典','1',null,'0','','2020-07-03 13:40:49.989','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('47','10','挂锁','LOCK_CATEGORY','锁具类别','5',null,'0','锁具类型（挂锁、箱变、防盗门锁、机柜锁、井盖锁）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('48','20','箱变','LOCK_CATEGORY','锁具类别','4',null,'0','锁具类型（挂锁、箱变、防盗门锁、机柜锁、井盖锁）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('49','30','防盗门锁','LOCK_CATEGORY','锁具类别','3',null,'0','锁具类型（挂锁、箱变、防盗门锁、机柜锁、井盖锁）','2020-07-03 16:39:45.85','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('50','40','机柜锁','LOCK_CATEGORY','锁具类别','2',null,'0','锁具类型（挂锁、箱变、防盗门锁、机柜锁、井盖锁）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('51','50','井盖锁','LOCK_CATEGORY','锁具类别','1',null,'0','锁具类型（挂锁、箱变、防盗门锁、机柜锁、井盖锁）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('52','10','无源','LOCK_MODEL','锁具型号','4',null,'0','锁具系列（无源、NB、蓝牙、4G）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('53','20','NB','LOCK_MODEL','锁具型号','3',null,'0','锁具系列（无源、NB、蓝牙、4G）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('54','30','蓝牙','LOCK_MODEL','锁具型号','2',null,'0','锁具系列（无源、NB、蓝牙、4G）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('55','40','4G','LOCK_MODEL','锁具型号','1',null,'0','锁具系列（无源、NB、蓝牙、4G）',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('56','10','钥匙损坏','KEY_UNBINDING_REASON','钥匙解绑原因','1',null,'0','','2020-07-06 10:16:25.266','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('57','20','调换部门移交钥匙','KEY_UNBINDING_REASON','钥匙解绑原因','2',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('58','10','智能锁损坏','LOCK_UNBINDING_REASON','智能锁解绑原因','1',null,'0','','2020-07-06 10:17:34.144','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('59','10','更换绑定设备','LOCK_UNBINDING_REASON','智能锁解绑原因','2',null,'0','','2020-07-06 10:18:37.456','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('60','0','正常','c_tenant_status','租户状态','1',null,'0','','2020-07-06 14:21:26.707','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('61','1','欠费','c_tenant_status','租户状态','2',null,'0','','2020-07-06 14:21:37.993','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('62','2','注销','c_tenant_status','租户状态','3',null,'0','','2020-07-06 14:21:48.834','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('63','3','停用','c_tenant_status','租户状态','4',null,'0','','2020-07-06 14:21:59.209','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('64','20','NB','OPERNER_KEY_TYPE','钥匙类型','2',null,'0','','2020-07-23 10:38:45.093','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('65','1','简单','c_app_encryption_rules','应用加密规则',null,null,'0','','2020-08-13 14:20:01.364','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('66','1','客户端帮助文档','c_doc_catalog','系统文档目录',null,null,'0','','2020-08-13 17:02:49.642','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('67','2','管理端帮助文档','c_doc_catalog','系统文档目录',null,null,'0','','2020-08-13 17:02:55.437','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('68','3','其他文档','c_doc_catalog','系统文档目录',null,null,'0','','2020-08-13 17:03:04.438','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('69','1','视频','c_doc_type','系统文档类别',null,null,'0','','2020-08-13 17:04:46.101','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('70','2','链接','c_doc_type','系统文档类别',null,null,'0','','2020-08-13 17:04:52.89','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('71','1','管理平台','c_config_type','系统配置类别',null,null,'0','','2020-08-13 17:37:49.868','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('72','2','授权APP','c_config_type','系统配置类别',null,null,'0','','2020-08-13 17:38:06.68','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('73','login','登录','c_config_catalog','系统配置目录',null,null,'0','','2020-08-13 17:39:20.35','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('74','comm','公共','c_config_catalog','系统配置目录',null,null,'0','','2020-08-13 17:39:34.282','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('75','__UNI__2C7940C','UNI安卓客户端','c_client_appid','客户端APPID',null,null,'0','','2020-08-21 10:12:31.332','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('76','1','安卓','c_client_ostype','客户端系统类型',null,null,'0','','2020-08-21 10:13:15.839','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('77','2','IOS','c_client_ostype','客户端系统类型',null,null,'0','','2020-08-21 10:13:22.71','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('78','apk','apk','c_client_package_type','客户端升级包类别',null,null,'0','','2020-08-21 10:14:13.879','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('79','wgt','wgt增量更新','c_client_package_type','客户端升级包类别',null,null,'0','','2020-08-21 10:14:30.864','9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('80','10','通过','author_audit_state','钥匙申请授权审核状态','1',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('81','20','待审核','author_audit_state','钥匙申请授权审核状态','2',null,'0','',null,'9999');
INSERT INTO `core_dict` (`ID`, `VALUE`, `NAME`, `TYPE`, `TYPE_NAME`, `SORT`, `PARENT`, `DEL_FLAG`, `REMARK`, `CREATE_TIME`, `tenant_id`) VALUES ('82','30','不通过','author_audit_state','钥匙申请授权审核状态','3',null,'0','',null,'9999');

INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('19','dict_upload_template.xls','20180311/dict_upload_template.xls.8caa38fb-52ef-4c73-85ea-abfb1f5c5dc4',null,'1','2018-03-11 15:36:58.906','1',null,'18c0dd67-e334-47ba-8969-915bcf77c544','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('20','dict_upload_template.xls','20180311/dict_upload_template.xls.f50b7f0f-d376-4a95-be6a-14f5aa4a81e6',null,'1','2018-03-11 15:37:15.884','1',null,'335a583a-9c74-462d-be0a-5a82d427b1aa','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('21','dict_upload_template.xls','20180311/dict_upload_template.xls.b0b9434d-e367-43ef-b8ac-366cf7b018b6',null,'1','2018-03-11 15:38:52.6','1',null,'a5b887c6-101c-46e8-b9e2-b3b448edff34','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('22','dict_upload_template.xls','20180311/dict_upload_template.xls.15f02d15-2dd0-4cb7-b2e5-4f7d72fb497d',null,'1','2018-03-11 15:39:02.091','1',null,'833d96bc-797c-403f-aa2e-00e2b5a3cd71','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('23','dict_upload_template.xls','20180311/dict_upload_template.xls.f12350bc-31da-4875-a78e-0135f512fb4c',null,'1','2018-03-11 15:41:52.323','1',null,'0b2a66a3-8aa8-46b7-8bf0-ce9f68041cd8','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('24','dict_upload_template.xls','20180311/dict_upload_template.xls.5bf626e5-2152-45a5-a432-fc2e9fcb7903',null,'1','2018-03-11 15:43:18.066','1',null,'813725ab-2c07-4e48-a766-7ebbe3083212','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('25','dict_upload_template.xls','20180311/dict_upload_template.xls.3cd3eb95-aab9-4105-8d28-76a723274709',null,'1','2018-03-11 15:43:58.096','1',null,'4216455c-20d7-4912-bfc8-c8cca7e70e9f','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('26','dict_upload_template.xls','20180311/dict_upload_template.xls.d3dc557b-1e77-4955-a3be-7a8b4f86407c',null,'1','2018-03-11 15:45:02.882','1',null,'e42dc975-edd5-4d16-8529-fa69b56a5eb5','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('34','dict_upload_template.xls','20180311/dict_upload_template.xls.d50f8245-ec3e-4de1-9742-0c5c12105f27','175','1','2018-03-11 16:30:36.191','1',null,'79b294da-8792-4bfd-9d84-e3f989b88cdf','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('37','副本 功能列表.xlsx','20180311/副本 功能列表.xlsx.bc7554e3-2a30-4667-aa61-0e280340b2be','175','1','2018-03-11 18:53:41.504','1','User','79b294da-8792-4bfd-9d84-e3f989b88cdf','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('38','副本 功能列表.xlsx','20180311/副本 功能列表.xlsx.d991eb1f-24a9-4db1-87c1-7ef9d2b8a40a','175','1','2018-03-11 22:10:57.873','1','User','79b294da-8792-4bfd-9d84-e3f989b88cdf','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('39','1A5F733C886CB77395575B6ED7BFD697.png','default\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.fb7883ab-90bd-4206-8b05-10eacfebe0af','','180','2020-08-12 14:46:08.129','21','','1281130206270062592','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('40','1A5F733C886CB77395575B6ED7BFD697.png','1\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.54f87f06-4a79-48d9-b804-149c1e6dc0de','2','180','2020-08-12 14:47:34.654','21','1','1281130206270062592','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('41','1A5F733C886CB77395575B6ED7BFD697.png','addLock_人员12\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.a237c829-654f-4c35-9348-1bb8077ff416','','180','2020-08-12 14:50:28.9','21','addLock_人员12','1281130206270062592','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('42','1A5F733C886CB77395575B6ED7BFD697.png','addLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.5824744f-8a8d-4012-ba8b-0daf7f3d3b98','1293458802334498816','180','2020-08-12 16:06:24.854','21','addLock','1293458802246418432','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('43','1A5F733C886CB77395575B6ED7BFD697.png','addLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.1ddb6bcb-53df-41a4-8d63-d9a450bde22b','1293459091531759616','180','2020-08-12 16:07:33.804','21','addLock','1293459091468845056','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('44','1A5F733C886CB77395575B6ED7BFD697.png','addLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.b394169c-009a-415b-9f43-3ccdfd615a7b','1293459264253198336','180','2020-08-12 16:08:14.957','21','addLock','1293459264194478080','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('45','1A5F733C886CB77395575B6ED7BFD697.png','delLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.2218df07-6f19-491b-8a6b-035e691df968','1293453543373537280','180','2020-08-12 17:07:37.262','21','delLock','1293453543172210688','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('46','1A5F733C886CB77395575B6ED7BFD697.png','delLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.0a3add17-60c3-4816-8de8-23af7e7d3311','1293454067032391680','180','2020-08-12 17:09:55.871','21','delLock','1293454066982060032','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('47','1A5F733C886CB77395575B6ED7BFD697.png','delLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.c472979c-6c10-4d08-a3c1-c6088b237b2e','1293454384427958272','180','2020-08-12 17:10:36.85','21','delLock','1293454384373432320','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('48','1A5F733C886CB77395575B6ED7BFD697.png','delLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.d4266d5d-23e6-4e1f-a536-bdebc709ccea','1293455102115315712','180','2020-08-12 17:12:47.313','21','delLock','1293455102052401152','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('49','1A5F733C886CB77395575B6ED7BFD697.png','delLock\\20200812\\1A5F733C886CB77395575B6ED7BFD697.png.1ee1bbea-7529-4813-9592-7d6500b6483b','1293455301822906368','180','2020-08-12 17:13:11.224','21','delLock','1293455301768380416','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('50','8FB2E274B683F66863A1EC970E0D3089.png','delKey\\20200812\\8FB2E274B683F66863A1EC970E0D3089.png.e35394d5-bced-4be1-978c-9b4755beb8c9','1293477945137954816','180','2020-08-12 17:24:40.924','21','delKey','1293477943917412352','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('51','1597297755299.jpg','addLock\\20200813\\1597297755299.jpg.d7d912b9-2a33-4a40-9189-f62ae15cebd3','1293786780880863232','180','2020-08-13 13:49:41.061','21','addLock','1293786780801171456','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('52','666c035c12f54ed89e6d67bb8c8229b1.jpg','addLock\\20200813\\666c035c12f54ed89e6d67bb8c8229b1.jpg.96ccb89e-3c41-4137-9ceb-44c7df80326e','2 ','180','2020-08-13 13:57:19.364','21','addLock','1','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('53','1597372310766.jpg','delLock\\20200814\\1597372310766.jpg.e5abc515-78ae-4582-b04e-a945444a72d6','23','180','2020-08-14 10:31:53.464','21','delLock','1292752026416971776','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('54','1A5F733C886CB77395575B6ED7BFD697.png','addLock\\20200814\\1A5F733C886CB77395575B6ED7BFD697.png.39716c9b-ce5e-4813-9e71-dc4fb8fc274e','1294155179725160448','180','2020-08-14 14:13:34.101','21','addLock','1294155179507056640','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('55','1597385664916.jpg','delLock\\20200814\\1597385664916.jpg.682c2987-35d0-4d1d-b3c1-d7a8210cd0b5','1294155179725160448','180','2020-08-14 14:14:27.495','21','delLock','1294155179507056640','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('56','1597385746811.jpg','addLock\\20200814\\1597385746811.jpg.eb70703e-5f91-4a95-b8ab-6060021e1ba1','1294155774410358784','180','2020-08-14 14:15:56.14','21','addLock','1294155774284529664','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('57','1597385813493.jpg','addLock\\20200814\\1597385813493.jpg.b78bd12e-ceaf-4b03-a045-3b5710c019dd','1294156064647806976','180','2020-08-14 14:17:05.425','21','addLock','1294156064610058240','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('58','8FB2E274B683F66863A1EC970E0D3089.png','addLock\\20200814\\8FB2E274B683F66863A1EC970E0D3089.png.721081de-71de-4a29-94f4-8fcb91756d67','1294178636244451328','180','2020-08-14 15:46:46.673','21','addLock','1294178636068290560','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('59','1597396185734.jpg','addLock\\20200814\\1597396185734.jpg.7f19c05e-b352-405c-9a3b-048af7d1037b','1294199595160567808','180','2020-08-14 17:10:03.848','21','addLock','1294199595101847552','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('60','1597396386332.jpg','delLock\\20200814\\1597396386332.jpg.03e23a38-96e0-46fa-ac44-864826621c1b','1294155774410358784','180','2020-08-14 17:13:23.778','21','delLock','1294155774284529664','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('61','1597907269960.jpg','addLock/20200820/1597907269960.jpg.4de2abbf-7c32-4b4a-92dc-7f68227f06fb','1296343173550833664','179','2020-08-20 15:07:59.388','20','addLock','1296343173471141888','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('62','1A5F733C886CB77395575B6ED7BFD697.png','delKey\\20200820\\1A5F733C886CB77395575B6ED7BFD697.png.7ba3ac76-8def-445c-a5ed-98a2feab1e50','1295647159500144640','180','2020-08-20 17:42:46.003','21','delKey','1295647154324373504','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('63','1A5F733C886CB77395575B6ED7BFD697.png','delLock\\20200820\\1A5F733C886CB77395575B6ED7BFD697.png.7636d665-865e-4bd4-a25f-6c6453f35a45','1296380117123072000','180','2020-08-20 17:44:22.943','21','delLock','1296380116359708672','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('64','2ef3411b217f125f7d333a8f28a3195a.png','CClientVersion\\20200821\\2ef3411b217f125f7d333a8f28a3195a.png.b1d58402-98ad-4988-ba76-33983c71f261','1296632111926607872','1','2020-08-21 10:16:18.31','1','CClientVersion','d666b028-a3ae-4ca5-8ebb-f75d24d1b0a9','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('65','2ef3411b217f125f7d333a8f28a3195a.png','CClientVersion\\20200821\\2ef3411b217f125f7d333a8f28a3195a.png.633439de-4ebf-4aff-9197-171802abee91','1296632111926607872','1','2020-08-21 10:34:43.306','1','CClientVersion','834284a4-1923-44d0-b30e-dbe13af33b20','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('66','2ef3411b217f125f7d333a8f28a3195a.png','CClientVersion\\20200821\\2ef3411b217f125f7d333a8f28a3195a.png.5712cc36-3198-4c59-9448-26acc964520b','1296632111926607872','1','2020-08-21 10:35:43.845','1','CClientVersion','9166abf1-dd4b-41a5-afa7-a7b533915130','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('68','6C周报表-年度南京供电段6C整改复测统计表 SQL.txt','CClientVersion\\20200821\\6C周报表-年度南京供电段6C整改复测统计表 SQL.txt.b4eb29a6-ca8d-4710-a276-8834095d7865','1296632111926607872','1','2020-08-21 11:52:42.599','1','CClientVersion','6c755bca-2145-4c04-9f11-021d2693a5a8','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('69','1A5F733C886CB77395575B6ED7BFD697.png','delLock\\20200821\\1A5F733C886CB77395575B6ED7BFD697.png.23a90a17-5619-4ea7-9f75-628175602586','1296701360485433344','180','2020-08-21 14:56:59.224','21','delLock','1296701359575269376','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('72','__UNI__2C7940C_0821150223.apk','CClientVersion/20200821/__UNI__2C7940C_0821150223.apk','1296632111926607872','1','2020-08-21 16:14:39.625','1','CClientVersion','123456','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('73','1598334482239.jpg','feedBack\\20200825\\1598334482239.jpg.ef5bb46e-f652-4569-b392-32b60f6095a3','1298135017414721536','180','2020-08-25 13:48:01.018','21','feedBack','1295985350899924992','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('74','1598334866140.jpg','feedBack\\20200825\\1598334866140.jpg.d83c8a6a-d4a4-4260-a414-91f0f4af6c29','1298136670033412096','180','2020-08-25 13:54:35.022','21','feedBack','1295985350899924992','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('75','1598335232813.jpg','feedBack\\20200825\\1598335232813.jpg.650d7513-ba6f-4d1a-b82e-2aac51d4f038','1298138166007431168','180','2020-08-25 14:00:31.693','21','feedBack','1295985350899924992','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('76','1598335970525.jpg','feedBack\\20200825\\1598335970525.jpg.4020bdd5-a474-4f1b-9cd2-bb8a9272508c','1298141260099354624','180','2020-08-25 14:12:49.396','21','feedBack','1295985350899924992','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('77','1598336103838.jpg','feedBack\\20200825\\1598336103838.jpg.94d4dbe8-ec57-4148-9797-31babcc60592','1298141819292352512','180','2020-08-25 14:15:02.701','21','feedBack','1295985350899924992','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('78','1598425092309.jpg','addLock\\20200826\\1598425092309.jpg.661e238c-0097-4a62-be7b-c2800c0ec3f6','1298515074687696896','180','2020-08-26 14:58:15.023','21','addLock','1298515072989003776','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('79','1598425147409.jpg','addLock\\20200826\\1598425147409.jpg.ed9ee24b-b679-4333-a632-ce2892408573','1298515301993807872','180','2020-08-26 14:59:08.366','21','addLock','1298515301888950272','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('80','1598432020996.jpg','feedBack\\20200826\\1598432020996.jpg.62ac2f6d-11bc-44f9-8cb2-b8f507a50827','1298544118015721472','180','2020-08-26 16:53:38.23','21','feedBack','1298515072989003776','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('81','1598492482232.jpg','addLock\\20200827\\1598492482232.jpg.db348f75-24c1-4a7e-bbae-e88632490a2a','1298797735490617344','180','2020-08-27 09:41:26.159','21','addLock','1298797730214182912','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('82','1598492536242.jpg','addLock\\20200827\\1598492536242.jpg.66527a3f-5541-4bbc-99e2-c0735d08eef1','1298797954357788672','180','2020-08-27 09:42:17.96','21','addLock','1298797954269708288','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('83','1598512884270.jpg','addLock/20200827/1598512884270.jpg.b79a9ec5-ef19-4bc6-abaf-ee20b9c8763d','1298883437842661376','180','2020-08-27 15:22:00.936','21','addLock','1298883435858755584','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('84','1598513147288.jpg','delLock/20200827/1598513147288.jpg.2402a79e-9525-4c39-96a1-ef8716bd51cc','1298883437842661376','180','2020-08-27 15:25:50.089','21','delLock','1298883435858755584','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('85','1598513220289.jpg','addLock/20200827/1598513220289.jpg.aae91b2e-f16c-4619-a96b-9607fbbbf3fe','1298884726664200192','180','2020-08-27 15:27:06.1','21','addLock','1298883435858755584','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('86','1598513498891.jpg','addLock/20200827/1598513498891.jpg.55f8d4c4-b3a6-4dc1-b1e8-460f546cff55','1298885881129598976','180','2020-08-27 15:31:41.495','21','addLock','1298885881096044544','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('87','1598514189953.jpg','feedBack/20200827/1598514189953.jpg.6a71eb39-47fd-4726-be4b-c8e45b447fcd','1298888776008859648','180','2020-08-27 15:43:11.048','21','feedBack','1298883435858755584','9999');
INSERT INTO `core_file` (`ID`, `NAME`, `PATH`, `BIZ_ID`, `USER_ID`, `CREATE_TIME`, `ORG_ID`, `BIZ_TYPE`, `FILE_BATCH_ID`, `tenant_id`) VALUES ('88','1598514202702.jpg','feedBack/20200827/1598514202702.jpg.ef0aa66e-d58d-4eec-b697-ef7afa3e1a18','1298888829196828672','180','2020-08-27 15:43:23.728','21','feedBack','1298885881096044544','9999');

INSERT INTO `core_file_tag` (`ID`, `KEY`, `VALUE`, `FILE_ID`, `tenant_id`) VALUES ('1','customer','12332','1','9999');
INSERT INTO `core_file_tag` (`ID`, `KEY`, `VALUE`, `FILE_ID`, `tenant_id`) VALUES ('2','type','crdit','2','9999');

INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('1','user','用户功能',null,'/admin/user/index.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('2','user.query','用户列表',null,null,'1','FN1');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('6','org','组织机构',null,'/admin/org/orgTree.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('7','role','角色管理',null,'/admin/role/indexNew.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('8','menu','菜单管理',null,'/admin/menu/index.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('9','function','功能点管理',null,'/admin/function/index.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('10','roleFunction','角色功能授权',null,'/admin/role/function.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('11','roleDataAccess','角色数据授权',null,'/admin/role/data.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('12','code','代码生成',null,'/core/codeGen/index.do','296','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('13','dict','字典管理',null,'/admin/dict/index.do','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('18','trace','审计查询',null,'/admin/audit/index.do','295','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('19','file','相关文档',null,'/trade/interAndRelate/file.do','295','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('91','adminExt.cSessionKey.query','会话信息查询','2017-10-11 16:59:01.0','/adminExt/cSessionKey/index.do','295','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('92','adminExt.cSessionKey.delete','会话信息删除','2017-10-11 16:59:01.0','','91','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('161','role.add','角色添加','2017-10-23 09:45:05.0',null,'7','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('180','code.query','代码生成测试',null,null,'12','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('183','code.project','项目生成','2018-03-01 09:38:17.068','/core/codeGen/project.do','12','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('214','adminExt.cTenant','租户管理','2020-06-12 16:05:25.921','','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('215','adminExt.cTenant.query','查询租户','2020-06-12 16:05:25.997','/adminExt/cTenant/index.do','214','FN1');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('216','adminExt.cTenant.edit','修改租户','2020-06-12 16:05:26.024',null,'214','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('217','adminExt.cTenant.add','添加租户','2020-06-12 16:05:26.057',null,'214','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('218','adminExt.cTenant.delete','删除租户','2020-06-12 16:05:26.115',null,'214','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('219','manager.slInstallLocation','设备区域管理','2020-06-18 13:55:24.788','','283','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('220','manager.slInstallLocation.query','查询区域','2020-06-18 13:55:24.799','/manager/slInstallLocation/index.do','219','FN1');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('221','manager.slInstallLocation.edit','修改区域','2020-06-18 13:55:24.806','','219','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('222','manager.slInstallLocation.add','添加区域','2020-06-18 13:55:24.812','','219','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('223','manager.slInstallLocation.delete','删除区域','2020-06-18 13:55:24.818','','219','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('260','org.save','机构保存','2020-07-08 17:28:11.699','','6','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('261','org.query','机构查询','2020-07-08 17:28:37.612','','6','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('262','org.update','机构编辑','2020-07-08 17:28:59.045','','6','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('263','org.delete','机构删除','2020-07-08 17:29:12.525','','6','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('264','role.query','角色查询','2020-07-08 17:33:36.17','','7','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('265','role.edit','角色编辑','2020-07-08 17:34:15.137','','7','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('266','role.delete','角色删除','2020-07-08 17:34:35.7','','7','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('267','user.add','用户新增','2020-07-09 10:07:35.147','','1','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('268','user.delete','用户删除','2020-07-09 10:08:42.528','','1','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('269','user.reset','重置用户密码','2020-07-09 10:09:18.299','','1','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('270','user.role','用户角色','2020-07-09 10:10:04.675','','1','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('271','user.export','用户导出','2020-07-09 10:10:58.717','','1','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('272','user.edit','用户编辑','2020-07-09 10:55:13.447','','1','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('273','role.user.query','角色人员查询','2020-07-09 11:35:17.221','','7','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('274','role.function.query','角色功能查询','2020-07-09 11:52:49.703','','10','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('275','function.query','功能查询','2020-07-09 11:54:14.715','','10','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('276','role.function.edit','角色功能编辑','2020-07-09 14:55:03.046','','10','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('283','基础数据','基础数据','2020-08-13 15:00:28.863','','0','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('284','系统文档','系统文档','2020-08-13 15:02:39.195','','283','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('285','adminExt.cDoc.query','文档查询','2020-08-13 15:02:58.673','/adminExt/cDoc/index.do','284','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('286','adminExt.cDoc.add','添加文档','2020-08-13 15:05:10.918','','284','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('287','adminExt.cDoc.edit','修改文档','2020-08-13 15:05:31.241','','284','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('288','adminExt.cDoc.delete','删除文档','2020-08-13 15:05:46.739','','284','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('289','系统配置','系统配置','2020-08-13 15:40:38.646','','283','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('290','adminExt.cConfig.query','查询系统配置','2020-08-13 15:41:39.242','/adminExt/cConfig/index.do','289','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('291','adminExt.cConfig.add','添加系统配置','2020-08-13 15:42:13.907','','289','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('292','adminExt.cConfig.edit','修改系统配置','2020-08-13 15:42:31.789','','289','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('293','adminExt.cConfig.delete','删除系统配置','2020-08-13 15:42:50.518','','289','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('294','系统管理','系统管理','2020-08-13 15:54:58.807','','0','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('295','运维监测','运维监测','2020-08-13 16:02:49.251','','0','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('296','开发辅助','开发辅助','2020-08-13 16:03:00.215','','0','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('299','系统通知','系统通知','2020-08-13 16:24:00.397','','283','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('300','adminExt.cNotice.query','查询系统通知','2020-08-13 16:24:58.514','/adminExt/cNotice/index.do','299','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('301','adminExt.cNotice.add','添加系统通知','2020-08-13 16:32:22.835','','299','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('302','adminExt.cNotice.edit','修改系统通知','2020-08-13 16:32:44.531','','299','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('303','adminExt.cNotice.delete','删除系统通知','2020-08-13 16:33:04.532','','299','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('304','adminExt.cNotice.receiveList','系统通知接收列表','2020-08-13 16:37:41.601','','299','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('305','adminExt.cNotice.view','查看系统通知','2020-08-13 16:38:15.834','','299','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('306','开放平台','开放平台','2020-08-13 17:22:49.568','','0','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('307','应用管理','应用管理','2020-08-13 17:24:56.242','','306','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('308','adminExt.cOpenApp.query','应用查询','2020-08-13 17:25:44.192','/adminExt/cOpenApp/index.do','307','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('309','adminExt.cOpenApp.add','创建应用','2020-08-13 17:26:31.469','','307','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('310','adminExt.cOpenApp.edit','修改应用','2020-08-13 17:26:48.06','','307','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('311','adminExt.cOpenApp.delete','删除应用','2020-08-13 17:27:01.939','','307','');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('312','adminExt.cTenant.queryAdminUser','租户管理员管理','2020-08-18 15:39:57.602','/adminExt/cTenant/adminUserIndex.do','214','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('331','adminExt.cClientVersion','客户端版本','2020-08-20 18:24:33.364','','294','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('332','adminExt.cClientVersion.query','查询客户端版本','2020-08-20 18:24:33.39','/adminExt/cClientVersion/index.do','331','FN1');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('333','adminExt.cClientVersion.edit','修改客户端版本','2020-08-20 18:24:33.401',null,'331','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('334','adminExt.cClientVersion.add','添加客户端版本','2020-08-20 18:24:33.412',null,'331','FN0');
INSERT INTO `core_function` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `ACCESS_URL`, `PARENT_ID`, `TYPE`) VALUES ('335','adminExt.cClientVersion.delete','删除客户端版本','2020-08-20 18:24:33.423',null,'331','FN0');

INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('8','系统管理','系统管理',null,null,'MENU_S','0','1','layui-icon-set');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('10','用户管理','用户管理',null,'1','MENU_M','18','1',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('11','组织机构管理','组织机构管理',null,'6','MENU_M','18','2',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('12','角色管理','角色管理',null,'7','MENU_M','18','3',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('13','菜单项','菜单项',null,'8','MENU_M','18','4',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('14','功能点管理','功能点管理',null,'9','MENU_M','18','5',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('15','字典数据管理','字典数据管理',null,'13','MENU_M','18','6',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('16','审计查询','审计查询',null,'18','MENU_M','19','7',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('17','代码生成','代码生成',null,'12','MENU_M','24','8','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('18','基础管理','基础管理',null,null,'MENU_N','8','1','layui-icon-set');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('19','监控管理','监控管理',null,null,'MENU_N','8','2','layui-icon-find-fill');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('21','角色功能授权','角色功能授权',null,'10','MENU_M','18','8',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('22','角色数据授权','角色数据授权',null,'11','MENU_M','18','9',null);
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('24','代码生成导航','代码生成','2018-03-01 09:39:31.096',null,'MENU_N','8','1','layui-icon-fonts-code');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('25','子系统生成','子系统生成','2018-03-01 09:42:35.839','183','MENU_M','24','1','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('30','业务子系统','业务子系统','2020-05-01 20:53:53.505',null,'MENU_S','0','0','layui-icon-auz');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('37','adminExt.cSessionKey.query','会话信息管理','2020-05-24 15:20:59.708','91','MENU_M','19','1','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('38','CTenant.Manager','租户管理',null,'215','MENU_M','57','3','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('51','开放平台','开放平台','2020-08-13 14:11:34.927',null,'MENU_N','30','30','layui-icon-component');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('52','COpenApp.Manager','应用管理','2020-08-13 14:12:31.145','308','','51','1','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('53','基础数据','基础数据','2020-08-13 15:06:45.306',null,'MENU_N','30','40','layui-icon-form');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('54','CDoc.Manager','系统文档管理','2020-08-13 15:08:08.724','285','','53','1','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('55','系统配置','系统配置','2020-08-13 15:43:39.402','290','','53','100','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('56','系统通知管理','系统通知管理','2020-08-13 16:34:27.453','300','','53','1','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('57','多租户管理','多租户管理','2020-08-14 13:48:01.148',null,'MENU_N','8','50','layui-icon-tabs');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('58','租户管理员管理','租户管理员管理','2020-08-18 15:40:33.621','312','MENU_N','57','20','');
INSERT INTO `core_menu` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `FUNCTION_ID`, `TYPE`, `PARENT_MENU_ID`, `SEQ`, `ICON`) VALUES ('65','CClientVersion.Manager','客户端版本管理',null,'332','MENU_M','18','10','');

INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('1','集团公司','平台运营','2018-02-02 17:18:50.0',null,'ORGT0','0','9999','.1.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('3','千友','千友',null,'1','ORGT1','0','9999','.1.3.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('4','触达云','触达云','2018-02-02 17:18:56.0','1','ORGT1','0','9999','.1.4.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('5','上海XX','上海XX',null,'1','ORGT1','0','9999','.1.5.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('6','研发中心','研发中心',null,'3','ORGT2','0','9999','.1.3.6.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('7','销售部','销售部','2018-02-05 13:49:52.754','3','ORGT2','0','9999','.1.3.7.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('8','生产部门','生产部门','2018-02-05 13:50:43.901','3','ORGT2','0','9999','.1.3.8.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('17','111','111','2020-07-02 10:25:21.594','1','ORGT2','0','9999','.1.17.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('18','Tenant_Org','租户管理','2020-07-06 16:51:49.022',null,'ORGT3','0','9999','.18.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('19','测试','测试','2020-07-06 17:38:15.935',null,'ORGT0','0','1280073568142491648',null);
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('20','test','测试部','2020-07-06 17:43:29.716',null,'ORGT0','0','1280074884243783680','.20.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('21','ceshi','测试部门','2020-07-09 09:52:42.059','20','ORGT2','0','1280074884243783680','.20.21.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('22','租户1','租户1','2020-07-15 17:28:38.938',null,'ORGT0','0','1283332638806900736','.22.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('23','南京分公司','南京分公司','2020-07-21 14:03:35.348',null,'ORGT0','0','1285455361041498112','.23.');
INSERT INTO `core_org` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `PARENT_ORG_ID`, `TYPE`, `DEL_FLAG`, `tenant_id`, `SEQ`) VALUES ('24','org1','子部门','2020-09-01 14:42:35.781','20','ORGT2','0','1280074884243783680','.20.24.');

INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('1','SYS_MANAGER','系统管理员',null,'R0','9999');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('2','CEO','公司CEO',null,'R0','9999');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('3','ASSIST','助理',null,'R0','9999');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('174','SYS_SUB_MANAGER','平台子管理员','2020-05-04 17:23:47.274','R0','9999');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('175','TENANT_ADMIN','租户管理员','2020-05-04 17:24:38.663','R0','9999');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('185','caoz1','操作员','2020-07-09 11:32:13.74','R0','1280074884243783680');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('187','子管理员','子管理员','2020-07-22 15:29:49.294','R0','1285455361041498112');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('188','manager','子管理员','2020-07-22 16:17:40.565','R0','1280074884243783680');
INSERT INTO `core_role` (`ID`, `CODE`, `NAME`, `CREATE_TIME`, `TYPE`, `tenant_id`) VALUES ('190','TENANT_ADMIN_VIP1','租户管理员VIP1','2020-08-18 15:36:29.358','R0','9999');

INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('1','1','1',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('2','1','2',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('4','2','2',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('5','3','2',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('162','1','6',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('164','1','91',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('192','3','1',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('194','3','12',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('196','3','180',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('197',null,'1',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('198',null,'2',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('200',null,'6',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('201',null,'91',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('202',null,'8',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('207','1','7',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('208','1','161',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('209','1','8',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('210','1','9',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('211','1','10',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('212','1','11',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('213','1','12',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('214','1','180',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('215','1','183',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('216','1','13',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('217','1','18',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('218','1','19',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('220','1','219',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('221','2','1',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('222','2','219',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('223','2','220',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('224','2','221',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('225','2','222',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('226','2','223',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('252','175','1',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('253','175','2',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('255','175','6',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('256','175','7',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('257','175','161',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('267','175','219',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('268','175','220',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('269','175','221',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('270','175','222',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('271','175','223',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('302','175','260',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('303','175','261',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('304','175','262',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('305','175','263',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('306','175','264',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('307','175','265',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('308','175','266',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('309','175','267',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('310','175','268',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('311','175','269',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('312','175','270',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('313','175','271',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('314','175','272',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('315','175','273',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('316','175','10',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('317','175','274',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('318','175','275',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('319','175','276',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('402','185','219',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('403','185','220',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('404','185','221',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('405','185','222',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('406','185','223',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('431','185','1',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('432','185','2',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('433','185','6',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('434','185','261',null,null,'1280074884243783680');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('435','175','283',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('436','175','284',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('437','175','285',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('438','175','286',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('439','175','287',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('440','175','288',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('441','175','289',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('442','175','290',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('443','175','291',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('444','175','292',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('445','175','293',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('446','175','294',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('447','175','295',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('448','175','91',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('449','175','92',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('452','175','306',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('453','175','307',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('454','175','308',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('455','175','309',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('456','175','310',null,null,'9999');
INSERT INTO `core_role_function` (`ID`, `ROLE_ID`, `FUNCTION_ID`, `DATA_ACCESS_TYPE`, `DATA_ACCESS_POLICY`, `tenant_id`) VALUES ('458','175','311',null,null,'9999');

INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('1','1','10',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('163','1','11',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('193','3','10',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('195','3','17',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('196',null,'10',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('197',null,'11',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('198',null,'13',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('199','1','12',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('200','1','13',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('201','1','14',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('202','1','21',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('203','1','22',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('204','1','17',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('205','1','25',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('206','1','15',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('207','1','16',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('208','2','10',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('214','175','10',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('215','175','11',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('216','175','12',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('228','175','21',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('248','185','10',null,'1280074884243783680');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('249','185','11',null,'1280074884243783680');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('250','175','54',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('251','175','55',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('252','175','37',null,'9999');
INSERT INTO `core_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`, `CREATE_TIME`, `tenant_id`) VALUES ('253','175','52',null,'9999');

INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('1','admin','超级管理员','ff92a240d11b05ebd392348c35f781b2','2017-09-13 09:21:03.0','1','S1','JT_S_01','0','2020-08-18 15:53:14.0','JT_01',null,null,'9999');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('171','zhangsan','张三','ff92a240d11b05ebd392348c35f781b2','2018-01-28 11:21:20.914','1','S1','JT_S_04','0',null,'JT_02',null,null,'9999');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('172','lisi','李四','ff92a240d11b05ebd392348c35f781b2','2018-01-28 11:22:38.814','3','S1','JT_S_02','0',null,'JT_01',null,null,'9999');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('173','wangwu','王五','ff92a240d11b05ebd392348c35f781b2','2018-01-28 14:44:55.407','6','S1','JT_S_04','0',null,'JT_02',null,null,'9999');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('174','zhaoliu','赵六','ff92a240d11b05ebd392348c35f781b2','2018-02-16 11:36:41.438','4','S1','JT_S_04','0',null,'JT_02',null,null,'9999');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('175','tianqi','田七','ff92a240d11b05ebd392348c35f781b2','2018-03-11 16:19:21.675','3','S1','JT_S_04','0',null,'JT_02','79b294da-8792-4bfd-9d84-e3f989b88cdf',null,'9999');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('179','sadmin','马新辉','9db06bcff9248837f86d1a6bcf41c9e7',null,'20','S1',null,'0',null,null,null,'1','1280074884243783680');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('180','test1','测试人员1','ff92a240d11b05ebd392348c35f781b2','2020-07-09 10:52:35.325','21','S1','JT_S_04','0',null,'JT_02','93237ec4-4c1e-4e40-8586-6157fef761cd',null,'1280074884243783680');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('181','test2','测试人员2','ff92a240d11b05ebd392348c35f781b2','2020-07-09 10:56:46.897','21','S1','JT_S_04','0',null,'JT_02','3639cdd1-981f-4afd-8f56-f27c2bfda08e',null,'1280074884243783680');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('182','test3','测试人员3','ff92a240d11b05ebd392348c35f781b2','2020-07-10 11:11:31.612','21','S1','JT_S_03','0',null,'JT_02','81e0bd5d-bbcf-43a5-8f81-ac99a4bf995a',null,'1280074884243783680');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('183','test4','测试人员4','ff92a240d11b05ebd392348c35f781b2','2020-07-10 11:49:45.602','21','S1','JT_S_03','0',null,'JT_02','552eb0c8-b786-4d01-b1ba-19587bd65413',null,'1280074884243783680');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('184','ddadmin','租户1管理员','ff92a240d11b05ebd392348c35f781b2','2020-07-15 17:28:38.974','22','S1',null,'0',null,null,null,'1','1283332638806900736');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('185','18621867509','贺卫龙','14e1b600b1fd579f47433b88e8d85291','2020-07-21 14:03:35.38','23','S1',null,'0','2020-08-18 16:48:00.0',null,null,'1','1285455361041498112');
INSERT INTO `core_user` (`ID`, `CODE`, `NAME`, `PASSWORD`, `CREATE_TIME`, `ORG_ID`, `STATE`, `JOB_TYPE1`, `DEL_FLAG`, `update_Time`, `JOB_TYPE0`, `attachment_id`, `tenant_admin`, `tenant_id`) VALUES ('186','zhangsan','zhangsan','ff92a240d11b05ebd392348c35f781b2','2020-08-27 17:34:09.715','21','S1','','0',null,'',null,null,'1280074884243783680');

INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('1','3','1','4',null);
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('2','4','2','5',null);
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('3','75','3','6','2017-09-21 18:03:05.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('35','1','1','1','2017-09-06 01:12:02.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('42','3','3','1','2017-09-06 04:01:00.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('47','47','3','1','2017-09-06 22:00:01.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('49','5','3','4','2017-09-06 22:01:00.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('52','47','2','1','2017-09-07 01:12:02.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('53','48','3','4','2017-09-07 01:14:04.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('55','68','2','3','2017-09-07 21:42:03.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('125','74','1','4','2017-10-17 09:37:02.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('144','74','3',null,'2017-10-17 16:55:00.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('145','67','3',null,'2017-10-17 16:55:01.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('146','73','3',null,'2017-10-17 16:55:02.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('147','22','3',null,'2017-10-17 16:55:04.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('148','68','3',null,'2017-10-17 16:56:00.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('168','72','1','3','2017-10-24 14:40:04.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('169','41','1',null,'2017-10-25 08:58:01.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('171','170','1','5','2017-10-25 09:08:05.0');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('196','1','2','1','2020-06-17 12:02:23.96');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('197','172','1','3','2020-06-17 12:07:26.205');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('198','173','1','6','2020-06-17 12:07:59.38');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('199','174','1','4','2020-06-17 12:13:26.407');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('200','175','1','3','2020-06-17 12:19:35.662');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('201','172','3','3','2020-06-17 14:21:48.449');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('202','173','3','6','2020-06-17 14:21:48.453');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('204','172','174','3','2020-06-17 14:41:04.427');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('205','172','2','3','2020-06-17 14:51:03.153');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('206','174','3','4','2020-06-17 14:53:23.513');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('207','173','2','6','2020-06-17 14:53:53.282');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('209','1','3','1','2020-06-17 14:59:32.161');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('210','175','3','3','2020-06-17 15:01:36.118');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('212','173','174','6','2020-06-17 15:04:03.681');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('213','1','174','1','2020-06-17 15:08:44.911');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('215','174','174','4','2020-06-17 15:11:43.234');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('217','175','174','3','2020-06-17 15:22:20.128');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('218','174','2','4','2020-06-17 16:10:06.229');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('219','175','2','3','2020-06-17 16:10:06.235');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('230',null,'175',null,'2020-07-06 16:03:25.574');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('231',null,'175',null,'2020-07-06 17:38:15.994');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('234','181','185','21','2020-07-09 11:48:54.619');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('235','180','185','21','2020-07-09 16:18:27.547');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('236','183','185','21','2020-07-10 11:50:17.947');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('237','184','175','22','2020-07-15 17:28:38.987');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('238','185','175','23','2020-07-21 14:03:35.392');
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('242','171','175','1',null);
INSERT INTO `core_user_role` (`ID`, `USER_ID`, `ROLE_ID`, `ORG_ID`, `CREATE_TIME`) VALUES ('247','179','175','20',null);

