package com.frame.admin.ext.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.beetl.sql.core.annotatoin.*;

import com.frame.admin.core.util.ValidateConfig;

import com.frame.admin.core.annotation.Dict;
import com.frame.admin.core.entity.BaseEntity;


/* 
* 开放应用管理
* gen by Spring Boot2 Admin 2020-04-27
*/
public class COpenApp extends BaseEntity{

    @NotNull(message = "ID不能为空", groups =ValidateConfig.UPDATE.class)
    @SeqID(name = ORACLE_CORE_SEQ_NAME)
    @AssignID("simple")

    private Long id ;
	

    private String appId ;
	

    private String appSecret ;
	

    private String publicKey ;
	

    private String privateKey ;
	

    private String appName ;
	

    private Integer remark ;
	
    //0 简单，1 复杂
    @Dict(type="c_app_encryption_rules")

    private Integer rules ;

    @UpdateIgnore
    private Long createId ;

    @UpdateIgnore
    private Date createTime ;
	
	/*逻辑删除标志*/
	@InsertIgnore
	@LogicDelete(value = 1)

    private Integer delFlag ;

	//租户id
	private Long tenantId;
	
    public COpenApp(){
    }

    public Long getId(){
        return  id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public String getAppId(){
        return  appId;
    }
    public void setAppId(String appId){
        this.appId = appId;
    }

    public String getAppSecret(){
        return  appSecret;
    }
    public void setAppSecret(String appSecret){
        this.appSecret = appSecret;
    }

    public String getPublicKey(){
        return  publicKey;
    }
    public void setPublicKey(String publicKey){
        this.publicKey = publicKey;
    }

    public String getPrivateKey(){
        return  privateKey;
    }
    public void setPrivateKey(String privateKey){
        this.privateKey = privateKey;
    }

    public String getAppName(){
        return  appName;
    }
    public void setAppName(String appName){
        this.appName = appName;
    }

    public Integer getRemark(){
        return  remark;
    }
    public void setRemark(Integer remark){
        this.remark = remark;
    }

    /**0 简单，1 复杂
    *@return 
    */
    public Integer getRules(){
        return  rules;
    }
    /**0 简单，1 复杂
    *@param  rules
    */
    public void setRules(Integer rules){
        this.rules = rules;
    }

    public Long getCreateId(){
        return  createId;
    }
    public void setCreateId(Long createId){
        this.createId = createId;
    }

    public Date getCreateTime(){
        return  createTime;
    }
    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    public Integer getDelFlag(){
        return  delFlag;
    }
    public void setDelFlag(Integer delFlag){
        this.delFlag = delFlag;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
