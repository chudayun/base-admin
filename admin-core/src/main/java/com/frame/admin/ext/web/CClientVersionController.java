package com.frame.admin.ext.web;

import com.frame.admin.ext.entity.CClientVersion;
import com.frame.admin.ext.service.CClientVersionService;
import com.frame.admin.ext.web.query.CClientVersionQuery;
import com.frame.admin.core.annotation.Function;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.util.ConvertUtil;
import com.frame.admin.core.util.ValidateConfig;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 客户端版本 接口
 */
@Controller
public class CClientVersionController{

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/adminExt/cClientVersion";


    @Autowired private CClientVersionService cClientVersionService;
    
    @Autowired
    FileService fileService;



    /* 页面 */

    @GetMapping(MODEL + "/index.do")
    @Function("adminExt.cClientVersion.query")
    @ResponseBody
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/adminExt/cClientVersion/index.html") ;
        view.addObject("search", CClientVersionQuery.class.getName());
        return view;
    }

    @GetMapping(MODEL + "/edit.do")
    @Function("adminExt.cClientVersion.edit")
    @ResponseBody
    public ModelAndView edit(Long id) {
        ModelAndView view = new ModelAndView("/adminExt/cClientVersion/edit.html");
        CClientVersion cClientVersion = cClientVersionService.queryById(id);
        view.addObject("cClientVersion", cClientVersion);
        return view;
    }

    @GetMapping(MODEL + "/add.do")
    @Function("adminExt.cClientVersion.add")
    @ResponseBody
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("/adminExt/cClientVersion/add.html");
        return view;
    }

    /* ajax json */

    @PostMapping(MODEL + "/list.json")
    @Function("adminExt.cClientVersion.query")
    @ResponseBody
    public JsonResult<PageQuery> list(CClientVersionQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cClientVersionService.queryByCondition(page);
        return JsonResult.success(page);
    }

    @PostMapping(MODEL + "/add.json")
    @Function("adminExt.cClientVersion.add")
    @ResponseBody
    public JsonResult add(@Validated(ValidateConfig.ADD.class)CClientVersion cClientVersion)
    {
        cClientVersionService.save(cClientVersion);
        return new JsonResult().success();
    }

    @PostMapping(MODEL + "/edit.json")
    @Function("adminExt.cClientVersion.edit")
    @ResponseBody
    public JsonResult<String> update(@Validated(ValidateConfig.UPDATE.class)  CClientVersion cClientVersion) {
        boolean success = cClientVersionService.update(cClientVersion);
        if (success) {
            return new JsonResult().success();
        } else {
            return JsonResult.failMessage("保存失败");
        }
    }


   
    @GetMapping(MODEL + "/view.json")
    @Function("adminExt.cClientVersion.query")
    @ResponseBody
    public JsonResult<CClientVersion>queryInfo(Long id) {
        CClientVersion cClientVersion = cClientVersionService.queryById( id);
        return  JsonResult.success(cClientVersion);
    }

    @PostMapping(MODEL + "/delete.json")
    @Function("adminExt.cClientVersion.delete")
    @ResponseBody
    public JsonResult delete(String ids) {
        if (ids.endsWith(",")) {
            ids = StringUtils.substringBeforeLast(ids, ",");
        }
        List<Long> idList = ConvertUtil.str2longs(ids);
        cClientVersionService.batchDelCClientVersion(idList);
        return new JsonResult().success();
    }
    

}
