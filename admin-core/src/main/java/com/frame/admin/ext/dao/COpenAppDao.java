package com.frame.admin.ext.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.beetl.sql.core.engine.PageQuery;

import  com.frame.admin.ext.entity.*;

/**
 * 应用 Dao
 */
@SqlResource("adminExt.cOpenApp")
public interface COpenAppDao extends BaseMapper<COpenApp>{
    public PageQuery<COpenApp> queryByCondition(PageQuery query);
    public void batchDelCOpenAppByIds( List<Long> ids);
    public COpenApp getByAppId( String appId);
}