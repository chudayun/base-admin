package com.frame.admin.ext.pojo;

import com.frame.admin.core.entity.CoreUser;

import java.io.Serializable;

public class CurrentUser implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3019465885046702022L;
	private CoreUser user;

    public CoreUser getUser() {
        return user;
    }

    public void setUser(CoreUser user) {
        this.user = user;
    }
}
