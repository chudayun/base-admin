package com.frame.admin.ext.service;


import com.frame.admin.ext.dao.CClientVersionDao;
import com.frame.admin.ext.entity.CClientVersion;



import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.util.PlatformException;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;






/**
 * 客户端版本 Service
 */

@Service
@Transactional
public class CClientVersionService extends CoreBaseService<CClientVersion>{

    @Autowired private CClientVersionDao cClientVersionDao;

    public PageQuery<CClientVersion>queryByCondition(PageQuery query){
        PageQuery ret =  cClientVersionDao.queryByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public CClientVersion getLatestVersion(CClientVersion version){
        return cClientVersionDao.getLatestVersion(version);
    }
    public void batchDelCClientVersion(List<Long> ids){
        try {
            cClientVersionDao.batchDelCClientVersionByIds(ids);
        } catch (Exception e) {
            throw new PlatformException("批量删除客户端版本失败", e);
        }
    }
}