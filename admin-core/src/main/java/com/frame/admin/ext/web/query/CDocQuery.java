package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;

/**
 *系统文档查询
 */
public class CDocQuery extends PageParam {
    @Query(name = "文档标题", display = true)
    private String title;
    @Query(name = "文档类别", display = true,type=Query.TYPE_DICT,dict="c_doc_type")
    private Integer type;
    
    private Long catalog;
    
    
    
    public Long getCatalog() {
		return catalog;
	}
	public void setCatalog(Long catalog) {
		this.catalog = catalog;
	}
	public String getTitle(){
        return  title;
    }
    public void setTitle(String title ){
        this.title = title;
    }
    public Integer getType(){
        return  type;
    }
    public void setType(Integer type ){
        this.type = type;
    }
 
}
