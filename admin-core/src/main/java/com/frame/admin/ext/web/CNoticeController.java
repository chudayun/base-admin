package com.frame.admin.ext.web;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.frame.admin.ext.entity.CNotice;
import com.frame.admin.ext.service.CNoticeService;
import com.frame.admin.ext.web.query.CNoticeQuery;
import com.frame.admin.core.annotation.Function;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.util.ConvertUtil;
import com.frame.admin.core.util.ValidateConfig;
import com.frame.admin.core.web.JsonResult;

/**
 * 系统通知 接口
 */
@Controller
public class CNoticeController{

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/adminExt/cNotice";
    @Autowired
	CorePlatformService platformService ;

    @Autowired private CNoticeService cNoticeService;
    
    @Autowired
    FileService fileService;
    /* 页面 */

    @GetMapping(MODEL + "/index.do")
    @Function("adminExt.cNotice.query")
    @ResponseBody
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/adminExt/cNotice/index.html") ;
        view.addObject("search", CNoticeQuery.class.getName());
        return view;
    }
    
    @GetMapping(MODEL + "/list.do")
    @Function("adminExt.cNotice.query")
    @ResponseBody
    public ModelAndView list() {
        ModelAndView view = new ModelAndView("/adminExt/cNotice/list.html") ;
        view.addObject("search", CNoticeQuery.class.getName());
        return view;
    }

    @GetMapping(MODEL + "/edit.do")
    @Function("adminExt.cNotice.edit")
    @ResponseBody
    public ModelAndView edit(Long id) {
        ModelAndView view = new ModelAndView("/adminExt/cNotice/edit.html");
        CNotice cNotice = cNoticeService.queryById(id);
        view.addObject("cNotice", cNotice);
        return view;
    }
    
    @GetMapping(MODEL + "/view.do")
    @Function("adminExt.cNotice.query")
    @ResponseBody
    public ModelAndView view(Long id) {
    	ModelAndView view = new ModelAndView("/adminExt/cNotice/view.html");
        CNotice cNotice = cNoticeService.queryById( id);
        view.addObject("cNotice", cNotice);
        return view;
    }

    @GetMapping(MODEL + "/add.do")
    @Function("adminExt.cNotice.add")
    @ResponseBody
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("/adminExt/cNotice/add.html");
        return view;
    }

    /* ajax json */

    @PostMapping(MODEL + "/list.json")
    @Function("adminExt.cNotice.query")
    @ResponseBody
    public JsonResult<PageQuery> list(CNoticeQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cNoticeService.queryByCondition(page);
        return JsonResult.success(page);
    }

    @PostMapping(MODEL + "/add.json")
    @Function("adminExt.cNotice.add")
    @ResponseBody
    public JsonResult add(@Validated(ValidateConfig.ADD.class)CNotice cNotice)
    {
    	cNotice.setCreateId(platformService.getCurrentUser().getId());
    	cNotice.setCreateName(platformService.getCurrentUser().getName());
    	
    	//cNotice.setContent(HtmlUtil.unescape(cNotice.getContent()));
    	//cNotice.setContent(HtmlUtil.filter(cNotice.getContent()));
    	cNotice.setCreateTime(new Date());
        cNoticeService.save(cNotice);
        return new JsonResult().success();
    }

    @PostMapping(MODEL + "/edit.json")
    @Function("adminExt.cNotice.edit")
    @ResponseBody
    public JsonResult<String> update(@Validated(ValidateConfig.UPDATE.class)  CNotice cNotice) {
    	//cNotice.setContent(HtmlUtil.unescape(cNotice.getContent()));
    	//cNotice.setContent(HtmlUtil.filter(cNotice.getContent()));
        boolean success = cNoticeService.update(cNotice);
        if (success) {
            return new JsonResult().success();
        } else {
            return JsonResult.failMessage("保存失败");
        }
    }


   
    @GetMapping(MODEL + "/view.json")
    @Function("adminExt.cNotice.query")
    @ResponseBody
    public JsonResult<CNotice>queryInfo(Long id) {
        CNotice cNotice = cNoticeService.queryById( id);
        return  JsonResult.success(cNotice);
    }

    @PostMapping(MODEL + "/delete.json")
    @Function("adminExt.cNotice.delete")
    @ResponseBody
    public JsonResult delete(String ids) {
        if (ids.endsWith(",")) {
            ids = StringUtils.substringBeforeLast(ids, ",");
        }
        List<Long> idList = ConvertUtil.str2longs(ids);
        cNoticeService.batchDelCNotice(idList);
        return new JsonResult().success();
    }


    @PostMapping(MODEL + "/receiveList.json")
    @Function("adminExt.cNotice.query")
    @ResponseBody
    public JsonResult<PageQuery> receiveList(CNoticeQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cNoticeService.queryReceiveListByCondition(page);
        return JsonResult.success(page);
    }
    
}
