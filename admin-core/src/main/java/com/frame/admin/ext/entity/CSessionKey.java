package com.frame.admin.ext.entity;

import com.frame.admin.core.annotation.Dict;
import com.frame.admin.core.entity.BaseEntity;
import com.frame.admin.core.util.ValidateConfig;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.InsertIgnore;
import org.beetl.sql.core.annotatoin.LogicDelete;

import javax.validation.constraints.NotNull;


/* 
* 客户端登录信息表
* gen by Spring Boot2 Admin 2020-04-10
*/
public class CSessionKey extends BaseEntity{

    //主键
    @NotNull(message = "ID不能为空", groups =ValidateConfig.UPDATE.class)
    
    @AssignID("simple")
    private Long id ;
	
    //会话key

    private String sessionkey ;
	
    //用户ID

    private Long userId ;
	
    //用户编码

    private String userCode ;
	
    //用户姓名(冗余)

    private String userName ;
	
    //登录时间，时间戳

    private Long loginTime ;
	
    //状态 0 正常，1 无效
    @Dict(type="user_state")

    private Integer status ;
	
    //客户端类别，目前只有一种 00001, windows客户端

    private String clientType ;
	

    private String clientName ;
	
	/*逻辑删除标志*/
	@InsertIgnore
	@LogicDelete(value = 1)
	
    private Integer delFlag ;
	

    private Long updateTime ;
	

    private Long tenantId ;

    /*手机设备号*/
    private String imei;

    /*手机型号*/
    private String phoneModel;
	
    public CSessionKey(){
    }

    /**主键
    *@return 
    */
    public Long getId(){
        return  id;
    }
    /**主键
    *@param  id
    */
    public void setId(Long id){
        this.id = id;
    }

    /**会话key
    *@return 
    */
    public String getSessionkey(){
        return  sessionkey;
    }
    /**会话key
    *@param  sessionkey
    */
    public void setSessionkey(String sessionkey){
        this.sessionkey = sessionkey;
    }

    /**用户ID
    *@return 
    */
    public Long getUserId(){
        return  userId;
    }
    /**用户ID
    *@param  userId
    */
    public void setUserId(Long userId){
        this.userId = userId;
    }

    /**用户编码
    *@return 
    */
    public String getUserCode(){
        return  userCode;
    }
    /**用户编码
    *@param  userCode
    */
    public void setUserCode(String userCode){
        this.userCode = userCode;
    }

    /**用户姓名(冗余)
    *@return 
    */
    public String getUserName(){
        return  userName;
    }
    /**用户姓名(冗余)
    *@param  userName
    */
    public void setUserName(String userName){
        this.userName = userName;
    }

    /**登录时间，时间戳
    *@return 
    */
    public Long getLoginTime(){
        return  loginTime;
    }
    /**登录时间，时间戳
    *@param  loginTime
    */
    public void setLoginTime(Long loginTime){
        this.loginTime = loginTime;
    }

    /**状态 0 正常，1 无效
    *@return 
    */
    public Integer getStatus(){
        return  status;
    }
    /**状态 0 正常，1 无效
    *@param  status
    */
    public void setStatus(Integer status){
        this.status = status;
    }

    /**客户端类别，目前只有一种 00001, windows客户端
    *@return 
    */
    public String getClientType(){
        return  clientType;
    }
    /**客户端类别，目前只有一种 00001, windows客户端
    *@param  clientType
    */
    public void setClientType(String clientType){
        this.clientType = clientType;
    }

    public String getClientName(){
        return  clientName;
    }
    public void setClientName(String clientName){
        this.clientName = clientName;
    }

    public Integer getDelFlag(){
        return  delFlag;
    }
    public void setDelFlag(Integer delFlag){
        this.delFlag = delFlag;
    }

    public Long getUpdateTime(){
        return  updateTime;
    }
    public void setUpdateTime(Long updateTime){
        this.updateTime = updateTime;
    }

    public Long getTenantId(){
        return  tenantId;
    }
    public void setTenantId(Long tenantId){
        this.tenantId = tenantId;
    }


    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }
}
