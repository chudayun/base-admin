package com.frame.admin.ext.web;

import com.frame.admin.ext.entity.CTenant;
import com.frame.admin.ext.pojo.TenantAdminUserRole;
import com.frame.admin.ext.service.CTenantService;
import com.frame.admin.ext.web.query.CTenantQuery;
import com.frame.admin.ext.web.query.CTenantUserQuery;
import com.frame.admin.core.annotation.Function;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.util.ConvertUtil;
import com.frame.admin.core.util.ValidateConfig;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 租户 接口
 */
@Controller
public class CTenantController{

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/adminExt/cTenant";


    @Autowired private CTenantService cTenantService;
    
    @Autowired
    FileService fileService;
    /* 页面 */

    @GetMapping(MODEL + "/index.do")
    @Function("adminExt.cTenant.query")
    @ResponseBody
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/adminExt/cTenant/index.html") ;
        view.addObject("search", CTenantQuery.class.getName());
        return view;
    }

    @GetMapping(MODEL + "/adminUserIndex.do")
    @Function("adminExt.cTenant.queryAdminUser")
    @ResponseBody
    public ModelAndView adminUserIndex() {
        ModelAndView view = new ModelAndView("/adminExt/cTenant/adminUserIndex.html") ;
        view.addObject("search", CTenantUserQuery.class.getName());
        return view;
    }



    @GetMapping(MODEL + "/edit.do")
    @Function("adminExt.cTenant.edit")
    @ResponseBody
    public ModelAndView edit(Long id) {
        ModelAndView view = new ModelAndView("/adminExt/cTenant/edit.html");
        CTenant cTenant = cTenantService.queryById(id);


        CTenantQuery query = new CTenantQuery();
        query.setId(cTenant.getId());
        List<CoreUser> adminUsers = cTenantService.getAdminUserByTenant(query);
        if (adminUsers.size()>0){
            cTenant.set("adminUser",adminUsers.get(0));
        }

        view.addObject("cTenant", cTenant);
        // TENANT_ADMIN
        return view;
    }

    @GetMapping(MODEL + "/add.do")
    @Function("adminExt.cTenant.add")
    @ResponseBody
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("/adminExt/cTenant/add.html");
        return view;
    }

    /* ajax json */

    @PostMapping(MODEL + "/list.json")
    @Function("adminExt.cTenant.query")
    @ResponseBody
    public JsonResult<PageQuery> list(CTenantQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cTenantService.queryByCondition(page);
        return JsonResult.success(page);
    }

    @PostMapping(MODEL + "/listAdminUser.json")
    @Function("adminExt.cTenant.queryAdminUser")
    @ResponseBody
    public JsonResult<PageQuery> listAdminUser(CTenantUserQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cTenantService.queryAdminUser(page);
        return JsonResult.success(page);
    }

    @PostMapping(MODEL + "/changeAdminRole.json")
    @Function("adminExt.cTenant.queryAdminUser")
    @ResponseBody
    public JsonResult changeAdminRole(TenantAdminUserRole tenantAdminUserRole)
    {
//        SELECT * FROM `core_user_role` WHERE USER_ID=171 AND ROLE_ID=175 AND ORG_ID = (SELECT MIN(ID) FROM `core_org` WHERE tenant_id = 1280074884243783680 AND parent_org_id is null)

        cTenantService.changeAdminRole(tenantAdminUserRole);

        return JsonResult.success();
    }


    @PostMapping(MODEL + "/add.json")
    @Function("adminExt.cTenant.add")
    @ResponseBody
    public JsonResult add(@Validated(ValidateConfig.ADD.class)CTenant cTenant)
    {
        cTenantService.SaveCTenant(cTenant);
        return new JsonResult().success();
    }

    @PostMapping(MODEL + "/edit.json")
    @Function("adminExt.cTenant.edit")
    @ResponseBody
    public JsonResult<String> update(@Validated(ValidateConfig.UPDATE.class)  CTenant cTenant) {
        boolean success = cTenantService.update(cTenant);
        if (success) {
            return new JsonResult().success();
        } else {
            return JsonResult.failMessage("保存失败");
        }
    }


   
    @GetMapping(MODEL + "/view.json")
    @Function("adminExt.cTenant.query")
    @ResponseBody
    public JsonResult<CTenant>queryInfo(Long id) {
        CTenant cTenant = cTenantService.queryById( id);
        return  JsonResult.success(cTenant);
    }

    @PostMapping(MODEL + "/delete.json")
    @Function("adminExt.cTenant.delete")
    @ResponseBody
    public JsonResult delete(String ids) {
        if (ids.endsWith(",")) {
            ids = StringUtils.substringBeforeLast(ids, ",");
        }
        List<Long> idList = ConvertUtil.str2longs(ids);
        cTenantService.batchDelCTenant(idList);
        return new JsonResult().success();
    }
    

}
