package com.frame.admin.ext.entity;

import com.frame.admin.core.annotation.Dict;
import com.frame.admin.core.entity.BaseEntity;
import com.frame.admin.core.util.ValidateConfig;
import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.InsertIgnore;
import org.beetl.sql.core.annotatoin.LogicDelete;
import org.beetl.sql.core.annotatoin.SeqID;

import javax.validation.constraints.NotNull;


/* 
* 客户端版本管理
* gen by Spring Boot2 Admin 2020-08-20
*/
public class CClientVersion extends BaseEntity{

    @NotNull(message = "ID不能为空", groups =ValidateConfig.UPDATE.class)
    @SeqID(name = ORACLE_CORE_SEQ_NAME)
    @AssignID("simple")

    private Long id ;


    @Dict(type="c_client_appid")
    private String appId ;

    private Integer versionCode ;
	

    private String versionName ;
	

    private String versionInfo ;
	
    //1强制更新
    @Dict(type="BASE_IS_YES_OR_NO")

    private Integer forceUpdate ;
	

    private String downloadUrl ;
	
    //1安卓，2IOS
    @Dict(type="c_client_ostype")

    private Integer osType ;
	
    @Dict(type="c_client_package_type")

    private String packageType ;

    private String attachmentId ;

    //删除标记
    /*逻辑删除标志*/
    @InsertIgnore
    @LogicDelete(value = 1)
    private Integer delFlag ;

    public CClientVersion(){
    }

    public Long getId(){
        return  id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public Integer getVersionCode(){
        return  versionCode;
    }
    public void setVersionCode(Integer versionCode){
        this.versionCode = versionCode;
    }

    public String getVersionName(){
        return  versionName;
    }
    public void setVersionName(String versionName){
        this.versionName = versionName;
    }

    public String getVersionInfo(){
        return  versionInfo;
    }
    public void setVersionInfo(String versionInfo){
        this.versionInfo = versionInfo;
    }

    /**1强制更新
    *@return 
    */
    public Integer getForceUpdate(){
        return  forceUpdate;
    }
    /**1强制更新
    *@param  forceUpdate
    */
    public void setForceUpdate(Integer forceUpdate){
        this.forceUpdate = forceUpdate;
    }

    public String getDownloadUrl(){
        return  downloadUrl;
    }
    public void setDownloadUrl(String downloadUrl){
        this.downloadUrl = downloadUrl;
    }

    /**1安卓，2IOS
    *@return 
    */
    public Integer getOsType(){
        return  osType;
    }
    /**1安卓，2IOS
    *@param  osType
    */
    public void setOsType(Integer osType){
        this.osType = osType;
    }

    public String getPackageType(){
        return  packageType;
    }
    public void setPackageType(String packageType){
        this.packageType = packageType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }
}
