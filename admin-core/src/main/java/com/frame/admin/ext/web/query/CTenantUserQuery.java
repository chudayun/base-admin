package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;

public class CTenantUserQuery extends PageParam {



	@Query(name="角色",display=true,type=Query.TYPE_CONTROL,control="tenantAdminRole")
	private Long adminRoleId;
	@Query(name="账号",display=true,fuzzy=true)
	private String code ;
	@Query(name="名称",display=true,fuzzy=true)
	private String name ;
	@Query(name="租户名称",display=true)
	private String tenantName;

	private Long tenantId;
	private String adminRoleCode;


	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public String getAdminRoleCode() {
		return adminRoleCode;
	}

	public void setAdminRoleCode(String adminRoleCode) {
		this.adminRoleCode = adminRoleCode;
	}

	public Long getAdminRoleId() {
		return adminRoleId;
	}

	public void setAdminRoleId(Long adminRoleId) {
		this.adminRoleId = adminRoleId;
	}
}
