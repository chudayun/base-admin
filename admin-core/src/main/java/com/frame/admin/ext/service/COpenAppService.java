package com.frame.admin.ext.service;


import com.frame.admin.ext.dao.COpenAppDao;
import com.frame.admin.ext.entity.COpenApp;



import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.util.PlatformException;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;






/**
 * 应用 Service
 */

@Service
@Transactional
public class COpenAppService extends CoreBaseService<COpenApp>{

    @Autowired private COpenAppDao cOpenAppDao;

    public PageQuery<COpenApp>queryByCondition(PageQuery query){
        PageQuery ret =  cOpenAppDao.queryByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public COpenApp getByAppId(String appId){
        COpenApp ret =  cOpenAppDao.getByAppId(appId);
        return ret;
    }


    public void batchDelCOpenApp(List<Long> ids){
        try {
            cOpenAppDao.batchDelCOpenAppByIds(ids);
        } catch (Exception e) {
            throw new PlatformException("批量删除应用失败", e);
        }
    }
}