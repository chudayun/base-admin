package com.frame.admin.ext.client;

import cn.hutool.core.util.IdUtil;
import com.frame.admin.ext.entity.CSessionKey;
import com.frame.admin.ext.entity.CTenant;
import com.frame.admin.ext.pojo.UserLogin;
import com.frame.admin.ext.service.CSessionKeyService;
import com.frame.admin.ext.service.CTenantService;
import com.frame.admin.core.conf.PasswordConfig.PasswordEncryptService;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.rbac.UserLoginInfo;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.service.CoreUserService;
import com.frame.admin.core.util.PlatformException;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


/**
 * 解决跨域请求 接口
 */
@Controller
public class UserController {

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/client/user";
    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;
    
    
    @Autowired
	CorePlatformService platformService;

	@Autowired
	CoreUserService userService;

	@Autowired
	CTenantService cTenantService;
	
	@Autowired
	PasswordEncryptService passwordEncryptService ;
	
	@Autowired 
	private CSessionKeyService cSessionKeyService;
    

    @RequestMapping(MODEL + "/login.json")
    @ResponseBody
    public JsonResult<HashMap> login(String username,String password,String imei,String phoneModel,String tenantCode) {
    	UserLoginInfo info = null;
		CTenant tenant = null;

    	try {

    		// 指定租户下，用户名密码登录
			tenant = cTenantService.queryById(tenantCode);
			UserLogin userLogin = new UserLogin();
			userLogin.setCode(username);
			userLogin.setPassword(password);
			userLogin.setTenantId(String.valueOf(tenant.getId()));
			info = userService.login(userLogin);

    	}catch(Exception e) {
    		return JsonResult.failMessage(e.getMessage());
    	}
		if (info == null) {
			return JsonResult.failMessage("用户名密码错");
		}
		if(null==tenant) {
			throw new PlatformException("未知的帐号信息");
		}
		CoreUser user = info.getUser();
		CoreOrg currentOrg = info.getOrgs().get(0);
		for (CoreOrg org : info.getOrgs()) {
			if (org.getId() == user.getOrgId()) {
				currentOrg = org;
				break;
			}
		}

		info.setCurrentOrg(currentOrg);
		// 记录登录信息到session
		this.platformService.setLoginUser(info.getUser(), info.getCurrentOrg(), info.getOrgs());
		this.platformService.setCurrentTenant(tenant);
		
		//tenantId ,deviceId, clientType 
		
		//生成sessionkey，记录登录会话信息
		String sessionkey = IdUtil.simpleUUID();
		CSessionKey model = new CSessionKey();
		model.setClientName(username);
		model.setClientType("10000");
		model.setDelFlag(0);
		model.setLoginTime(new Date().getTime()/1000);
		model.setSessionkey(sessionkey);
		model.setStatus(0);
		model.setTenantId(tenant.getId());
		model.setUserId(info.getUser().getId());
		model.setUserCode(info.getUser().getCode());
		model.setUserName(info.getUser().getName());
		model.setImei(imei);
		model.setPhoneModel(phoneModel);
		
		//更新同设备ID 的sessionkey 为无效状态

		cSessionKeyService.updateStatus(imei);

		//保存sessionkey
		cSessionKeyService.save(model);
		HashMap ret = new HashMap<>();
		ret.put("sessionkey", sessionkey);
		ret.put("currentOrg", info.getCurrentOrg());
		ret.put("user", info.getUser());
		return JsonResult.success(ret);
    }
    
    @RequestMapping(MODEL + "/checklogin.json")
    @ResponseBody
    public JsonResult<CSessionKey> checklogin(String sk, Long userid) {
//    	UserLoginInfo info = userService.login(code, password);
//		if (info == null) {
//			
//			return JsonResult.failMessage("用户名密码错");
//		}
//		CoreUser user = info.getUser();
//		CoreOrg currentOrg = info.getOrgs().get(0);
//		for (CoreOrg org : info.getOrgs()) {
//			if (org.getId() == user.getOrgId()) {
//				currentOrg = org;
//				break;
//			}
//		}
//
//		info.setCurrentOrg(currentOrg);
//		// 记录登录信息到session
//		this.platformService.setLoginUser(info.getUser(), info.getCurrentOrg(), info.getOrgs());
    	//System.out.println(sk);
    	
    	CSessionKey model = new CSessionKey();
		model.setClientType("10000");
		model.setDelFlag(0);
		model.setSessionkey(sk);
		model.setStatus(0);
		model.setTenantId(0l);
		model.setUserId(userid);
		List<CSessionKey> rets = cSessionKeyService.queryBySessionkey(model);
		if(rets.size() >0 ) {
			HashMap ret = new HashMap<>();
			ret.put("model", model);
			return JsonResult.success(model);
		}
		
		return JsonResult.fail();
    }
    
    
}
