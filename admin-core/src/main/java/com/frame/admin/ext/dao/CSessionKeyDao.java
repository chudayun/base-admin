package com.frame.admin.ext.dao;

import java.util.List;

import com.frame.admin.ext.entity.CSessionKey;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.beetl.sql.core.engine.PageQuery;


/**
 * 会话信息 Dao
 */
@SqlResource("adminExt.cSessionKey")
public interface CSessionKeyDao extends BaseMapper<CSessionKey>{
    public PageQuery<CSessionKey> queryByCondition(PageQuery query);
    public void batchDelCSessionKeyByIds( List<Long> ids);
    public List<CSessionKey> queryBySessionkey(CSessionKey query);
    public void updateStatus(String imei);
}