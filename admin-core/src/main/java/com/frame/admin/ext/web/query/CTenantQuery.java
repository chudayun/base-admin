package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;
/**
 *租户查询
 */
public class CTenantQuery extends PageParam {
    @Query(name = "租户名称", display = true)
    private String name;
    @Query(name = "二级域名", display = true)
    private String subdomain;
    @Query(name = "自有域名", display = true)
    private String domain;
    @Query(name = "租户状态", display = true,type=Query.TYPE_DICT,dict="c_tenant_status")
    private Integer status;
    @Query(name = "统一社会信用代码", display = true)
    private String creditCode;

    private Long id;
    private String adminRoleCode;


    public String getName(){
        return  name;
    }
    public void setName(String name ){
        this.name = name;
    }
    public String getSubdomain(){
        return  subdomain;
    }
    public void setSubdomain(String subdomain ){
        this.subdomain = subdomain;
    }
    public String getDomain(){
        return  domain;
    }
    public void setDomain(String domain ){
        this.domain = domain;
    }
    public Integer getStatus(){
        return  status;
    }
    public void setStatus(Integer status ){
        this.status = status;
    }
    public String getCreditCode(){
        return  creditCode;
    }
    public void setCreditCode(String creditCode ){
        this.creditCode = creditCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAdminRoleCode() {
        return adminRoleCode;
    }

    public void setAdminRoleCode(String adminRoleCode) {
        this.adminRoleCode = adminRoleCode;
    }
}
