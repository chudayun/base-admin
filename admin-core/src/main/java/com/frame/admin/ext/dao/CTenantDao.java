package com.frame.admin.ext.dao;

import com.frame.admin.ext.entity.CTenant;
import com.frame.admin.ext.web.query.CTenantQuery;
import com.frame.admin.core.entity.CoreUser;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

/**
 * 租户 Dao
 */
@SqlResource("adminExt.cTenant")
public interface CTenantDao extends BaseMapper<CTenant>{
    public PageQuery<CTenant> queryByCondition(PageQuery query);
    public void batchDelCTenantByIds( List<Long> ids);
    public List<CTenant> getListAll();

    public List<CoreUser> getAdminUserByTenant(CTenantQuery query);

    public PageQuery<CoreUser> queryAdminUser(PageQuery query);
}