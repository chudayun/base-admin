package com.frame.admin.ext.web;

import com.frame.admin.ext.entity.CSessionKey;
import com.frame.admin.ext.service.CSessionKeyService;
import com.frame.admin.ext.web.query.CSessionKeyQuery;
import com.frame.admin.core.annotation.Function;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.util.ConvertUtil;
import com.frame.admin.core.util.ValidateConfig;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 会话信息 接口
 */
@Controller
public class CSessionKeyController{

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/adminExt/cSessionKey";


    @Autowired private CSessionKeyService cSessionKeyService;
    
    @Autowired
    FileService fileService;
    /* 页面 */

    @GetMapping(MODEL + "/index.do")
    @Function("adminExt.cSessionKey.query")
    @ResponseBody
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/adminExt/cSessionKey/index.html") ;
        view.addObject("search", CSessionKeyQuery.class.getName());
        return view;
    }

    @GetMapping(MODEL + "/edit.do")
    @Function("adminExt.cSessionKey.edit")
    @ResponseBody
    public ModelAndView edit(Long id) {
        ModelAndView view = new ModelAndView("/adminExt/cSessionKey/edit.html");
        CSessionKey cSessionKey = cSessionKeyService.queryById(id);
        view.addObject("cSessionKey", cSessionKey);
        return view;
    }

    @GetMapping(MODEL + "/add.do")
    @Function("adminExt.cSessionKey.add")
    @ResponseBody
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("/adminExt/cSessionKey/add.html");
        return view;
    }

    /* ajax json */

    @PostMapping(MODEL + "/list.json")
    @Function("adminExt.cSessionKey.query")
    @ResponseBody
    public JsonResult<PageQuery> list(CSessionKeyQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cSessionKeyService.queryByCondition(page);
        return JsonResult.success(page);
    }

    @PostMapping(MODEL + "/add.json")
    @Function("adminExt.cSessionKey.add")
    @ResponseBody
    public JsonResult add(@Validated(ValidateConfig.ADD.class)CSessionKey cSessionKey)
    {
        cSessionKeyService.save(cSessionKey);
        return new JsonResult().success();
    }

    @PostMapping(MODEL + "/edit.json")
    @Function("adminExt.cSessionKey.edit")
    @ResponseBody
    public JsonResult<String> update(@Validated(ValidateConfig.UPDATE.class)  CSessionKey cSessionKey) {
        boolean success = cSessionKeyService.update(cSessionKey);
        if (success) {
            return new JsonResult().success();
        } else {
            return JsonResult.failMessage("保存失败");
        }
    }


   
    @GetMapping(MODEL + "/view.json")
    @Function("adminExt.cSessionKey.query")
    @ResponseBody
    public JsonResult<CSessionKey>queryInfo(Long id) {
        CSessionKey cSessionKey = cSessionKeyService.queryById( id);
        return  JsonResult.success(cSessionKey);
    }

    @PostMapping(MODEL + "/delete.json")
    @Function("adminExt.cSessionKey.delete")
    @ResponseBody
    public JsonResult delete(String ids) {
        if (ids.endsWith(",")) {
            ids = StringUtils.substringBeforeLast(ids, ",");
        }
        List<Long> idList = ConvertUtil.str2longs(ids);
        cSessionKeyService.batchDelCSessionKey(idList);
        return new JsonResult().success();
    }
    

}
