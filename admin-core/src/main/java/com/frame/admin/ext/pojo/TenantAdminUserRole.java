package com.frame.admin.ext.pojo;

import java.io.Serializable;

public class TenantAdminUserRole implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -3019465885046702022L;
	private Long userId;

    private Long roleId;

    private Long orgId;

    private Long tenantId;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
