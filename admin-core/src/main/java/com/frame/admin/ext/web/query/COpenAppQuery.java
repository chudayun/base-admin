package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;

/**
 *应用查询
 */
public class COpenAppQuery extends PageParam {
    @Query(name = "appId", display = true)
    private String appId;
    @Query(name = "appName", display = true)
    private String appName;
    @Query(name = "加密规则", display = true,type=Query.TYPE_DICT,dict="c_app_encryption_rules")
    private Integer rules;
    public String getAppId(){
        return  appId;
    }
    public void setAppId(String appId ){
        this.appId = appId;
    }
    public String getAppName(){
        return  appName;
    }
    public void setAppName(String appName ){
        this.appName = appName;
    }
    public Integer getRules(){
        return  rules;
    }
    public void setRules(Integer rules ){
        this.rules = rules;
    }
 
}
