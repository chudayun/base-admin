package com.frame.admin.ext.web;

import java.util.Date;
import java.util.List;

import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.util.ConvertUtil;
import com.frame.admin.core.util.ValidateConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.frame.admin.core.annotation.Function;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.web.JsonResult;
import com.frame.admin.ext.entity.*;
import com.frame.admin.ext.service.*;
import com.frame.admin.ext.web.query.*;

/**
 * 应用 接口
 */
@Controller
public class COpenAppController{

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/adminExt/cOpenApp";

    @Autowired
    CorePlatformService platformService ;
    @Autowired private COpenAppService cOpenAppService;
    
    @Autowired
    FileService fileService;
    /* 页面 */

    @GetMapping(MODEL + "/index.do")
    @Function("adminExt.cOpenApp.query")
    @ResponseBody
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/adminExt/cOpenApp/index.html") ;
        view.addObject("search", COpenAppQuery.class.getName());
        return view;
    }

    @GetMapping(MODEL + "/edit.do")
    @Function("adminExt.cOpenApp.edit")
    @ResponseBody
    public ModelAndView edit(Long id) {
        ModelAndView view = new ModelAndView("/adminExt/cOpenApp/edit.html");
        COpenApp cOpenApp = cOpenAppService.queryById(id);
        view.addObject("cOpenApp", cOpenApp);
        return view;
    }

    @GetMapping(MODEL + "/add.do")
    @Function("adminExt.cOpenApp.add")
    @ResponseBody
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("/adminExt/cOpenApp/add.html");
        return view;
    }

    /* ajax json */

    @PostMapping(MODEL + "/list.json")
    @Function("adminExt.cOpenApp.query")
    @ResponseBody
    public JsonResult<PageQuery> list(COpenAppQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cOpenAppService.queryByCondition(page);
        return JsonResult.success(page);
    }

    @PostMapping(MODEL + "/add.json")
    @Function("adminExt.cOpenApp.add")
    @ResponseBody
    public JsonResult add(@Validated(ValidateConfig.ADD.class)COpenApp cOpenApp)
    {
        cOpenApp.setCreateId(platformService.getCurrentUser().getId());
        cOpenApp.setCreateTime(new Date());
        cOpenAppService.save(cOpenApp);
        return new JsonResult().success();
    }

    @PostMapping(MODEL + "/edit.json")
    @Function("adminExt.cOpenApp.edit")
    @ResponseBody
    public JsonResult<String> update(@Validated(ValidateConfig.UPDATE.class)  COpenApp cOpenApp) {
        boolean success = cOpenAppService.update(cOpenApp);
        if (success) {
            return new JsonResult().success();
        } else {
            return JsonResult.failMessage("保存失败");
        }
    }


   
    @GetMapping(MODEL + "/view.json")
    @Function("adminExt.cOpenApp.query")
    @ResponseBody
    public JsonResult<COpenApp>queryInfo(Long id) {
        COpenApp cOpenApp = cOpenAppService.queryById( id);
        return  JsonResult.success(cOpenApp);
    }

    @PostMapping(MODEL + "/delete.json")
    @Function("adminExt.cOpenApp.delete")
    @ResponseBody
    public JsonResult delete(String ids) {
        if (ids.endsWith(",")) {
            ids = StringUtils.substringBeforeLast(ids, ",");
        }
        List<Long> idList = ConvertUtil.str2longs(ids);
        cOpenAppService.batchDelCOpenApp(idList);
        return new JsonResult().success();
    }
    

}
