package com.frame.admin.ext.service;


import com.frame.admin.ext.dao.CConfigDao;
import com.frame.admin.ext.entity.CConfig;


import com.frame.admin.ext.web.query.CConfigQuery;
import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.util.PlatformException;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;






/**
 * 系统配置 Service
 */

@Service
@Transactional
public class CConfigService extends CoreBaseService<CConfig>{

    @Autowired private CConfigDao cConfigDao;

    public PageQuery<CConfig>queryByCondition(PageQuery query){
        PageQuery ret =  cConfigDao.queryByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public List<CConfig> queryTenantCfgByCondition(CConfigQuery query){
        List<CConfig> list =  cConfigDao.queryTenantCfgByCondition(query);
        queryListAfter(list);
        return list;
    }


    public void batchDelCConfig(List<Long> ids){
        try {
            cConfigDao.batchDelCConfigByIds(ids);
        } catch (Exception e) {
            throw new PlatformException("批量删除系统配置失败", e);
        }
    }


    /**
     * 单个配置信息
     * @param cConfig
     * @return
     */
    public CConfig getCconfig(CConfig cConfig){

        cConfig.setDelFlag(0);
        cConfig = cConfigDao.templateOne(cConfig);
        return cConfig;
    }
}