package com.frame.admin.ext.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.beetl.sql.core.annotatoin.*;

import com.frame.admin.core.util.ValidateConfig;

import com.frame.admin.core.annotation.Dict;
import com.frame.admin.core.entity.BaseEntity;


/* 
* 租户信息表
* gen by Spring Boot2 Admin 2020-06-12
*/
public class CTenant extends BaseEntity{

    @NotNull(message = "ID不能为空", groups =ValidateConfig.UPDATE.class)
    @SeqID(name = ORACLE_CORE_SEQ_NAME)
    @AssignID("simple")

    private Long id ;
	
    //租户名称

    private String name ;
	
    //二级域名

    private String subdomain ;
	
    //自有域名

    private String domain ;
	
    //状态，正常、欠费、注销、停用
    @Dict(type="c_tenant_status")

    private Integer status ;
	

    private String creditCode ;
	
    //租户创建时间
    @UpdateIgnore
    private Date createTime ;
	
    //租户创建人姓名
    @UpdateIgnore
    private String createName ;
	
    //租户创建人id（内部时）
    @UpdateIgnore
    private Long createId ;

    //管理员账号
    private String adminCode;

    //管理员名称
    private String adminName;


	
    public CTenant(){
    }

    public Long getId(){
        return  id;
    }
    public void setId(Long id){
        this.id = id;
    }

    /**租户名称
    *@return 
    */
    public String getName(){
        return  name;
    }
    /**租户名称
    *@param  name
    */
    public void setName(String name){
        this.name = name;
    }

    /**二级域名
    *@return 
    */
    public String getSubdomain(){
        return  subdomain;
    }
    /**二级域名
    *@param  subdomain
    */
    public void setSubdomain(String subdomain){
        this.subdomain = subdomain;
    }

    /**自有域名
    *@return 
    */
    public String getDomain(){
        return  domain;
    }
    /**自有域名
    *@param  domain
    */
    public void setDomain(String domain){
        this.domain = domain;
    }

    /**状态，正常、欠费、注销、停用
    *@return 
    */
    public Integer getStatus(){
        return  status;
    }
    /**状态，正常、欠费、注销、停用
    *@param  status
    */
    public void setStatus(Integer status){
        this.status = status;
    }

    public String getCreditCode(){
        return  creditCode;
    }
    public void setCreditCode(String creditCode){
        this.creditCode = creditCode;
    }

    /**租户创建时间
    *@return 
    */
    public Date getCreateTime(){
        return  createTime;
    }
    /**租户创建时间
    *@param  createTime
    */
    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    /**租户创建人姓名
    *@return 
    */
    public String getCreateName(){
        return  createName;
    }
    /**租户创建人姓名
    *@param  createName
    */
    public void setCreateName(String createName){
        this.createName = createName;
    }

    /**租户创建人id（内部时）
    *@return 
    */
    public Long getCreateId(){
        return  createId;
    }
    /**租户创建人id（内部时）
    *@param  createId
    */
    public void setCreateId(Long createId){
        this.createId = createId;
    }


    public String getAdminCode() {
        return adminCode;
    }

    public void setAdminCode(String adminCode) {
        this.adminCode = adminCode;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }


}
