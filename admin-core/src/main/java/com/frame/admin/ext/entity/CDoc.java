package com.frame.admin.ext.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.SeqID;
import org.beetl.sql.core.annotatoin.UpdateIgnore;

import com.frame.admin.core.util.ValidateConfig;

import com.frame.admin.core.annotation.Dict;
import com.frame.admin.core.entity.BaseEntity;

import org.beetl.sql.core.annotatoin.InsertIgnore;
import org.beetl.sql.core.annotatoin.LogicDelete;


/* 
* 系统文档
* gen by Spring Boot2 Admin 2020-04-27
*/
public class CDoc extends BaseEntity{

    @NotNull(message = "ID不能为空", groups =ValidateConfig.UPDATE.class)
    @SeqID(name = ORACLE_CORE_SEQ_NAME)
    @AssignID("simple")

    private Long id ;
	

    private String title ;
	

    private String thumbnail ;
	

    private String url ;
	

    private String content ;
	
    //0 文档，1 视频，2链接
    @Dict(type="c_doc_type")

    private Integer type ;
	

    private String remark ;
	

    private Long catalog ;
	
    @UpdateIgnore
    private Long createId ;
	
    @UpdateIgnore
    private Date createTime ;
	
	/*逻辑删除标志*/
	@InsertIgnore
	@LogicDelete(value = 1)
    private Integer delFlag ;
	
    public CDoc(){
    }

    public Long getId(){
        return  id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public String getTitle(){
        return  title;
    }
    public void setTitle(String title){
        this.title = title;
    }

    public String getThumbnail(){
        return  thumbnail;
    }
    public void setThumbnail(String thumbnail){
        this.thumbnail = thumbnail;
    }

    public String getUrl(){
        return  url;
    }
    public void setUrl(String url){
        this.url = url;
    }

    public String getContent(){
        return  content;
    }
    public void setContent(String content){
        this.content = content;
    }

    /**0 文档，1 视频，2链接
    *@return 
    */
    public Integer getType(){
        return  type;
    }
    /**0 文档，1 视频，2链接
    *@param  type
    */
    public void setType(Integer type){
        this.type = type;
    }

    public String getRemark(){
        return  remark;
    }
    public void setRemark(String remark){
        this.remark = remark;
    }

    public Long getCatalog(){
        return  catalog;
    }
    public void setCatalog(Long catalog){
        this.catalog = catalog;
    }

    public Long getCreateId(){
        return  createId;
    }
    public void setCreateId(Long createId){
        this.createId = createId;
    }

    public Date getCreateTime(){
        return  createTime;
    }
    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    public Integer getDelFlag(){
        return  delFlag;
    }
    public void setDelFlag(Integer delFlag){
        this.delFlag = delFlag;
    }


}
