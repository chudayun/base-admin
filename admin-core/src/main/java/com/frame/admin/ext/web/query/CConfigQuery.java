package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;

/**
 *系统配置查询
 */
public class CConfigQuery extends PageParam {


    @Query(name = "类别", display = true,type=Query.TYPE_DICT,dict="c_config_type")
    private Integer type;
    @Query(name = "目录", display = true,type=Query.TYPE_DICT,dict="c_config_catalog")
    private String catalog;
    @Query(name = "代码", display = true)
    private String code;
    @Query(name = "名称", display = true)
    private String name;
    private Long tenantId;

    @Query(name = "删除标记", display = false)
    private Integer delFlag;
    public String getName(){
        return  name;
    }
    public void setName(String name ){
        this.name = name;
    }
    public String getCode(){
        return  code;
    }
    public void setCode(String code ){
        this.code = code;
    }
    public String getCatalog(){
        return  catalog;
    }
    public void setCatalog(String catalog ){
        this.catalog = catalog;
    }
    public Integer getType(){
        return  type;
    }
    public void setType(Integer type ){
        this.type = type;
    }
    public Integer getDelFlag(){
        return  delFlag;
    }
    public void setDelFlag(Integer delFlag ){
        this.delFlag = delFlag;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
