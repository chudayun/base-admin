package com.frame.admin.ext.dao;

import com.frame.admin.ext.entity.CClientVersion;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

/**
 * 客户端版本 Dao
 */
@SqlResource("adminExt.cClientVersion")
public interface CClientVersionDao extends BaseMapper<CClientVersion>{
    public PageQuery<CClientVersion> queryByCondition(PageQuery query);
    public void batchDelCClientVersionByIds( List<Long> ids);
    public CClientVersion getLatestVersion(CClientVersion version);

}