package com.frame.admin.ext.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.beetl.sql.core.engine.PageQuery;

import  com.frame.admin.ext.entity.*;

/**
 * 系统通知 Dao
 */
@SqlResource("adminExt.cNotice")
public interface CNoticeDao extends BaseMapper<CNotice>{
    public PageQuery<CNotice> queryByCondition(PageQuery query);
    public void batchDelCNoticeByIds( List<Long> ids);
    public PageQuery<CNotice> queryReceiveListByCondition(PageQuery query);
}