package com.frame.admin.ext.web;

import java.util.List;

import com.frame.admin.core.util.ConvertUtil;
import com.frame.admin.core.util.ValidateConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.frame.admin.core.annotation.Function;
import com.frame.admin.core.file.FileService;
import com.frame.admin.core.web.JsonResult;
import com.frame.admin.ext.entity.*;
import com.frame.admin.ext.service.*;
import com.frame.admin.ext.web.query.*;

/**
 * 系统文档 接口
 */
@Controller
public class CDocController{

    private final Log log = LogFactory.getLog(this.getClass());
    private static final String MODEL = "/adminExt/cDoc";


    @Autowired private CDocService cDocService;
    
    @Autowired
    FileService fileService;
    /* 页面 */

    @GetMapping(MODEL + "/index.do")
    @Function("adminExt.cDoc.query")
    @ResponseBody
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/adminExt/cDoc/index.html") ;
        view.addObject("search", CDocQuery.class.getName());
        return view;
    }

    @GetMapping(MODEL + "/edit.do")
    @Function("adminExt.cDoc.edit")
    @ResponseBody
    public ModelAndView edit(Long id) {
        ModelAndView view = new ModelAndView("/adminExt/cDoc/edit.html");
        CDoc cDoc = cDocService.queryById(id);
        view.addObject("cDoc", cDoc);
        return view;
    }

    @GetMapping(MODEL + "/add.do")
    @Function("adminExt.cDoc.add")
    @ResponseBody
    public ModelAndView add() {
        ModelAndView view = new ModelAndView("/adminExt/cDoc/add.html");
        return view;
    }

    /* ajax json */

    @PostMapping(MODEL + "/list.json")
    @Function("adminExt.cDoc.query")
    @ResponseBody
    public JsonResult<PageQuery> list(CDocQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cDocService.queryByCondition(page);
        return JsonResult.success(page);
    }

    @PostMapping(MODEL + "/add.json")
    @Function("adminExt.cDoc.add")
    @ResponseBody
    public JsonResult add(@Validated(ValidateConfig.ADD.class)CDoc cDoc)
    {
        cDocService.save(cDoc);
        return new JsonResult().success();
    }

    @PostMapping(MODEL + "/edit.json")
    @Function("adminExt.cDoc.edit")
    @ResponseBody
    public JsonResult<String> update(@Validated(ValidateConfig.UPDATE.class)  CDoc cDoc) {
        boolean success = cDocService.update(cDoc);
        if (success) {
            return new JsonResult().success();
        } else {
            return JsonResult.failMessage("保存失败");
        }
    }


   
    @GetMapping(MODEL + "/view.json")
    @Function("adminExt.cDoc.query")
    @ResponseBody
    public JsonResult<CDoc>queryInfo(Long id) {
        CDoc cDoc = cDocService.queryById( id);
        return  JsonResult.success(cDoc);
    }

    @PostMapping(MODEL + "/delete.json")
    @Function("adminExt.cDoc.delete")
    @ResponseBody
    public JsonResult delete(String ids) {
        if (ids.endsWith(",")) {
            ids = StringUtils.substringBeforeLast(ids, ",");
        }
        List<Long> idList = ConvertUtil.str2longs(ids);
        cDocService.batchDelCDoc(idList);
        return new JsonResult().success();
    }
    
    

    @RequestMapping(value=MODEL + "/doclist.json")
    @ResponseBody
    public JsonResult<PageQuery> doclist(CDocQuery condtion)
    {
        PageQuery page = condtion.getPageQuery();
        cDocService.queryByCondition(page);
        return JsonResult.success(page);
    }

}
