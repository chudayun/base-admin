package com.frame.admin.ext.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.beetl.sql.core.annotatoin.*;

import com.frame.admin.core.util.ValidateConfig;

import com.frame.admin.core.annotation.Dict;
import com.frame.admin.core.entity.BaseEntity;


/* 
* 系统配置
* gen by Spring Boot2 Admin 2020-05-15
*/
public class CConfig extends BaseEntity{

    @NotNull(message = "ID不能为空", groups =ValidateConfig.UPDATE.class)
    @SeqID(name = ORACLE_CORE_SEQ_NAME)
    @AssignID("simple")

    private Long id ;
	

    private String name ;
	

    private String code ;
	

    private String value ;
	
    @Dict(type="c_config_catalog")

    private String catalog ;
	
    //类别，0 不可变，1 client 2 管理端
    @Dict(type="c_config_type")

    private Integer type ;
	

    @UpdateIgnore
    private Date createTime ;

    @UpdateIgnore
    private Long createId ;
	
    @InsertIgnore
    private Long updateId ;

    @InsertIgnore
    private Date updateTime ;
	
    //删除标记
	/*逻辑删除标志*/
	@InsertIgnore
	@LogicDelete(value = 1)

    private Integer delFlag ;

    @UpdateIgnore
    private Long tenantId ;
	
    public CConfig(){
    }

    public Long getId(){
        return  id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public String getName(){
        return  name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getCode(){
        return  code;
    }
    public void setCode(String code){
        this.code = code;
    }

    public String getValue(){
        return  value;
    }
    public void setValue(String value){
        this.value = value;
    }

    public String getCatalog(){
        return  catalog;
    }
    public void setCatalog(String catalog){
        this.catalog = catalog;
    }

    /**类别，0 不可变，1 client 2 管理端
    *@return 
    */
    public Integer getType(){
        return  type;
    }
    /**类别，0 不可变，1 client 2 管理端
    *@param  type
    */
    public void setType(Integer type){
        this.type = type;
    }

    public Date getCreateTime(){
        return  createTime;
    }
    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    public Long getCreateId(){
        return  createId;
    }
    public void setCreateId(Long createId){
        this.createId = createId;
    }

    public Long getUpdateId(){
        return  updateId;
    }
    public void setUpdateId(Long updateId){
        this.updateId = updateId;
    }

    public Date getUpdateTime(){
        return  updateTime;
    }
    public void setUpdateTime(Date updateTime){
        this.updateTime = updateTime;
    }

    /**删除标记
    *@return 
    */
    public Integer getDelFlag(){
        return  delFlag;
    }
    /**删除标记
    *@param  delFlag
    */
    public void setDelFlag(Integer delFlag){
        this.delFlag = delFlag;
    }

    public Long getTenantId(){
        return  tenantId;
    }
    public void setTenantId(Long tenantId){
        this.tenantId = tenantId;
    }


}
