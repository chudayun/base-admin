package com.frame.admin.ext.service;




import com.frame.admin.ext.dao.CSessionKeyDao;
import com.frame.admin.ext.entity.CSessionKey;
import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.util.PlatformException;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;






/**
 * 会话信息 Service
 */

@Service
@Transactional
public class CSessionKeyService extends CoreBaseService<CSessionKey> {

    @Autowired private CSessionKeyDao cSessionKeyDao;

    public PageQuery<CSessionKey>queryByCondition(PageQuery query){
        PageQuery ret =  cSessionKeyDao.queryByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public void batchDelCSessionKey(List<Long> ids){
        try {
            cSessionKeyDao.batchDelCSessionKeyByIds(ids);
        } catch (Exception e) {
            throw new PlatformException("批量删除会话信息失败", e);
        }
    }
    
    public List<CSessionKey> queryBySessionkey(CSessionKey query){
    	List<CSessionKey> ret =  cSessionKeyDao.queryBySessionkey(query);
        return ret;
    }
    
    public void updateStatus(String imei){
        cSessionKeyDao.updateStatus(imei);
    }
}