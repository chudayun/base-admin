package com.frame.admin.ext.config;


import cn.hutool.json.JSONUtil;
import com.frame.admin.ext.entity.CTenant;
import com.frame.admin.ext.resolver.CurrentUserMethodArgumentResolver;
import com.frame.admin.ext.service.CSessionKeyService;
import com.frame.admin.ext.service.CTenantService;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.service.CoreUserService;
import com.frame.admin.core.util.HttpRequestLocal;

import com.frame.admin.core.util.PlatformException;
import com.frame.admin.core.web.JsonResult;
import org.beetl.core.GroupTemplate;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class MVCConf implements WebMvcConfigurer, InitializingBean {

    public static final String DEFAULT_APP_NAME = "开发平台";

    /**
     * 系统名称,可以在application.properties中配置
     * app.name=xxx
     */
//    @Value("${app.name}")
//    String appName;

    // 开发用的模拟当前用户和机构
    Long useId;

    Long orgId;

    String mvcTestPath;

    @Autowired
    Environment env;

    @Autowired
    CoreUserService userService;
    
    @Autowired
    CSessionKeyService cSessionKeyService;

    @Autowired
    BeetlGroupUtilConfiguration beetlGroupUtilConfiguration;

    @Autowired
    HttpRequestLocal httpRequestLocal;

    @Autowired
    GroupTemplate groupTemplate;

    @Autowired
    CTenantService cTenantService;

    @Autowired
    CorePlatformService corePlatformService;
    
    private static final List<String> EXCLUDE_PATH= Arrays.asList("/css/**","/js/**","/img/**","/imgs/**","/media/**","/eweb/**","/plugins/**");

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new SessionInterceptor(httpRequestLocal, this)).addPathPatterns("/**").excludePathPatterns(EXCLUDE_PATH);
        // super.addInterceptors(registry);

    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addFormatter(new DateFormatter("yyyy-MM-dd HH:mm:ss"));
       // registry.addFormatter(new DateFormatter("yyyy-MM-dd"));
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        this.useId = env.getProperty("user.id", Long.class);
        this.orgId = env.getProperty("user.orgId", Long.class);
        this.mvcTestPath = env.getProperty("mvc.test.path");
        Map<String, Object> var = new HashMap<>(5);
        String appName =  env.getProperty("app.name");
        if(appName==null) {
        	 var.put("appName",DEFAULT_APP_NAME);
      
        }
        
        var.put("jsVer", System.currentTimeMillis());
       
       groupTemplate.setSharedVars(var);
        
        
   
       
    }

    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        String os = System.getProperty("os.name");
        String localStaticFile = env.getProperty("localStaticFile.root");//静态资源的存储跟目录
        String localStaticFileDir = env.getProperty("localStaticFile.dir");//请求静态资源的 地址目录
        //registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        
        if (os.toLowerCase().startsWith("win")) {  //如果是Windows系统
            registry.addResourceHandler("/"+localStaticFileDir+"/**")
                    // /apple/**表示在磁盘apple目录下的所有资源会被解析为以下的路径
                    .addResourceLocations("file:"+localStaticFile+"/") //媒体资源
                    .addResourceLocations("classpath:/META-INF/resources/");  //swagger2页面
        } else {  //linux 和mac
            registry.addResourceHandler("/"+localStaticFileDir+"/**")
                    .addResourceLocations("file:"+localStaticFile+"/")   //媒体资源
                    .addResourceLocations("classpath:/META-INF/resources/");  //swagger2页面;
        }
    }
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new CurrentUserMethodArgumentResolver());
    }
}

class SessionInterceptor implements HandlerInterceptor {

    MVCConf conf;
    HttpRequestLocal httpRequestLocal;

    public SessionInterceptor(HttpRequestLocal httpRequestLocal, MVCConf conf) {
        this.conf = conf;
        this.httpRequestLocal = httpRequestLocal;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        httpRequestLocal.set(request);
        if (conf.useId != null && conf.orgId != null
                && request.getSession().getAttribute(CorePlatformService.ACCESS_CURRENT_USER) == null) {
            // 模拟用户登录，用于快速开发,未来用rember么代替？
            CoreUser user = conf.userService.getUserById(conf.useId);
            CoreOrg org = conf.userService.getOrgById(conf.orgId);
            List<CoreOrg> orgs = conf.userService.getUserOrg(conf.useId, org.getId());
            CTenant tenant = conf.cTenantService.queryById(user.getTenantId());
            conf.corePlatformService.setLoginUser(user,org,orgs);
            request.getSession().setAttribute(CorePlatformService.TENANT_CURRENT, tenant);
            request.getSession().setAttribute("ip", request.getRemoteHost());

        }

        

        
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // do nothing
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // do nothing
    }


    private void returnJson(HttpServletResponse response){
        PrintWriter writer = null;
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            writer = response.getWriter();

            writer.print(JSONUtil.parse(new JsonResult().httpNotToken(null)));
        } catch (IOException e){
            throw new PlatformException("拦截器输出流异常",e);
        } finally {
            if(writer != null){
                writer.close();
            }
        }
    }
}
