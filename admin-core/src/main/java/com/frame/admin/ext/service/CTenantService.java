package com.frame.admin.ext.service;


import com.frame.admin.ext.dao.CTenantDao;
import com.frame.admin.ext.entity.CTenant;


import com.frame.admin.ext.pojo.TenantAdminUserRole;
import com.frame.admin.ext.web.query.CTenantQuery;
import com.frame.admin.core.conf.PasswordConfig;
import com.frame.admin.core.dao.CoreOrgDao;
import com.frame.admin.core.dao.CoreUserDao;
import com.frame.admin.core.dao.CoreUserRoleDao;
import com.frame.admin.core.entity.BaseSessionResultQuery;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.entity.CoreUserRole;
import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.util.PlatformException;
import com.frame.admin.core.util.enums.DelFlagEnum;
import com.frame.admin.core.util.enums.GeneralStateEnum;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;






/**
 * 租户 Service
 */

@Service
@Transactional
public class CTenantService extends CoreBaseService<CTenant>{

    @Autowired private CTenantDao cTenantDao;

    @Autowired
    CorePlatformService platformService;

    @Autowired
    PasswordConfig.PasswordEncryptService passwordEncryptService;


    @Autowired
    CoreUserDao coreUserDao;


    @Autowired
    CoreOrgDao coreOrgDao;

    @Autowired
    CoreUserRoleDao coreUserRoleDao;

    @Autowired
    Environment env;



    public PageQuery<CTenant>queryByCondition(PageQuery query){
        PageQuery ret =  cTenantDao.queryByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public void batchDelCTenant(List<Long> ids){
        try {
            cTenantDao.batchDelCTenantByIds(ids);
        } catch (Exception e) {
            throw new PlatformException("批量删除租户失败", e);
        }
    }

    //所有状态正常的租户

    public List<CTenant> getListAll(){

        return cTenantDao.getListAll();
    }

    public List<CoreUser> getAdminUserByTenant(CTenantQuery query){
        query.setAdminRoleCode("TENANT_ADMIN");
        return cTenantDao.getAdminUserByTenant(query);
    }

    public PageQuery<CoreUser> queryAdminUser(PageQuery query){
        PageQuery ret =  cTenantDao.queryAdminUser(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public void changeAdminRole(TenantAdminUserRole tenantAdminUserRole){
        CoreOrg orgRoot = coreOrgDao.getTenantOrgRoot(tenantAdminUserRole.getTenantId());
        tenantAdminUserRole.setOrgId(orgRoot.getId());
        //根据用户id，删除用户角色信息
        //插入角色信息
        CoreUserRole coreUserRole = new CoreUserRole();
        coreUserRole.setOrgId(tenantAdminUserRole.getOrgId());
        coreUserRole.setRoleId(tenantAdminUserRole.getRoleId());
        coreUserRole.setUserId(tenantAdminUserRole.getUserId());
        coreUserRoleDao.deleteByUserId(coreUserRole);//用户id和 机构id
        coreUserRoleDao.insert(coreUserRole);
    }




    //租户显示隐藏\平台/租户判断、租户号是否存在
    public String[] isHide(CTenant cTenant){
        String[] result = null;
        //判断是平台还是租户
        if(cTenant !=null) {
            if (cTenant.getId().equals(platformService.TENANT_DEFAULT_ID)){//平台
                result = new String[]{"block","1",""};
            }else{
                result = new String[]{"none","0",cTenant.getId().toString()};
            }
        }else{
            result = new String[]{"none","2","租户不存在"};
        }
        return result;
    }



    /**
     * 新增租户,默认创建租户管理员并赋予租户管理员权限
     * @param cTenant
     * @return
     */
    public void SaveCTenant(CTenant cTenant){

        try{
            BaseSessionResultQuery q = platformService.getBaseSession();



            //新建租户
            cTenant.setCreateName(q.getUserName());
            cTenant.setCreateId(q.getUserId());
            cTenant.setCreateTime(new Date());
            cTenantDao.insert(cTenant);


            //新建根目录
            CoreOrg org  = new CoreOrg();
            org.setTenantId(cTenant.getId());
            org.setCode(cTenant.getName());
            org.setName(cTenant.getName());
            org.setCreateTime(new Date());
            org.setType("ORGT0");
            coreOrgDao.insert(org,true);
            org.setSeq("."+org.getId()+".");
            coreOrgDao.updateTemplateById(org);


            //新增管理员,用戶
            CoreUser user = new CoreUser();
            user.setTenantId(cTenant.getId());
            user.setCode(cTenant.getAdminCode());
            user.setName(cTenant.getAdminName());
            user.setState(GeneralStateEnum.ENABLE.getValue());
            user.setPassword(passwordEncryptService.password(env.getProperty("sys_password")));
            user.setDelFlag(DelFlagEnum.NORMAL.getValue());
            user.setCreateTime(new Date());
            user.setOrgId(org.getId());
            user.setState("S1");
            user.setTenantAdmin(1); //用户标记为租户超级管理员



            coreUserDao.insert(user,true);

            //新增角色关系，租户超级管理员 CODE和ID为常量

            //初始角色权限
            CoreUserRole r = new CoreUserRole();
            r.setCreateTime(new Date());
            r.setUserId(user.getId());
            r.setOrgId(org.getId());
            r.setRoleId(platformService.TENANT_ADMIN_ID);


            sqlManager.insert(r);
        }catch (Exception e) {
            throw new PlatformException("租户增加失败", e);
        }

    }

}