package com.frame.admin.ext.pojo;

import java.io.Serializable;

public class UserLogin implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7369871082136901305L;
	private String code;
	private String tenantId;
	private String tenantCode;
	private String password;
	private String verificationCode;//短信验证码 
	private String mobileNo;
	private String loginType;//用户名密码登录，手机号验证码登录，手机号 密码 登录
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getVerificationCode() {
		return verificationCode;
	}
	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getLoginType() {
		return loginType;
	}
	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}
	public String getTenantCode() {
		return tenantCode;
	}
	public void setTenantCode(String tenantCode) {
		this.tenantCode = tenantCode;
	}
	
	
}
