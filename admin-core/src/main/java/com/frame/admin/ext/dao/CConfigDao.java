package com.frame.admin.ext.dao;

import java.util.List;

import com.frame.admin.ext.web.query.CConfigQuery;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.beetl.sql.core.engine.PageQuery;

import  com.frame.admin.ext.entity.*;

/**
 * 系统配置 Dao
 */
@SqlResource("adminExt.cConfig")
public interface CConfigDao extends BaseMapper<CConfig>{
    public PageQuery<CConfig> queryByCondition(PageQuery query);
    public List<CConfig> queryTenantCfgByCondition(CConfigQuery query);

    public void batchDelCConfigByIds( List<Long> ids);
}