package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;
/**
 *客户端版本查询
 */
public class CClientVersionQuery extends PageParam {
    @Query(name = "APPID", display = true,type=Query.TYPE_DICT,dict="c_client_appid")
    private String appId;
    @Query(name = "版本号", display = true)
    private Integer versionCode;
    @Query(name = "版本名称", display = true)
    private String versionName;
    @Query(name = "操作系统", display = true,type=Query.TYPE_DICT,dict="c_client_ostype")
    private Integer osType;
    @Query(name = "更新包类别", display = true,type=Query.TYPE_DICT,dict="c_client_package_type")
    private String packageType;
    public Integer getVersionCode(){
        return  versionCode;
    }
    public void setVersionCode(Integer versionCode ){
        this.versionCode = versionCode;
    }
    public String getVersionName(){
        return  versionName;
    }
    public void setVersionName(String versionName ){
        this.versionName = versionName;
    }
    public Integer getOsType(){
        return  osType;
    }
    public void setOsType(Integer osType ){
        this.osType = osType;
    }
    public String getPackageType(){
        return  packageType;
    }
    public void setPackageType(String packageType ){
        this.packageType = packageType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
