package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;

/**
 *系统通知查询
 */
public class CNoticeQuery extends PageParam {
    @Query(name = "标题", display = true)
    private String title;
    public String getTitle(){
        return  title;
    }
    public void setTitle(String title ){
        this.title = title;
    }
 
}
