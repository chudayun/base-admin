package com.frame.admin.ext.service;


import com.frame.admin.ext.dao.CDocDao;
import com.frame.admin.ext.entity.CDoc;



import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.util.PlatformException;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;






/**
 * 系统文档 Service
 */

@Service
@Transactional
public class CDocService extends CoreBaseService<CDoc>{

    @Autowired private CDocDao cDocDao;

    public PageQuery<CDoc>queryByCondition(PageQuery query){
        PageQuery ret =  cDocDao.queryByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public void batchDelCDoc(List<Long> ids){
        try {
            cDocDao.batchDelCDocByIds(ids);
        } catch (Exception e) {
            throw new PlatformException("批量删除系统文档失败", e);
        }
    }
}