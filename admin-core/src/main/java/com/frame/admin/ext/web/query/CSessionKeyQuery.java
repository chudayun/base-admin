package com.frame.admin.ext.web.query;

import com.frame.admin.core.annotation.Query;
import com.frame.admin.core.web.query.PageParam;

/**
 *会话信息查询
 */
public class CSessionKeyQuery extends PageParam {
    @Query(name = "会话key", display = true)
    private String sessionkey;
    @Query(name = "用户ID", display = true)
    private Long userId;
    @Query(name = "用户编码", display = true)
    private String userCode;
    @Query(name = "登录时间", display = false)
    private Long loginTime;

    @Query(name = "状态 ", display = true,type=Query.TYPE_DICT,dict="session_key_state")
   // @Query(name = "状态 ", display = true,type=Query.TYPE_DICT,dict="session_key_state")
    private Integer status;

    private Long tenantId;
    private String imei;
    public String getSessionkey(){
        return  sessionkey;
    }
    public void setSessionkey(String sessionkey ){
        this.sessionkey = sessionkey;
    }
    public Long getUserId(){
        return  userId;
    }
    public void setUserId(Long userId ){
        this.userId = userId;
    }
    public String getUserCode(){
        return  userCode;
    }
    public void setUserCode(String userCode ){
        this.userCode = userCode;
    }
    public Long getLoginTime(){
        return  loginTime;
    }
    public void setLoginTime(Long loginTime ){
        this.loginTime = loginTime;
    }
    public Integer getStatus(){
        return  status;
    }
    public void setStatus(Integer status ){
        this.status = status;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }
}
