package com.frame.admin.ext.dao;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;
import org.beetl.sql.core.engine.PageQuery;

import  com.frame.admin.ext.entity.*;

/**
 * 系统文档 Dao
 */
@SqlResource("adminExt.cDoc")
public interface CDocDao extends BaseMapper<CDoc>{
    public PageQuery<CDoc> queryByCondition(PageQuery query);
    public void batchDelCDocByIds( List<Long> ids);
}