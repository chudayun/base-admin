package com.frame.admin.ext.service;


import com.frame.admin.ext.dao.CNoticeDao;
import com.frame.admin.ext.entity.CNotice;



import com.frame.admin.core.service.CoreBaseService;
import com.frame.admin.core.util.PlatformException;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;






/**
 * 系统通知 Service
 */

@Service
@Transactional
public class CNoticeService extends CoreBaseService<CNotice>{

    @Autowired private CNoticeDao cNoticeDao;

    public PageQuery<CNotice>queryByCondition(PageQuery query){
        PageQuery ret =  cNoticeDao.queryByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

    public void batchDelCNotice(List<Long> ids){
        try {
            cNoticeDao.batchDelCNoticeByIds(ids);
        } catch (Exception e) {
            throw new PlatformException("批量删除系统通知失败", e);
        }
    }

    public PageQuery<CNotice>queryReceiveListByCondition(PageQuery query){
        PageQuery ret =  cNoticeDao.queryReceiveListByCondition(query);
        queryListAfter(ret.getList());
        return ret;
    }

}