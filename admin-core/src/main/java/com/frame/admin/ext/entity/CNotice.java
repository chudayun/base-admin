package com.frame.admin.ext.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.beetl.sql.core.annotatoin.AssignID;
import org.beetl.sql.core.annotatoin.SeqID;
import org.beetl.sql.core.annotatoin.UpdateIgnore;

import com.frame.admin.core.util.ValidateConfig;

import com.frame.admin.core.entity.BaseEntity;

import org.beetl.sql.core.annotatoin.InsertIgnore;
import org.beetl.sql.core.annotatoin.LogicDelete;


/* 
* 系统通知公告
* gen by Spring Boot2 Admin 2020-04-24
*/
public class CNotice extends BaseEntity{

    @NotNull(message = "ID不能为空", groups =ValidateConfig.UPDATE.class)
    @SeqID(name = ORACLE_CORE_SEQ_NAME)
    @AssignID("simple")

    private Long id ;
	

    private String title ;
	

    private String content ;
	

    private String url ;
	

    @UpdateIgnore
    private Long createId ;
	
    @UpdateIgnore
    private String createName ;
	

    private String author ;
	
    @UpdateIgnore
    private Date createTime ;
	
	/*逻辑删除标志*/
	@InsertIgnore
	@LogicDelete(value = 1)

    private Integer delFlag ;
	
    public CNotice(){
    }

    public Long getId(){
        return  id;
    }
    public void setId(Long id){
        this.id = id;
    }

    public String getTitle(){
        return  title;
    }
    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return  content;
    }
    public void setContent(String content){
        this.content = content;
    }

    public String getUrl(){
        return  url;
    }
    public void setUrl(String url){
        this.url = url;
    }

    public Long getCreateId(){
        return  createId;
    }
    public void setCreateId(Long createId){
        this.createId = createId;
    }

    public String getCreateName(){
        return  createName;
    }
    public void setCreateName(String createName){
        this.createName = createName;
    }

    public String getAuthor(){
        return  author;
    }
    public void setAuthor(String author){
        this.author = author;
    }

    public Date getCreateTime(){
        return  createTime;
    }
    public void setCreateTime(Date createTime){
        this.createTime = createTime;
    }

    public Integer getDelFlag(){
        return  delFlag;
    }
    public void setDelFlag(Integer delFlag){
        this.delFlag = delFlag;
    }


}
