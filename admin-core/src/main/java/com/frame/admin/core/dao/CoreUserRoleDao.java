package com.frame.admin.core.dao;

import com.frame.admin.core.entity.CoreUserRole;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;

@SqlResource("core.coreUserRole")
public interface CoreUserRoleDao extends BaseMapper<CoreUserRole> {
    public void deleteByUserId( CoreUserRole coreUserRole);
}
