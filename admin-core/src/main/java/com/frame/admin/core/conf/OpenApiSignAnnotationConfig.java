package com.frame.admin.core.conf;

import cn.hutool.core.codec.Base64;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.frame.admin.core.annotation.OpenApiSign;
import com.frame.admin.core.entity.CoreAudit;
import com.frame.admin.core.entity.CoreFunction;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.entity.OpenParam;
import com.frame.admin.core.service.CoreAuditService;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.util.FunctionLocal;
import com.frame.admin.core.util.HttpRequestLocal;
import com.frame.admin.core.util.PlatformException;
import com.frame.admin.ext.entity.COpenApp;
import com.frame.admin.ext.service.COpenAppService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.Date;

@Aspect
@Component
public class OpenApiSignAnnotationConfig {
	@Autowired
	CorePlatformService platformService;
	@Autowired
	CoreAuditService sysAuditService;
	@Autowired
	HttpRequestLocal httpRequestLocal;

	@Autowired
	COpenAppService cOpenAppService;
	@Autowired
	Environment env;

	ObjectMapper jsonMapper = new ObjectMapper();
	private final Log log = LogFactory.getLog(this.getClass());

	@org.aspectj.lang.annotation.Around("within(@org.springframework.stereotype.Controller *) && @annotation(openApiSign)")
	public Object functionAccessCheck(final ProceedingJoinPoint pjp, OpenApiSign openApiSign) throws Throwable {
		// debug

		String funCode = null;
		CoreUser user = null;
		Method m = null;
		try {
			//对api 的身份做校验， 应用id ，密钥 等信息
			// appid

			// publicKey

			String __auth = httpRequestLocal.getRequestParam("api.auth");
			if(StringUtils.isEmpty(__auth)) {
				__auth = httpRequestLocal.getRequestHeaderValue("api.auth");
			}
			System.out.println("log:" + __auth);
			if(StringUtils.isNotEmpty(__auth)) {
				//解密 auth信息

				String paramStr =  Base64.decodeStr(__auth);
				paramStr = URLDecoder.decode(paramStr);
				OpenParam openParam = new OpenParam(paramStr);

				System.out.println("log2:" + paramStr);
				COpenApp app = cOpenAppService.getByAppId(openParam.getAppid());


				if(OpenParam.verifyApi(openParam,app)) {
					//记录日志
					System.out.println("log3:" + paramStr);
					httpRequestLocal.setRequestParam("tenantId",app.getTenantId());
					//httpRequestLocal.setSessionValue("__openApiApp", app);
				}else {
					throw new PlatformException("校验码错误");
				}
			}else {
				throw new PlatformException("校验码错误");
			}

			System.out.println("log4:" + openApiSign);
			// sign
			// appid 和 密钥 来申请 token ，根据token 调取
			if (openApiSign != null) {
				funCode = openApiSign.value();
				//api接口权限校验
				System.out.println("api==============================================" + funCode);
//				user = platformService.getCurrentUser();
//				Long orgId = platformService.getCurrentOrgId();
//				boolean access = platformService.canAcessFunction(user.getId(), orgId, funCode);
//				if (!access) {
//					log.warn(jsonMapper.writeValueAsString(user) + "试图访问未授权功能 " + funCode);
//					throw new PlatformException("试图访问未授权功能");
//				}


				FunctionLocal.set(funCode);
			}

			Object o = pjp.proceed();
			if (openApiSign != null) {
				MethodSignature ms = (MethodSignature)pjp.getSignature();
				m = ms.getMethod();
				//createAudit(funCode,function.name(), user, true, "",m);
			}
			return o;

		} catch (Throwable e) {
			if (openApiSign != null) {
				//createAudit(funCode, function.name(),user, false, e.getMessage(),m);
			}
			throw e;
		}

	}

	private void createAudit(String functionCode, String functionName,CoreUser user, boolean success, String msg, Method m) {
		boolean enable = env.getProperty("audit.enable", Boolean.class, false);
		if (!enable) {
			return;
		}
		if(filter(m,functionCode)){
			return ;
		}
		
		CoreAudit audit = new CoreAudit();
		if(StringUtils.isEmpty(functionName)) {
		    CoreFunction fun = this.platformService.getFunction(functionCode);

	        if (fun == null) {
	            // 没有在数据库定义，但写在代码里了
	            log.warn(functionCode + " 未在数据库里定义");
	            functionName = "未定义";
	        } else {
	            functionName = fun.getName();
	        }
		}
		audit.setCreateTime(new Date());
		audit.setFunctionCode(functionCode);
		audit.setFunctionName(functionName);
		audit.setUserId(user.getId());
		audit.setSuccess(success ? 1 : 0);
		audit.setUserName(user.getName());
		audit.setMessage(msg);
		
		audit.setIp(httpRequestLocal.getRequestIP());
		sysAuditService.save(audit);
	}
	
	private boolean filter(Method m,String functionCode){
		if(functionCode.startsWith("audit.")){
			return true;
		}
		String uri = httpRequestLocal.getRequestURI();
		if(uri!=null&&uri.endsWith("/index/condition.json")){
			
			return true ;
		}else{
			return false;
		}
	}

}
