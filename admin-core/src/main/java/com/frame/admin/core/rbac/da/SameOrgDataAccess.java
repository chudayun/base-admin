package com.frame.admin.core.rbac.da;

import java.util.Arrays;

import com.frame.admin.core.service.CorePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.admin.core.rbac.AccessType;
import com.frame.admin.core.rbac.DataAccess;
import com.frame.admin.core.rbac.DataAccessResullt;

/**
 * 同机构
 * @author lijiazhi
 *
 */
@Component
public class SameOrgDataAccess implements DataAccess {
	
	@Autowired
    CorePlatformService platformService;

	@Override
	public DataAccessResullt getOrg(Long userId, Long orgId) {
		DataAccessResullt ret = new DataAccessResullt();
		ret.setStatus(AccessType.OnlyOrg);
		ret.setOrgIds(Arrays.asList(orgId));
		return ret;
		
	}

	@Override
	public String getName() {
		return "同部门";
	}

	@Override
	public Integer getType() {
		return 3;
	}

}
