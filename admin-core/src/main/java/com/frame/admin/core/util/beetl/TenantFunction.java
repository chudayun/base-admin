package com.frame.admin.core.util.beetl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.frame.admin.core.service.CorePlatformService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.beetl.core.Context;
import org.beetl.core.Function;
import org.beetl.sql.core.engine.SQLParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.admin.core.rbac.DataAccessFactory;

/**
 * 租户过滤条件函数 
 * @author owes
 *
 */
@Component
public class TenantFunction implements Function {
	
	Log log = LogFactory.getLog(TenantFunction.class);
	
	@Autowired
    CorePlatformService platFormService;
	@Autowired
	DataAccessFactory dataAccessFactory;
	
	private static Map defaultTargets = new HashMap();
	static{
		//数据库默认的跟组织和用户相关字段
		defaultTargets.put("tenant", "tenant_id");
	}
	

	
	public Object call(Object[] paras, Context ctx){
		//{"org":"org_id","user","user_id"}
		Map targets  = this.defaultTargets;
		//用户调用conroller 结果"user.view"
		String table_alias  = "t";
		
		if(paras.length==1){
			Object o = paras[0];
			if(o instanceof String){
				table_alias = (String)o;
			}else if(o instanceof Map){
				targets  = (Map)paras[1];
			}
		}else if(paras.length==2){
			table_alias = (String)paras[0];
			targets.put("tenant", String.valueOf(paras[1]));
		}
		
		List<Object> list = (List<Object>)ctx.getGlobal("_paras");
		
		StringBuilder sb = new StringBuilder(" and ");
		if(StringUtils.isNotEmpty(table_alias)) {
			sb.append(table_alias + "." + targets.get("tenant"));
		}else {
			sb.append(targets.get("tenant"));
		}
		
		sb.append("= ? ");
		list.add(new SQLParameter(platFormService.getCurrentUser().getTenantId()));//获取当前租户信息
//		platFormService.getCurrentUser().
		//System.out.println("================++++++++++" + sb.toString());
		return sb.toString();
		
	}
	

	
	
}
