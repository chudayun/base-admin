package com.frame.admin.core.entity;


import com.frame.admin.core.util.PlatformException;
import com.frame.admin.core.web.JsonReturnCode;


public class BaseSessionResultQuery {



    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 部门id
     */
    private Long orgId;

    /**
     * 部门名
     */
    private String orgName;

    /**
     * 租户
     */
    private Long tenantId;


    private String orgSeq;

    private Boolean isSupperAdmin;



    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Boolean getSupperAdmin() {
        return isSupperAdmin;
    }

    public void setSupperAdmin(Boolean supperAdmin) {
        isSupperAdmin = supperAdmin;
    }

    public String getOrgSeq() {
        return orgSeq;
    }

    public void setOrgSeq(String orgSeq) {
        this.orgSeq = orgSeq;
    }


    public void isSupperAdmin() {
        if(!this.getSupperAdmin()){
            throw new PlatformException(JsonReturnCode.COMMON_NOT_ADMIN.getDesc());
        }
    }
    public BaseSessionResultQuery(){


    }


}
