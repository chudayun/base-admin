package com.frame.admin.core.util;

import java.util.HashMap;
import java.util.Map;

public class UrlUtil {

	public static class UrlEntity {
		/**
		 * 基础url
		 */
		public String baseUrl;
		/**
		 * url参数
		 */
		public Map<String, String> params;
	}

	public static UrlEntity parse(String url) {
		return parse(url,true);
	}
	/**
	 * 解析url
	 *
	 * @param url
	 * @return
	 */
	public static UrlEntity parse(String url,boolean base) {
		UrlEntity entity = new UrlEntity();
		if (url == null) {
			return entity;
		}
		url = url.trim();
		if (url.equals("")) {
			return entity;
		}
		String[] urlParts = url.split("\\?");
		String paramStr = "";
		if(base) {
			entity.baseUrl = urlParts[0];
			//没有参数
			if (urlParts.length == 1) {
				return entity;
			}
			paramStr = urlParts[1];
		}else {
			entity.baseUrl = "";
			paramStr = urlParts[0];
			
		}
		//有参数
		String[] params = paramStr.split("&");
		entity.params = new HashMap<>();
		for (String param : params) {
			String[] keyValue = param.split("=");
			entity.params.put(keyValue[0], keyValue[1]);
		}

		return entity;
	}
	

	/**
	 * 测试
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		UrlEntity entity = parse(null);
		System.out.println(entity.baseUrl + "\n" + entity.params);
		entity = parse("http://www.123.com");
		System.out.println(entity.baseUrl + "\n" + entity.params);
		entity = parse("http://www.123.com?id=1");
		System.out.println(entity.baseUrl + "\n" + entity.params);
		entity = parse("http://www.123.com?id=1&name=小明");
		System.out.println(entity.baseUrl + "\n" + entity.params);
	}
}
