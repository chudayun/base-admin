package com.frame.admin.core.dao;

import com.frame.admin.core.entity.CoreFunction;
import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.annotatoin.Sql;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

@SqlResource("core.coreFunction")
public interface CoreFunctionDao extends BaseMapper<CoreFunction> {
    @Sql("select * from core_function where code = ?")
    CoreFunction getFunctionByCode(@Param(value = "code") String code);

    @Sql("select distinct t1.* from core_role_function t left join core_function t1 on t.function_id = t1.id where t.role_id in (select role_id from core_user_role where org_id = ? and user_id = ?)")
    List<CoreFunction> getFunctionToRole(@Param(value = "orgId") Long orgId,@Param(value = "userId") Long userId);
}
