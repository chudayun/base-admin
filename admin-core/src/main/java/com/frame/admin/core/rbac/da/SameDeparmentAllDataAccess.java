package com.frame.admin.core.rbac.da;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.frame.admin.core.rbac.tree.OrgItem;
import com.frame.admin.core.service.CorePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.admin.core.rbac.AccessType;
import com.frame.admin.core.rbac.DataAccess;
import com.frame.admin.core.rbac.DataAccessResullt;

/**
 * 同部门下所有
 * @author lijiazhi
 *
 */
@Component
public class SameDeparmentAllDataAccess implements DataAccess {
	
	@Autowired
    CorePlatformService platformService;

	@Override
	public DataAccessResullt getOrg(Long userId, Long orgId) {
		DataAccessResullt ret = new DataAccessResullt();
		ret.setStatus(AccessType.OnlyOrg);
		OrgItem item = platformService.getCurrentOrgItem();
		OrgItem dept = item.findParentOrgItem(DefaultDataAccessFactory.DEPARTMENT_TYPE);
		if(dept==null){
			ret.setOrgIds(Collections.EMPTY_LIST);
			ret.setOrgId(orgId);
			ret.setUserId(userId);
			return ret;
		}
		List<OrgItem> all = dept.findAllChildOrgItem(null);
		all.add(dept);
		List<Long> list = new ArrayList<Long>(all.size());
		for(OrgItem org:all){
			list.add(org.getId());
		}


		ret.setOrgIds(list);
		ret.setOrgId(orgId);
		ret.setUserId(userId);
		return ret;
		
	}

	@Override
	public String getName() {
		return "部门及子部门";
	}

	@Override
	public Integer getType() {
		return 4;
	}

}
