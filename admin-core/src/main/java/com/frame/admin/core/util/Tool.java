package com.frame.admin.core.util;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * 常用工具类方法
 * 
 * @author lijiazhi
 *
 */
public class Tool {
	static final String DATE_FORAMT = "yyyy-MM-dd";
	static final String DATETIME_FORAMT = "yyyy-MM-dd HH:mm:ss";
	
	static final String rangeSplit = "至";
	
	public static Date[] parseDataRange(String str) {
		//查询范围
		String[] arrays = str.split(rangeSplit);
		Date min = parseDate(arrays[0]);
		Date max = parseDate(arrays[1]);
	
		return new Date[] { min,max };
	}
	
	public static Long[] parseDataLongRange(String str) {
		//查询范围,时间戳
		String[] arrays = str.split(rangeSplit);
		Date min = parseDate(arrays[0]);
		Date max = parseDate(arrays[1]);
	
		return new Long[] { min.getTime(),max.getTime() };
	}

	public static Date[] parseDataTimeRange(String str) {
		//查询范围
		String[] arrays = str.split(rangeSplit);
		Date min = parseDateWithPattern(arrays[0], DATETIME_FORAMT);
		Date max = parseDateWithPattern(arrays[1], DATETIME_FORAMT);

		return new Date[] { min,max };
	}
	
	public static Long[] parseDataTimeLongRange(String str) {
		//查询范围,时间戳
		String[] arrays = str.split(rangeSplit);
		Date min = parseDateWithPattern(arrays[0], DATETIME_FORAMT);
		Date max = parseDateWithPattern(arrays[1], DATETIME_FORAMT);
	
		return new Long[] { min.getTime(),max.getTime() };
	}

	public static Date parseDate(String str) {
	    return parseDateWithPattern(str, DATE_FORAMT);
	}

	public static Date parseDateWithPattern(String str, String pattern) {
		try {
			return DateUtils.parseDate(str.trim(), pattern);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}
}
