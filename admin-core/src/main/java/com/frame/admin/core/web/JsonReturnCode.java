package com.frame.admin.core.web;

/**
 * 描述: json格式数据返回码
 *<ul>
 *      <li>100 : 用户未登录 </li>
 *      <li>200 : 成功 </li>
 *      <li>300 : 失败 </li>
 * </ul>
 * @author : Administrator
 */
public enum JsonReturnCode {

    NOT_LOGIN("401","未登录"),
    SUCCESS ("200","成功"),
    FAIL ("500","内部失败"),
	ACCESS_ERROR ("403","禁止访问"),
	NOT_FOUND ("404","页面未发现"),
    NOT_TOKEN("401","token验证失败,请传入正确的token！"),
    COMMON_TENANT_SAVE_FAIL("0004","保存失败"),
    COMMON_NOT_ADMIN("0000","账号不是管理员账号,无效的操作！");

    private String code;
    private String desc;

    JsonReturnCode(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
