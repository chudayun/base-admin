package com.frame.admin.core.conf;

import com.frame.admin.ext.service.LocalStaticFileService;
import com.frame.admin.ext.service.StaticFileService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import java.io.File;

@Configuration
@ConditionalOnMissingBean(StaticFileService.class)
public class StaticFileSystemConfig {
	@Autowired
	Environment env;
	@Bean
	public StaticFileService getStaticFileService(ApplicationContext ctx) {
		String root = env.getProperty("localStaticFile.root");
		if(StringUtils.isEmpty(root)) {
			String userDir = System.getProperty("user.dir");
			root = userDir+File.separator+"filesystem";
		}
		File f = new File(root);
		if(!f.exists()) {
			f.mkdirs();
		}
		return new LocalStaticFileService(ctx,root);
	}
}
