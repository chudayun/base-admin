package com.frame.admin.core.dao;

import com.frame.admin.core.entity.CoreAudit;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;

/*
* 
* gen by starter mapper 2017-08-01
*/
@SqlResource("core.coreAudit")
public interface CoreAuditDao extends BaseMapper<CoreAudit> {
	
}