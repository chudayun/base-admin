package com.frame.admin.core.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 用来标注功能id
 * <pre>
 * &#064;Function("user.add")
 * public String addUser(){
 * }
 * </pre>
 *此功能用于校验SessionKey
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface SessionKeyCheck {
	public String value() default "sessionkey";
	public String name() default "";
}
