package com.frame.admin.core.dao;

import com.frame.admin.core.entity.CoreOrg;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

@SqlResource("core.coreOrg")
public interface CoreOrgDao extends BaseMapper<CoreOrg> {

    List<CoreOrg> queryOrgByUser(Long userId);

 
    List<String> queryAllOrgCode(List<Long> orgIds);
    

    CoreOrg getRoot(String isSuperAdmin,Long tenantId);

    CoreOrg getTenantOrgRoot(Long tenantId);

}
