package com.frame.admin.core.conf;

import cn.hutool.core.codec.Base64;
import cn.hutool.crypto.SecureUtil;
import com.frame.admin.core.util.UrlUtil;

import java.net.URLDecoder;
import java.util.Date;

public class Demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//登录签名计算生成
		String appid = "11223344";//录音转储平台分配， 业务系统保存到配置中。
		String secret = "1cd2c452c767e6d04bb89eb2e4a7b3c7"; //录音转储平台分配， 业务系统保存到配置中。
		String timestamp = String.valueOf(new Date().getTime()/1000);//时间戳
		String usercode = "admin";//登录账号

		System.out.println("=====login sign=========================");
		
		// appid + scret + 时间戳 + usercode md5 两次，作为 sgin 时间戳 2分钟之内有效
		String sign = SecureUtil.md5(appid + secret + timestamp + usercode);
		sign = SecureUtil.md5(sign);
				
		System.out.println("login sign=" + sign);
		
		String paramStr = "";
		paramStr += "appid=" + appid;
		paramStr += "&sign=" + sign;
		paramStr += "&timestamp=" + timestamp;
		paramStr += "&usercode=" + usercode;
		
		System.out.println("login paramStr=" + paramStr);
		
		String encode = Base64.encode(paramStr);
		System.out.println("login paramStr Base64=" + encode);


		System.out.println("get 方式传参：api.auth=" + encode);
		System.out.println("post 方式传参，在header中增加 api.auth=" + encode);

		// 签名校验
		String decodeStr = Base64.decodeStr(encode);

		System.out.println("签名校验：");
//		System.out.println(URLDecoder.decode(decodeStr));
		System.out.println("参数解析");
		System.out.println(UrlUtil.parse(decodeStr,false).params);
		
		
		System.out.println("=====api sign=========================");
				// appid + scret + 时间戳 + usercode md5 两次，作为 sgin 时间戳 2分钟之内有效
		String apisign = SecureUtil.md5(appid + secret + timestamp);
		apisign = SecureUtil.md5(apisign);
				
		System.out.println("apisign=" + apisign);
		
		String apiparamStr = "";
		apiparamStr += "appid=" + appid;
		apiparamStr += "&sign=" + apisign;
		apiparamStr += "&timestamp=" + timestamp;
		
		System.out.println("paramStr=" + apiparamStr);
		
		String apiencode = Base64.encode(apiparamStr);
		System.out.println("paramStr Base64=" + apiencode);
		// 还原为a
		String apidecodeStr = Base64.decodeStr(apiencode);
		System.out.println(apidecodeStr);
		
		System.out.println(URLDecoder.decode(apidecodeStr));
		
		System.out.println(UrlUtil.parse(apidecodeStr,false).params);
		
	}

}
