package com.frame.admin.core.service;

import com.frame.admin.core.conf.PasswordConfig;
import com.frame.admin.ext.pojo.UserLogin;
import com.frame.admin.core.dao.CoreOrgDao;
import com.frame.admin.core.dao.CoreUserDao;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.rbac.UserLoginInfo;
import com.frame.admin.core.util.PlatformException;
import com.frame.admin.core.util.enums.DelFlagEnum;
import com.frame.admin.core.util.enums.GeneralStateEnum;
import org.beetl.sql.core.SQLManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CoreUserService {
	@Autowired
	CoreUserDao userDao ;
	
	@Autowired
	CoreOrgDao orgDao;
	
	@Autowired
    PasswordConfig.PasswordEncryptService passwordEncryptService;
	
	@Autowired SQLManager sqlManager;
	
	public UserLoginInfo login(String userName,String password){
		CoreUser query = new CoreUser();
		query.setCode(userName);
		query.setPassword(passwordEncryptService.password(password));
		query.setState(GeneralStateEnum.ENABLE.getValue());
		CoreUser user =userDao.createLambdaQuery().andEq(CoreUser::getCode,userName).
		    andEq(CoreUser::getPassword, passwordEncryptService.password(password)).single();
//		if(user==null) {
//		    throw new PlatformException("用户"+userName+"不存在或者密码错误");
//		}
//		if(!user.getState().equals(GeneralStateEnum.ENABLE.getValue())){
//		    throw new PlatformException("用户"+userName+"已经失效");
//		}
//		if(user.getDelFlag()==DelFlagEnum.DELETED.getValue()) {
//		    throw new PlatformException("用户"+userName+"已经删除");
//		}
		
		if(user==null) {
		    throw new PlatformException("codePassword_expiration");
		}
		if(!user.getState().equals(GeneralStateEnum.ENABLE.getValue())){
		    throw new PlatformException("userInvalid_expiration");
		}
		if(user.getDelFlag()==DelFlagEnum.DELETED.getValue()) {
		    throw new PlatformException("userDeleted_expiration");
		}
		
		
		List<CoreOrg>  orgs = getUserOrg(user.getId(),user.getOrgId());
		UserLoginInfo loginInfo = new UserLoginInfo();
		loginInfo.setOrgs(orgs);
		loginInfo.setUser(user);
		return loginInfo;
		
	}
	
	public UserLoginInfo login(UserLogin userlogin){
		CoreUser query = new CoreUser();
		query.setCode(userlogin.getCode());
		query.setPassword(passwordEncryptService.password(userlogin.getPassword()));
		query.setState(GeneralStateEnum.ENABLE.getValue());

		if(null==userlogin.getTenantId()) {
			userlogin.setTenantId(String.valueOf(CorePlatformService.TENANT_DEFAULT_ID));
		}
		//System.out.println("========================================getTenantId=" + userlogin.getTenantId());
		CoreUser user =userDao.createLambdaQuery().andEq(CoreUser::getCode, userlogin.getCode()).
		    andEq(CoreUser::getPassword, passwordEncryptService.password(userlogin.getPassword())).andEq(CoreUser::getTenantId, userlogin.getTenantId()).single();
//		if(user==null) {
//		    throw new PlatformException("用户"+userName+"不存在或者密码错误");
//		}
//		if(!user.getState().equals(GeneralStateEnum.ENABLE.getValue())){
//		    throw new PlatformException("用户"+userName+"已经失效");
//		}
//		if(user.getDelFlag()==DelFlagEnum.DELETED.getValue()) {
//		    throw new PlatformException("用户"+userName+"已经删除");
//		}
		
		if(user==null) {
		    throw new PlatformException("codePassword_expiration");
		}
		if(!user.getState().equals(GeneralStateEnum.ENABLE.getValue())){
		    throw new PlatformException("userInvalid_expiration");
		}
		if(user.getDelFlag()==DelFlagEnum.DELETED.getValue()) {
		    throw new PlatformException("userDeleted_expiration");
		}
		
		
		List<CoreOrg>  orgs = getUserOrg(user.getId(),user.getOrgId());
		UserLoginInfo loginInfo = new UserLoginInfo();
		loginInfo.setOrgs(orgs);
		loginInfo.setUser(user);
		return loginInfo;
		
	}
	
	public  List<CoreOrg> getUserOrg(long userId,long orgId){
		List<CoreOrg> orgs =  orgDao.queryOrgByUser(userId);
		if(orgs.isEmpty()){
			//没有赋值任何角色，默认给一个所在部门
			CoreOrg userOrg = orgDao.unique(orgId);
			orgs.add(userOrg);
		}
		return orgs;
	}
	
	
	
	public List<CoreUser> getAllUsersByRole(String role){
		return userDao.getUserByRole(role);
	}
	
	public CoreUser getUserByCode(String userName){
		CoreUser user = new CoreUser();
		user.setCode(userName);
		return userDao.templateOne(user);
	}
	
	public void update(CoreUser user){
		userDao.updateById(user);
	}
	
	public CoreOrg getOrgById(Long orgId){
		return orgDao.unique(orgId);
	}
	
	public CoreUser getUserById(Long userId){
		return userDao.unique(userId);
	}
	
	public List<String> getOrgCode(List<Long> orgIds){
		return orgDao.queryAllOrgCode(orgIds);
	}
}
