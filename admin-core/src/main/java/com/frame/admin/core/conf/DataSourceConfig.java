package com.frame.admin.core.conf;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.SQLException;

@Configuration
public class DataSourceConfig {

//	@Bean(name = "baseDataSource")
//	public DataSource datasource(Environment env) {
//		HikariDataSource ds = new HikariDataSource();
//		ds.setJdbcUrl(env.getProperty("spring.datasource.baseDataSource.url"));
//		ds.setUsername(env.getProperty("spring.datasource.baseDataSource.username"));
//		ds.setPassword(env.getProperty("spring.datasource.baseDataSource.password"));
//		ds.setDriverClassName(env.getProperty("spring.datasource.baseDataSource.driver-class-name"));
//		return ds;
//	}

	@Bean(name = "baseDataSource")
	public DataSource datasource(Environment env) {
		DruidDataSource druidDataSource=new DruidDataSource();
		druidDataSource.setName("baseDataSource");
		druidDataSource.setUrl(env.getProperty("spring.datasource.url"));
		druidDataSource.setUsername(env.getProperty("spring.datasource.username"));
		druidDataSource.setPassword(env.getProperty("spring.datasource.password"));
		druidDataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));

		//druidDataSource配置
		druidDataSource.setMaxActive(env.getProperty("spring.datasource.maxActive",Integer.class));
		druidDataSource.setInitialSize(env.getProperty("spring.datasource.initialSize",Integer.class));
		druidDataSource.setMaxWait(env.getProperty("spring.datasource.maxWait",Integer.class));
		druidDataSource.setMinIdle(env.getProperty("spring.datasource.minIdle",Integer.class));
		druidDataSource.setTimeBetweenEvictionRunsMillis(env.getProperty("spring.datasource.timeBetweenEvictionRunsMillis",Integer.class));
		druidDataSource.setMinEvictableIdleTimeMillis(env.getProperty("spring.datasource.minEvictableIdleTimeMillis",Integer.class));
		druidDataSource.setValidationQuery(env.getProperty("spring.datasource.validationQuery"));
		druidDataSource.setTestWhileIdle(env.getProperty("spring.datasource.testWhileIdle",Boolean.class));
		druidDataSource.setTestOnBorrow(env.getProperty("spring.datasource.testOnBorrow",Boolean.class));
		druidDataSource.setTestOnReturn(env.getProperty("spring.datasource.testOnReturn",Boolean.class));

		try {
			druidDataSource.setFilters(env.getProperty("spring.datasource.filters"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return druidDataSource;
	}
}



