package com.frame.admin.core.service;

import com.frame.admin.core.dao.CoreRoleDao;
import com.frame.admin.core.entity.BaseSessionResultQuery;
import com.frame.admin.core.entity.CoreRole;
import com.frame.admin.core.web.query.RoleQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 描述: 字典 service，包含常规字典和级联字典的操作。
 * @author : xiandafu
 */
@Service
@Transactional
public class CoreRoleService extends CoreBaseService<CoreRole> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoreRoleService.class);

    @Autowired
    private CoreRoleDao roleDao;

    @Autowired
    CorePlatformService platformService;
    
    public List<CoreRole> getAllRoles(String type){
    	CoreRole template = new CoreRole();
    	template.setType(type);
        BaseSessionResultQuery q = platformService.getBaseSession();


            template.setTenantId(q.getTenantId());


    	return roleDao.template(template);
    }

    public List<CoreRole> queryRoleByCondition(String type,String tenantAdminRolePrefix){

        BaseSessionResultQuery q = platformService.getBaseSession();
        RoleQuery query  = new RoleQuery();

        query.setTenantId(q.getTenantId());
        query.setType(type);
        query.setTenantAdminRolePrefix(tenantAdminRolePrefix);
        return roleDao.queryRoleByCondition(query);
    }
   

    
    
  
}
