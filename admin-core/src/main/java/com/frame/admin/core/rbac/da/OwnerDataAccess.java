package com.frame.admin.core.rbac.da;

import java.util.Arrays;

import com.frame.admin.core.service.CorePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.admin.core.rbac.AccessType;
import com.frame.admin.core.rbac.DataAccess;
import com.frame.admin.core.rbac.DataAccessResullt;

/**
 * 只查看自己
 * @author lijiazhi
 *
 */
@Component
public class OwnerDataAccess implements DataAccess {
	
	@Autowired
    CorePlatformService platformService;

	@Override
	public DataAccessResullt getOrg(Long userId, Long orgId) {
		DataAccessResullt ret = new DataAccessResullt();
		ret.setStatus(AccessType.OnlyUser);
		ret.setUserIds(Arrays.asList(userId));
		return ret;
		
	}

	@Override
	public String getName() {
		return "本人创建";
	}

	@Override
	public Integer getType() {
		return 1;
	}

}
