package com.frame.admin.core.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 用来标注功能id
 * <pre>
 * &#064;Function("user.add")
 * public String addUser(){
 * }
 * </pre>
 * 
 * 只有校验码验证通过才能操作，否则，权限不足
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface OpenApiSign {
	public String value() default "checkSign";
	public String name() default "";
}
