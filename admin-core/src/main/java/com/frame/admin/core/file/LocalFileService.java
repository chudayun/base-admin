package com.frame.admin.core.file;

import com.frame.admin.core.dao.CoreFileDao;
import com.frame.admin.core.entity.CoreFile;
import com.frame.admin.core.util.DateUtil;
import com.frame.admin.core.util.FileUtil;
import com.frame.admin.core.util.PlatformException;
import com.frame.admin.core.util.UUIDUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
/**
 * 一个本地文件系统，管理临时文件和用户文件
 * @author xiandafu
 *
 */
public class LocalFileService   implements FileService {
    Log log = LogFactory.getLog(this.getClass());
    DBIndexHelper dbHelper =  null;
	String root = null;

	public LocalFileService(ApplicationContext ctx,String root) {
		this.root = root;
		new File(root,"temp").mkdir();
		dbHelper = new DBIndexHelper(ctx.getBean(CoreFileDao.class));
	}

	@Override
	public FileItem loadFileItemByPath(String path) {
	    CoreFile coreFile = dbHelper.getFileItemByPath(path);
	    if(coreFile!=null) {
	        return getFileItem(coreFile);
	    }
	    LocalFileItem item = new LocalFileItem(root);
		item.setPath(path);
		item.setName(parseTempFileName(path));
		item.setTemp(true);
		return item;
	}

	@Override
	public FileItem createFileTemp(String name) {
		FileItem item = new LocalFileItem(root);
		String fileName = "temp"+File.separator+name + "." + this.suffix();
		item.setPath(fileName);
		item.setName(name);
		item.setTemp(true);
		return item;
	}

	@Override
    public FileItem createFileItem(String name, String bizType, String bizId, Long userId, Long orgId, String fileBatchId,List<FileTag> tags) {
	    CoreFile coreFile = new CoreFile();
	    coreFile.setBizId(bizId);
	    coreFile.setBizType(bizType);
	    coreFile.setUserId(userId);
	    coreFile.setOrgId(orgId);
	    coreFile.setName(name);
	    coreFile.setCreateTime(new Date());
	    coreFile.setFileBatchId(fileBatchId);
	    String dir = DateUtil.now("yyyyMMdd");
	    File dirFile = new File(root + File.separator +  bizType + File.separator  + dir);
	    if(!dirFile.exists()) {
	        dirFile.mkdirs();
	    }
	    String fileName = name+"."+ UUIDUtil.uuid();
	    String path =  dir+File.separator+fileName;
	    coreFile.setPath(path);
	    //目前忽略tags
	    dbHelper.createFileItem(coreFile,tags);
	    return this.getFileItem(coreFile);
	    
    }
	
	@Override
    public FileItem createFileItem(CoreFile coreFile,List<FileTag> tags) {
	    coreFile.setCreateTime(new Date());

	    String biz_type = coreFile.getBizType();
	    if(null == biz_type || "".equalsIgnoreCase(biz_type)) {
	    	biz_type = "default";
	    }
	    
	    String dir = DateUtil.now("yyyyMMdd");
	    File dirFile = new File(root + File.separator + biz_type  + File.separator + dir);
	    if(!dirFile.exists()) {
	        dirFile.mkdirs();
	    }

		log.info("dirFile.getAbsoluteFile()=======" + dirFile.getAbsoluteFile());
		if("false".equals(coreFile.get("pathuuid"))){
			String fileName = coreFile.getName();
			String path =  biz_type +File.separator+ dir+File.separator+fileName;

			log.info("pathuuid=false.path()=======" + path);

			coreFile.setPath(path);
		}else{
			String fileName = coreFile.getName()+"."+UUIDUtil.uuid();
			String path =  biz_type +File.separator+ dir+File.separator+fileName;
			coreFile.setPath(path);
			log.info("pathuuid=true.path()=======" + path);
		}
	    //目前忽略tags
	    dbHelper.createFileItem(coreFile,tags);
	    return this.getFileItem(coreFile);
	    
    }

	public List<String> createFileZip(CoreFile coreFile,MultipartFile file){


		String dir = DateUtil.now("yyyyMMdd");
		File dirFile = new File(root + File.separator + coreFile.getBizType()  + File.separator + dir);
		if(!dirFile.exists()) {
			dirFile.mkdirs();
		}

		//获得原始文件名
		String oriName = coreFile.getName();
		String path =  coreFile.getBizType() +File.separator+ dir+File.separator+oriName;
		coreFile.setPath(path);


			//判断是不是zip文件
			if(oriName.matches(".*.zip")) {
				List<String> zipFileList = new ArrayList<>();
				FileItem fileItem = this.getFileItem(coreFile);
				String savePath = dirFile.getPath();
				try {
					//文件保存到目录然后解压↓
					OutputStream os = fileItem.openOutpuStream();
					FileUtil.copy(file.getInputStream(), os);

					//解压文件到指定目录
					zipFileList = FileUnZip.zipToFile(root + File.separator + path, savePath);
					new File(savePath,oriName).delete();
				} catch (Exception e){
					e.printStackTrace();
					new File(savePath,oriName).delete();
				}
				//fileItem.delete();
				//保存路径文件名
				return zipFileList;

			}else {
				//不是zip则返回false
				return null;
			}
	}

	private String suffix() {
		// TODO,改成唯一算法
		return DateUtil.now("yyyyMMddhhmm")+ "-" + UUIDUtil.uuid();
	}
	
	private String parseTempFileName(String path) {
		File file = new File(path);
		String name =  file.getName();
		//去掉最后的临时标记
		int index = name.lastIndexOf(".");
		return name.substring(0, index);
	}
	
	protected  FileItem getFileItem(CoreFile file) {
	    LocalFileItem item = new LocalFileItem(root);
	    item.setName(file.getName());
	    item.setPath(file.getPath());
	    item.setBizId(file.getBizId());
	    item.setBizType(file.getBizType());
	    item.setId(file.getId());
	    item.setOrgId(file.getOrgId());
	    item.setId(file.getId());
	    return item;
	 }
	
	protected  List<FileItem> getFileItem(List<CoreFile> files) {
	    List<FileItem> items = new ArrayList<>(files.size());
	    for(CoreFile file:files) {
	        items.add(this.getFileItem(file));
	    }
	    return items;
       
     }

    

    @Override
    public FileItem getFileItemById(Long id) {
        return this.getFileItem(dbHelper.getFileItemById(id));
    }

    @Override
    public List<FileItem> queryByUserId(Long userId, List<FileTag> tags) {
        return this.getFileItem(dbHelper.queryByUserId(userId, tags));
    }

    @Override
    public List<FileItem> queryByBiz(String bizType, String bizId) {
        return this.getFileItem(dbHelper.queryByBiz(bizType, bizId));
    }

    @Override
    public List<FileItem> queryByBatchId(String fileBatchId) {
        return this.getFileItem(dbHelper.queryByBatchId(fileBatchId));
    }

    @Override
    public void removeFile(Long id, String fileBatchId) {
        CoreFile file = dbHelper.getFileItemById(id);
        if(!file.getFileBatchId().equals(fileBatchId)){
            return ;
        }
        
        FileItem item = this.getFileItem(file);
		dbHelper.fileDao.deleteById(id);
        boolean success = item.delete();
        if(!success) {
            log.warn("删除文件失败 "+file.getName()+ ",id="+file.getId()+" path="+file.getPath());
            throw new PlatformException("删除文件失败 "+file.getName());
        }

        return ;
      
        
    }

    @Override
    public void updateFile(String fileBatchId, String bizType, String bizId) {
       dbHelper.fileDao.updateBatchIdInfo(bizType, bizId, fileBatchId);
    }


	@Override
	public void updateIdFile(String id, String bizType, String bizId) {
		dbHelper.fileDao.updateIdInfo(bizType, bizId, id);
	}

    @Override
    public FileItem getFileItemById(Long id, String fileBatchId) {
        CoreFile file = dbHelper.getFileItemById(id);
        if(!file.getFileBatchId().equals(fileBatchId)){
            return  null;
        }
        return this.getFileItem(file);
    }
	
	
	
	

}
