package com.frame.admin.core.gen;

import com.frame.admin.core.gen.model.Entity;

public interface AutoGen {
	public void make(Target target, Entity entity);
	public String getName();
}
