package com.frame.admin.core.rbac.da;

import com.frame.admin.core.service.CorePlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.frame.admin.core.rbac.AccessType;
import com.frame.admin.core.rbac.DataAccess;
import com.frame.admin.core.rbac.DataAccessResullt;

/**
 * 所有机构
 * @author lijiazhi
 *
 */
@Component
public class AllGroupAccess implements DataAccess {
	
	@Autowired
    CorePlatformService platformService;

	@Override
	public DataAccessResullt getOrg(Long userId, Long orgId) {
	
		
		DataAccessResullt ret = new DataAccessResullt();
		ret.setStatus(AccessType.AllOrg);
		return ret;
		
	}

	@Override
	public String getName() {
		return "所有部门";
	}

	@Override
	public Integer getType() {
		return 5;
	}

}
