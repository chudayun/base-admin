package com.frame.admin.core.web.query;

/**
 * 描述:  角色查询条件
 *
 */
public class RoleQuery extends PageParam {

    private Long tenantId;

    private String tenantAdminRolePrefix;

    private String type;


    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public String getTenantAdminRolePrefix() {
        return tenantAdminRolePrefix;
    }

    public void setTenantAdminRolePrefix(String tenantAdminRolePrefix) {
        this.tenantAdminRolePrefix = tenantAdminRolePrefix;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
