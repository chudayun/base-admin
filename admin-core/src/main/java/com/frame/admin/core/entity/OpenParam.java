package com.frame.admin.core.entity;

import cn.hutool.crypto.SecureUtil;
import com.frame.admin.core.util.UrlUtil;
import com.frame.admin.ext.entity.COpenApp;



import java.util.Map;

public class OpenParam {

    private String appid;
    private String secret;
    private String sign;
    private String timestamp;
    private String usercode;
    private Map<String, String> params;

    public OpenParam() {

    }

    public OpenParam(String paramStr) {
        this.params = UrlUtil.parse(paramStr, false).params;
        this.appid = this.params.getOrDefault("appid", "");
        this.sign = this.params.getOrDefault("sign", "");
        this.timestamp = this.params.getOrDefault("timestamp", "");
        this.usercode = this.params.getOrDefault("usercode", "");
    }


    public OpenParam(Map<String, String> params) {
        this.appid = params.getOrDefault("appid", "");
        this.sign = params.getOrDefault("sign", "");
        this.timestamp = params.getOrDefault("timestamp", "");
        this.usercode = params.getOrDefault("usercode", "");
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsercode() {
        return usercode;
    }

    public void setUsercode(String usercode) {
        this.usercode = usercode;
    }


    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public static boolean verifyLogin(OpenParam param, COpenApp app) {
        if (null == app) {
            return false;
        }
        //校验签名
        // appid + scret + 时间戳 + usercode md5 两次，作为 sgin 时间戳 2分钟之内有效
        String sign = SecureUtil.md5(param.getAppid() + app.getAppSecret() + param.getTimestamp() + param.getUsercode());
        sign = SecureUtil.md5(sign);
        if (sign.equalsIgnoreCase(param.getSign())) {
            return true;
        }
        return false;
    }

    public static boolean verifyApi(OpenParam param, COpenApp app) {
        //校验签名
        // appid + scret + 时间戳 + md5 两次，作为 sgin  时间戳 2分钟之内
        if (null == app) {
            return false;
        }
        String sign = SecureUtil.md5(param.getAppid() + app.getAppSecret() + param.getTimestamp());
        sign = SecureUtil.md5(sign);
        if (sign.equalsIgnoreCase(param.getSign())) {
            return true;
        }
        return false;
    }
}