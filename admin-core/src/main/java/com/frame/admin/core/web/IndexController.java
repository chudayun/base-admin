package com.frame.admin.core.web;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.service.CoreUserService;
import com.frame.admin.core.util.PlatformException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.frame.admin.ext.entity.CTenant;
import com.frame.admin.ext.pojo.UserLogin;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.rbac.UserLoginInfo;
import com.frame.admin.core.rbac.tree.MenuItem;
import com.frame.admin.core.util.HttpRequestLocal;

@Controller
public class IndexController {

	@Autowired
    CorePlatformService platformService;

	@Autowired
    CoreUserService userService;

	@Autowired
	HttpRequestLocal httpRequestLocal;

	@Autowired
	Environment env;

	@RequestMapping(value= {"/{tenantCode}","/"})
	public ModelAndView login(@PathVariable(name="tenantCode",required=false) String tenantCode) {
		ModelAndView view = new ModelAndView("/login.html");

		String tenantType = env.getProperty("tenant.type");


		//单租户
		if(StringUtils.isNotEmpty(tenantType) && "single".equals(tenantType)){
			//单租户运行模式下，如果url中有设置租户代码时，按照url中传递的租户打开，方便进入平台管理租户进行权限配置等。
			if(StringUtils.isEmpty(tenantCode)){
				tenantCode = env.getProperty("tenant.code");
				if(StringUtils.isEmpty(tenantCode)){
					throw new PlatformException("未知的帐号信息");
				}
			}
		}

		//租戶參數 或者從 域中 解析 租户代码
		// 根據租戶代碼,獲取租戶信息
		//獲取租戶配置信息，顯示不同登陸界面
		view.addObject("tenant", platformService.getTenant(tenantCode));


		return view;
	}

	@PostMapping("/login.do")
	public ModelAndView login(UserLogin userlogin) {
		//userlogin.setTenantId("9999");

		System.out.println("###############################" + httpRequestLocal.getRequestURI());
		CTenant tenant=  platformService.getTenant(userlogin.getTenantCode());
		userlogin.setTenantId(String.valueOf(tenant.getId()));
		//增加按照租戶id來登陸
		UserLoginInfo info = userService.login(userlogin);
		if (info == null) {
			throw new PlatformException("用户名密码错");
		}
		CoreUser user = info.getUser();
		CoreOrg currentOrg = info.getOrgs().get(0);
		for (CoreOrg org : info.getOrgs()) {
			if (org.getId() == user.getOrgId()) {
				currentOrg = org;
				break;
			}
		}

		info.setCurrentOrg(currentOrg);
		// 记录登录信息到session
		this.platformService.setLoginUser(info.getUser(), info.getCurrentOrg(), info.getOrgs());
		ModelAndView view = new ModelAndView("forward:/index.do");//forward  redirect
		return view;
	}
	
	@RequestMapping("/index.do")
	public ModelAndView index() {

		ModelAndView view = new ModelAndView("/index.html");
		CoreUser currentUser = platformService.getCurrentUser();
		Long orgId = platformService.getCurrentOrgId();
		MenuItem menuItem = platformService.getMenuItem(currentUser.getId(), orgId);
//		System.out.println(JSONObject.);

		view.addObject("tenant", platformService.getCurrentTenant());
		view.addObject("menus", menuItem);
		return view;
	}

	@RequestMapping("/logout.do")
	public ModelAndView logout(HttpServletRequest request) {

		CTenant tenant=  platformService.getCurrentTenant();
		String tenantCode = "";
		if(null!=tenant){
			tenantCode = tenant.getSubdomain();
		}

		HttpSession session = request.getSession();
		Enumeration eum = session.getAttributeNames();
		while(eum.hasMoreElements()) {
			String key = (String)eum.nextElement();
			session.removeAttribute(key);
		}
		session.invalidate();
		ModelAndView view = new ModelAndView("forward:/" + tenantCode);
		return view;
	}
	@RequestMapping("/changeOrg.do")
	public ModelAndView changeOrg(HttpServletRequest request,Long orgId) {
		platformService.changeOrg(orgId);
		ModelAndView view = new ModelAndView("redirect:/index.do");
		return view;
	}

}
