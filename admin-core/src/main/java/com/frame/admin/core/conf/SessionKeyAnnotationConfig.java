package com.frame.admin.core.conf;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.frame.admin.ext.entity.CSessionKey;
import com.frame.admin.ext.entity.CTenant;
import com.frame.admin.ext.service.CSessionKeyService;
import com.frame.admin.ext.service.CTenantService;
import com.frame.admin.core.annotation.SessionKeyCheck;
import com.frame.admin.core.entity.CoreOrg;
import com.frame.admin.core.entity.CoreUser;
import com.frame.admin.core.service.CoreAuditService;
import com.frame.admin.core.service.CorePlatformService;
import com.frame.admin.core.service.CoreUserService;
import com.frame.admin.core.util.HttpRequestLocal;
import com.frame.admin.core.web.JsonResult;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Aspect
@Component
public class SessionKeyAnnotationConfig {
	@Autowired
	CorePlatformService platformService;
	@Autowired
	CoreAuditService sysAuditService;
	@Autowired
	HttpRequestLocal httpRequestLocal;

	@Autowired
	CoreUserService userService;

	@Autowired
	CSessionKeyService cSessionKeyService;

	@Autowired
	CTenantService cTenantService;

	@Autowired
	Environment env;

	ObjectMapper jsonMapper = new ObjectMapper();
	private final Log log = LogFactory.getLog(this.getClass());

	/**
	 * 判断手机端调用api是否传入sessionkey
	 * @param pjp
	 * @param sessionKeyCheck
	 * @return
	 * @throws Throwable
	 */
	@org.aspectj.lang.annotation.Around("within(@org.springframework.stereotype.Controller *) && @annotation(sessionKeyCheck)")
	public Object functionAccessCheck(final ProceedingJoinPoint pjp, SessionKeyCheck sessionKeyCheck) throws Throwable {
		// debug
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = servletRequestAttributes.getRequest();
		String funCode = null;
		try {
			String sessionkey = request.getHeader("sessionkey");
			String clientType = request.getHeader("clientType");
			String imei = request.getHeader("imei");
			String tenantId = request.getHeader("tenantId");

			System.out.println(sessionkey);
			System.out.println(imei);
			System.out.println(tenantId);

				// appid 和 密钥 来申请 token ，根据token 调取
			if (sessionKeyCheck != null) {
				funCode = sessionKeyCheck.value();

				if (null == sessionkey || "".equalsIgnoreCase(sessionkey)) {
					//api接口权限校验
					System.out.println("api==============================================" + funCode);
					return new JsonResult().httpNotToken(null);
				}else{
					CSessionKey model = new CSessionKey();
					model.setClientType(clientType);
					model.setDelFlag(0);
					model.setSessionkey(sessionkey);
					model.setStatus(0);
					model.setTenantId(Long.valueOf(tenantId));
					model.setImei(imei);
					List<CSessionKey> rets = cSessionKeyService.queryBySessionkey(model);
					if(rets.size() >0 ) {

						CoreUser user = userService.getUserById(rets.get(0).getUserId());
						CoreOrg org = userService.getOrgById(user.getOrgId());
						List<CoreOrg> orgs = userService.getUserOrg(user.getId(), org.getId());
						CTenant tenant = cTenantService.queryById(user.getTenantId());
						platformService.setLoginUser(user,org,orgs);
						request.getSession().setAttribute(CorePlatformService.TENANT_CURRENT, tenant);
						request.getSession().setAttribute("ip", request.getRemoteHost());
					}else{

						return new JsonResult().httpNotToken(null);
					}
				}
			}
			Object o = pjp.proceed();

			return o;

		} catch (Throwable e) {

			throw e;
		}

	}
}


