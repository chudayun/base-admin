package com.frame.admin.core.dao;

import com.frame.admin.core.entity.CoreRole;
import com.frame.admin.core.web.query.RoleQuery;
import org.beetl.sql.core.annotatoin.SqlResource;
import org.beetl.sql.core.mapper.BaseMapper;

import java.util.List;

@SqlResource("core.coreRole")
public interface CoreRoleDao extends BaseMapper<CoreRole> {

    List<CoreRole> queryRoleByCondition(RoleQuery query);
}
