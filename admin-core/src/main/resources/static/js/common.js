/*一些基础的js方法，基础的业务js方法在lib.js里*/
//日期格式化
Date.prototype.format=function(format){var d=this,o={"M+":d.getMonth()+1,"d+":d.getDate(),"H+":d.getHours(),"m+":d.getMinutes(),"s+":d.getSeconds(),w:["日","一","二","三","四","五","六"][d.getDay()]};if(/(y+)/.test(format)){format=format.replace(RegExp.$1,(d.getFullYear()+"").substr(4-RegExp.$1.length))}for(var k in o){if(new RegExp("("+k+")").test(format)){format=format.replace(RegExp.$1,RegExp.$1.length==1?o[k]:("00"+o[k]).substr((""+o[k]).length))}}return format};

var Common = {
    ctxPath: "",
    version:"",
    log: function (info) {
        console.log(info);
    },
    alert: function (info, iconIndex,next) {
        parent.layer.msg(info, {
            icon: iconIndex
        },function(){
            if(next != null){next()}
        });
        
        
    },
    info: function (info,next) {
        Common.alert(info, 0,next);
    },
    success: function (info,next) {
        Common.alert(info, 1,next);
    },
    error: function (info,next) {
    		Common.openConfirm(info,next)
    },
    post: function (url, paras, next,error) {
    		$.ajax({
    			url:Common.ctxPath+url,
    			type:"POST",
    			data:paras,
    			success:function(rsp){
    				if(rsp.code!=0){
    					Common.error(rsp.msg);
    					
    				}else{
    					//成功
    					if(next!=null){
        					next(rsp.data);
        				}else{
        					Common.success(rsp.responseJSON.msg);
        				}
    				}
    				
    			},
    			error:function(rsp){
                    if(error!=null){
                        error(rsp.responseJSON);
                    }else{
                        Common.error(rsp.responseJSON.msg);
                    }
    			}
    		})
       
    },
    postJson: function (url, paras, next) {

        $.ajax({
            url:Common.ctxPath+url,
            type:"POST",
            data:JSON.stringify(paras),
            dataType: "json",
            contentType: 'application/json; charset=UTF-8',
            success:function(rsp){
                if(rsp.code!=0){
                    Common.error(rsp.msg);

                }else{
                    //成功
                    if(next!=null){
                        next(rsp.data);
                    }else{
                        Common.success(rsp.responseJSON.msg);
                    }
                }

            },
            error:function(rsp){
                Common.error(rsp.responseJSON.msg);
            }
        })

    },
    getOneFromTable:function(layuiTable,tableId){
    		var checkStatus = layuiTable.checkStatus(tableId)
        ,data = checkStatus.data;
        if(data.length==0){
        		Common.info("请选中一条记录");
        }else if(data.length>1){
        		Common.info("只能选中一条记录")
        }else{
        		return  data[0];
        }
    },
    getMoreDataFromTable:function(layuiTable,tableId){
		var checkStatus = layuiTable.checkStatus(tableId)
	    ,data = checkStatus.data;
	    if(data.length==0){
	    		Common.info("请选中记录");
	    }else{
	    		return  data;
	    }
    },
    openDlg:function(url,title,cb){
        var index = layer.open({
            type: 2,  
            content: Common.ctxPath+url,  
            title: title,  
            maxmin: false,success: cb || function(){}
        });  
		layer.full(index);
    },
    topOpenDlg:function(url,title){
                   		var index = top.layer.open({
                           type: 2,
                           content: Common.ctxPath+url,
                           title: title,
                           area:['100%', '100%'],
                           maxmin: false
                       });
               		layer.full(index);
   },
    openPage:function(url,title,area,next){
        var index = layer.open({
            type: 2,
            content: Common.ctxPath+url,
            title: title,
            area:area,
            maxmin: false,
            end: function () {//无论是确认还是取消，只要层被销毁了，end都会执行，不携带任何参数。layer.open关闭事件
               if(next != null){
                   next();
               }
            }
        });
    },
    openUserPage:function(limit,title,area,callback){
        var url = "/admin/user/userTree.do";
        var index = layer.open({
            type: 2,
            content: Common.ctxPath+url,
            title: title,
            area:area,
            scrollbar:false,
            btn: ['确定', '取消']
            ,yes: function(index, layero){
                var iframeWin=window[layero.find('iframe')[0]['name']];
                //执行iframe页的重新加载表格方法
                var datas = iframeWin.userData(limit);
                callback(datas,index);
            }
            ,btn2: function(index, layero){
                    //按钮【按钮二】的回调
                layer.close(index);
                    //return false 开启该代码可禁止点击该按钮关闭
            }
        });
    },
    openConfirm:function(content,callback,callBackNo){
    		var index = layer.confirm(content, {
    		  btn: ['确认','取消'] //按钮
    		}, function(){
    		    if(callback!=null){
    		        callback();
    		    }
    			layer.close(index);
    		}, function(){
    		    if(callBackNo!=null){
    		        callBackNo()
    		    }
    			layer.close(index);
    		});
		
    },
    openPrompt:function(title,defaultValue,callback){
    		layer.prompt({title: title, formType: 0,value:defaultValue}, function(value, index,elem){
    		  layer.close(index);
    		  callback(value);
    		});
    },
    concatBatchId:function(data,idField){
    		var ids = ""
    		var name=idField==null?"id":idField;
    		for(var i=0;i<data.length;i++){
    			var item = data[i];
    			ids=ids+item[name];
    			if(i!=data.length-1){
    				ids=ids+","
    			}
    		}
    		return ids;
    },
    sessionTimeoutRegistry: function () {
        $.ajaxSetup({
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            complete: function (XMLHttpRequest, textStatus) {
                //通过XMLHttpRequest取得响应头，sessionstatus，
                var sessionstatus = XMLHttpRequest.getResponseHeader("sessionstatus");
                if (sessionstatus == "timeout") {
                    //如果超时就处理 ，指定要跳转的页面
                    window.location = Common.ctxPath + "/global/sessionError";
                }
            }
        });
    },
    initValidator: function(formId,fields){
        $('#' + formId).bootstrapValidator({
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: fields,
            live: 'enabled',
            message: '该字段不能为空'
        });
    },
    getDate:function(date,pattern){
    		if(date==null||date==''){
    			return "";
    		}else{
    			if(pattern){
    				return new Date(date).format(pattern);
    			}else{
    				return date.substring(0,10);
    			}
    			
    		}
    },
    http500ToLogin:function(errorMessage){

        var isToLogin = false;
        switch (errorMessage) {
            case 'session_expiration':
                errorMessage = "登录失效过期,请重新登陆!";
                isToLogin = true;
                break;
            case 'userInvalid_expiration':
                errorMessage = "用户已经失效";
                isToLogin = true;
                break;
            case '试图访问未授权功能':
                this.error('试图访问未授权功能,请授权后操作',function(){
                    var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
                    if(null !=index){
                        parent.layer.close(index);
                    }else{
                        parent.layui.admin.events.closeThisTabs();
                    }
                });
                break;

        }

        if(isToLogin){
            parent.layer.open({
                content: errorMessage
                ,btn: '确定',yes: function(){
                    //跳转到登陆页面
                    top.window.location.href="/";
                }
            });
        }
    },
    generateUUID:function() {
    var d = new Date().getTime();
    if (window.performance && typeof window.performance.now === "function") {
        d += performance.now(); //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
}
    
};



// JQuery方法定制
(function($){
	
    
	$.ajaxSetup({
	  type: 'POST',
      async: true,
      dataType : "json",
      timeout : 30000,
        complete:function(XMLHttpRequest,textStatus){
	      //console.log(textStatus);
	      //console.log(XMLHttpRequest.responseText);
	      //根據code返回統一處理
            //var result = JSON.parse(XMLHttpRequest.responseText);
           // if('401' == result.code){
                // Common.error(result.msg,function(){
                //     window.location = Common.ctxPath + "/logout.do";
                // })
           // }
        },
        statusCode: {
            404: function() {
                alert('数据获取/输入失败，没有此服务。404');
            },
            504: function() {
                alert('数据获取/输入失败，服务器没有响应。504');
            },
            500: function(XMLHttpRequest) {
                console.log(XMLHttpRequest.responseText)
                var error = JSON.parse(XMLHttpRequest.responseText);
                Common.http500ToLogin(error.msg);
                //alert('服务器有误。500');
            }
        }
	 });
	
	/**
	 * 获取form表单数据
	 */
	$.fn.getFormData = function (isValid) {
	  var fieldElem = $(this).find('input,select,textarea'); //获取所有表单域
	  var data ={};
	  layui.each(fieldElem, function(index, item){
      if(!item.name) return;
      if(/^checkbox|radio$/.test(item.type) && !item.checked) return;
      var value = item.value;
      if(item.type == "checkbox"){//如果多选
      	if(data[item.name]){
      		value = data[item.name] + "," + value;
      	}
      }
      if(isValid)
      {
    	 //如果为true,只需要处理有数据的值
    	 if(!$.isEmpty(value))
       {
    		 data[item.name] = value;
       }
      }
      else
      {
    	  data[item.name] = value;
      }
    });
    return data;
  };
  
  $.fn.serializeJson = function() {
      var serializeObj = {};
      var array = this.serializeArray();
      var str = this.serialize();
      $(array).each(
              function() {
                  if (serializeObj[this.name]) {
                      if ($.isArray(serializeObj[this.name])) {
                          serializeObj[this.name].push(this.value);
                      } else {
                          serializeObj[this.name] = [
                                  serializeObj[this.name], this.value ];
                      }
                  } else {
                      serializeObj[this.name] = this.value;
                  }
              });
      return serializeObj;
  };
 
  
  $.extend({
  	//非空判断
  	isEmpty: function(value) {
  		if (value === null || value == undefined || value === '') { 
  			return true;
  		}
  		return false;
    },
    //获取对象指
    result: function(object, path, defaultValue) {
    	var value = "";
  		if(!$.isEmpty(object) && $.isObject(object) && !$.isEmpty(path)){
  			var paths = path.split('.');
  			var length = paths.length;
  			$.each(paths,function(i,v){
  				object = object[v];
  				if(length-1 == i){
						value = object;
					}
  				if(!$.isObject(object)){
  					return false;
  				}
  			})
  			
  		}
  		
  		if($.isEmpty(value) && !$.isEmpty(defaultValue)){
  			value = defaultValue;
  		}
  		return value;
    },
    //判断是否obj对象
    isObject : function(value) {
      var type = typeof value;
      return value != null && (type == 'object' || type == 'function');
    },
    //是否以某个字符开头
    startsWith : function(value,target){
    	return value.indexOf(target) == 0;
    },
    //设置sessionStorage
    setSessionStorage:function(key, data){
    	sessionStorage.setItem(key, data);
    },
    //获取sessionStorage
    getSessionStorage:function(key){
    	return sessionStorage.getItem(key) == null ? "" : sessionStorage.getItem(key);
    },
    //删除sessionStorage
    removeSessionStorage:function(key){
    	sessionStorage.removeItem(key);
    },
    //清除sessionStorage
    clearSessionStorage:function(){
    	sessionStorage.clear();
    },
    uuid : function(){
  		return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
		    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
		    return v.toString(16);
  		});
    }
  });

}(jQuery));
