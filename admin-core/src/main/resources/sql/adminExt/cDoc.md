queryByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_doc t
    where t.del_flag=0  
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    
    #tenant("t")#
    
    @if(!isEmpty(title)){
        and  t.title like #"%"+title+"%"#
    @}
    @if(!isEmpty(type)){
        and  t.type =#type#
    @}
    
    @if(!isEmpty(catalog)){
        and  t.catalog =#catalog#
    @}
    
queryByCondition2222
===


    select 
    @pageTag(){
    t.*
    @}
    from c_doc t
    where 1=1  
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    and #function("cDoc.query")#
    @if(!isEmpty(title)){
        and  t.title =#title#
    @}
    @if(!isEmpty(type)){
        and  t.type =#type#
    @}
    
    @if(!isEmpty(catalog)){
        and  t.catalog =#catalog#
    @}
    
    
      
    

batchDelCDocByIds
===

* 批量逻辑删除

    update c_doc set del_flag = 1 where id  in( #join(ids)#)
    
