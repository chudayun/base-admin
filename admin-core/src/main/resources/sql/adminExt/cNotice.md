queryByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_notice t
    where t.del_flag=0 
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    and #function("cNotice.query")#
    
    #tenant("t")#
    
    @if(!isEmpty(title)){
        and  t.title like #"%"+title+"%"#
    @}
    
    order by t.create_time desc
    

batchDelCNoticeByIds
===

* 批量逻辑删除

    update c_notice set del_flag = 1 where id  in( #join(ids)#)
    

    
queryReceiveListByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_notice t
    where t.del_flag=0 
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    and #function("cms.cNotice.query")#
     #tenant("t")#
    @if(!isEmpty(title)){
        and  t.title =#title#
    @}
    
    order by t.create_time desc
