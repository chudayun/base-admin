queryByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_open_app t
    where 1=1  
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  

    #tenant("t")#
    
    @if(!isEmpty(appId)){
        and  t.app_id =#appId#
    @}
    @if(!isEmpty(appName)){
        and  t.app_name like #"%"+appName+"%"#
    @}
    @if(!isEmpty(rules)){
        and  t.rules =#rules#
    @}
    
    
    

batchDelCOpenAppByIds
===

* 批量逻辑删除

    update c_open_app set del_flag = 1 where id  in( #join(ids)#)
    
getByAppId
===

* appId获取数据

	select 
    t.*
    from c_open_app t
    where 1=1  
    and  t.app_id =#appId#