queryByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_session_key t
    where 1=1  
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    and #function("cSessionKey.query")#
    
    #tenant("t")#
    
    @if(!isEmpty(sessionkey)){
        and  t.sessionkey like #"%"+sessionkey+"%"# 
    @}
    @if(!isEmpty(userId)){
        and  t.user_id like #"%"+userId+"%"# 
    @}
    @if(!isEmpty(userCode)){
        and  t.user_code like #"%"+userCode+"%"# 
    @}
    @if(!isEmpty(loginTime)){
        and  t.login_time =#loginTime#
    @}
    @if(!isEmpty(status)){
        and  t.status =#status#
    @}
    @if(!isEmpty(tenantId)){
        and  t.tenant_id =#tenantId#
    @}
    @if(!isEmpty(imei)){
        and  t.imei =#imei#
    @}

    

batchDelCSessionKeyByIds
===

* 批量逻辑删除

    update c_session_key set del_flag = 1 where id  in( #join(ids)#)
    

updateStatus
===

* 批量逻辑修改

    update c_session_key set status = 1 where imei = #imei# and status = 0
     

    
queryBySessionkey
===


    select 
    
    t.*
    
    from c_session_key t
    where 1=1  
    
    
    @if(!isEmpty(sessionkey)){
        and  t.sessionkey =#sessionkey#
    @}
    @if(!isEmpty(userId)){
        and  t.user_id =#userId#
    @}
    
    @if(!isEmpty(clientType)){
        and  t.client_type =#clientType#
    @}
    @if(!isEmpty(status)){
        and  t.status =#status#
    @}
    @if(!isEmpty(tenantId)){
            and  t.tenant_id =#tenantId#
    @}
    @if(!isEmpty(imei)){
        and  t.imei =#imei#
    @}