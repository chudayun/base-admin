queryByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_tenant t
    where 1=1  
    and t.del_flag = 0 
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    and #function("cTenant.query")#
    @if(!isEmpty(name)){
        and  t.name =#name#
    @}
    @if(!isEmpty(subdomain)){
        and  t.subdomain =#subdomain#
    @}
    @if(!isEmpty(domain)){
        and  t.domain =#domain#
    @}
    @if(!isEmpty(status)){
        and  t.status =#status#
    @}
    @if(!isEmpty(creditCode)){
        and  t.credit_code =#creditCode#
    @}
    
    
    

batchDelCTenantByIds
===

* 批量逻辑删除

    update c_tenant set del_flag = 1 where id  in( #join(ids)#)
    

getListAll
===
    select * from c_tenant where status = 0
    
getAdminUserByTenant
===

	select u.* from core_user u,core_role  r,core_user_role ur 
    	where r.code= #adminRoleCode# and r.ID=ur.ROLE_ID and ur.USER_ID=u.ID
        AND u.tenant_id = #id#
	
queryAdminUser
===
    select 
    @pageTag(){
	u.*,
	t.name tenant_name
	@}
	from core_user u left join c_tenant t
	on u.tenant_id = t.id
    where 1=1 
    
        and u.id in(
            select ur.USER_ID from core_role  r,core_user_role ur 
                    where r.ID=ur.ROLE_ID
            and r.code like 'TENANT_ADMIN%'
            @if(!isEmpty(adminRoleCode)){
                and r.code= #adminRoleCode#
            @}
            @if(!isEmpty(adminRoleId)){
                and r.id= #adminRoleId#
            @}
        )
        
        @if(!isEmpty(tenantId)){
            AND u.tenant_id = #tenantId#
        @}
        @if(!isEmpty(code)){
            and  u.code like #"%"+code+"%"#
        @}
        @if(!isEmpty(name)){
            and  u.name like #"%"+name+"%"#
        @}
        
        @if(!isEmpty(tenantName)){
            and  t.name like #"%"+tenantName+"%"#
        @}