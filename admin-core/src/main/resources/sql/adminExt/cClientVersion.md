queryByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_client_version t
    where 1=1  
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    and #function("cClientVersion.query")#
    @if(!isEmpty(versionCode)){
        and  t.version_code =#versionCode#
    @}
    @if(!isEmpty(versionName)){
        and  t.version_name =#versionName#
    @}
    @if(!isEmpty(osType)){
        and  t.os_type =#osType#
    @}
    @if(!isEmpty(packageType)){
        and  t.package_type =#packageType#
    @}
     @if(!isEmpty(appId)){
                      and  t.app_id =#appId#
                  @}

    and  t.del_flag =0

    
    

batchDelCClientVersionByIds
===

* 批量逻辑删除

    update c_client_version set del_flag = 1 where id  in( #join(ids)#)
    

getLatestVersion
===

* 获取最新版本


    select 
        t.*
        from c_client_version t
        where 1=1  
        
        @if(!isEmpty(versionCode)){
            and  t.version_code > #versionCode#
        @}
        
        and  t.os_type =#osType#
        and  t.app_id =#appId#
        and  t.del_flag =0
        ORDER BY version_code DESC
        limit 1