queryByCondition
===


    select 
    @pageTag(){
    t.*
    @}
    from c_config t
    where 1=1  
    @//数据权限，该sql语句功能点,如果不考虑数据权限，可以删除此行  
    
    #tenant("t")#
    
    @if(!isEmpty(name)){
        and  t.name like #"%"+name+"%"#
    @}
    @if(!isEmpty(code)){
        and  t.code like #"%"+code+"%"#
    @}
    @if(!isEmpty(catalog)){
        and  t.catalog =#catalog#
    @}
    @if(!isEmpty(type)){
        and  t.type =#type#
    @}
    and  t.del_flag =0
    order by t.type,t.catalog
    
    
    

batchDelCConfigByIds
===

* 批量逻辑删除

    update c_config set del_flag = 1 where id  in( #join(ids)#)
    

queryTenantCfgByCondition
===


    select 
    t.*
    from c_config t
    where 1=1  
    @if(!isEmpty(name)){
        and  t.name =#name#
    @}
    @if(!isEmpty(code)){
        and  t.code =#code#
    @}
    @if(!isEmpty(catalog)){
        and  t.catalog =#catalog#
    @}
    @if(!isEmpty(type)){
        and  t.type =#type#
    @}
    @if(!isEmpty(delFlag)){
        and  t.del_flag =#delFlag#
    @}
    @if(!isEmpty(tenantId)){
            and  t.tenant_id =#tenantId#
    @}
    order by t.type,t.catalog
    limit 500
    
    