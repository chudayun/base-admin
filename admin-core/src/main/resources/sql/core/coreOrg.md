queryOrgByUser
===

* 根据用户id查询可能所在的部门，考虑到兼职部门

	select * from core_org where id in ( select org_id from core_user_role where user_id=#userId# group by org_id) and del_flag = 0 order by id desc
	
queryAllOrgCode
===

* 根据id对应的code，目前用于传递给工作流系统

	select code from core_org where id  in( #join(orgIds)#)

getRoot
===
	select * from core_org where parent_org_id is null and del_flag = 0 
	
	@if(!isEmpty(isSuperAdmin)){
            and  tenant_id= #tenantId#
    @}

getTenantOrgRoot
===
	select * from core_org where parent_org_id is null and del_flag = 0 
     and  tenant_id= #tenantId#
		